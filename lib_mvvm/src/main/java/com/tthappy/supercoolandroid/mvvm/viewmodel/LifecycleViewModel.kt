package com.tthappy.supercoolandroid.mvvm.viewmodel

import androidx.lifecycle.ViewModel
import com.uber.autodispose.lifecycle.CorrespondingEventsFunction
import com.uber.autodispose.lifecycle.LifecycleEndedException
import com.uber.autodispose.lifecycle.LifecycleScopeProvider
import com.uber.autodispose.lifecycle.LifecycleScopes
import io.reactivex.CompletableSource
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/8 13:41
 * Update Date:
 * Modified By:
 * Description:
 */
open class LifecycleViewModel: ViewModel(), LifecycleScopeProvider<LifecycleViewModel.ViewModelEvent> {

    enum class ViewModelEvent { CREATED, CLEARED }

    companion object{
        private val CORRESPONDING_EVENTS = CorrespondingEventsFunction<ViewModelEvent> { event ->
            when (event) {
                ViewModelEvent.CREATED -> ViewModelEvent.CLEARED
                else -> throw LifecycleEndedException(
                        "Cannot bind to ViewModel lifecycle after onCleared."
                )
            }
        }
    }

    private val lifecycleEvents = BehaviorSubject.createDefault(ViewModelEvent.CREATED)

    override fun lifecycle(): Observable<ViewModelEvent> = lifecycleEvents.hide()

    override fun correspondingEvents() = CORRESPONDING_EVENTS

    override fun peekLifecycle() = lifecycleEvents.value

    //通过复写这个方法能够在 ViewModel 被销毁时进行额外的操作(比如释放资源等)。
    override fun onCleared() {
        lifecycleEvents.onNext(ViewModelEvent.CLEARED)
        super.onCleared()
    }

    override fun requestScope(): CompletableSource {
        return LifecycleScopes.resolveScopeFromLifecycle(this)
    }

}