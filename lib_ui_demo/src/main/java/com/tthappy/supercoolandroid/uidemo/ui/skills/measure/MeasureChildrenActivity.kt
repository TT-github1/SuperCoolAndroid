package com.tthappy.supercoolandroid.uidemo.ui.skills.measure

import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityMeasureChildrenBinding

@HpRouterData(name = "测量的逻辑", icon = "R.drawable.icon_double_drag_bar_pentagon", desc = "测量子View注意事项", isNew = false, url = BridgeConstansUIDemo.MEASURE_CHILDREN)
@Route(path = BridgeConstansUIDemo.MEASURE_CHILDREN)
class MeasureChildrenActivity : BaseTitleActivity<ActivityMeasureChildrenBinding, BaseViewModel>() {

    override fun initView() {

    }

    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = "测量的逻辑"
}