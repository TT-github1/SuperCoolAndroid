package com.tthappy.supercoolandroid.base.router

object RouterService {

    private const val INITSERVICE = "/initService/"
    const val EQUIPMENTUTILS = "/utilsService/equipmentUtils"
    const val HPLOG = "/hpLogService/hpLog"
    const val DEBUG = "/appService/debug"
    const val HOTFIX = "/uiDemoService/hotFix"

}