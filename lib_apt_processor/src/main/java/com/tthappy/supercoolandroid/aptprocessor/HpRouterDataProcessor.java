package com.tthappy.supercoolandroid.aptprocessor;

import com.google.auto.service.AutoService;
import com.tthappy.supercoolandroid.annotation.HpRouterData;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.JavaFileObject;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/6 14:22
 * Update Date:
 * Modified By:
 * Description:
 */

@AutoService(Processor.class)
public class HpRouterDataProcessor extends AbstractProcessor {

    private Messager mMessager;
//    private Elements mElementsUtils;
    private Filer mFiler;
    private Map<String, TypeElement> mProxyMap = new HashMap<>();

    /**
     * 这个方法用于初始化处理器，方法中有一个ProcessingEnvironment类型的参数，ProcessingEnvironment是一个注解处理工具的集合。
     * 如Filer可以用来编写新文件，Messager可以用来打印错误信息，还有Elements是一个可以处理Element的工具类。
     * 在这里我们有必要对Element做下说明
     * Element是一个接口，表示一个程序元素，它可以是包、类、方法或者一个变量。Element已知的子接口有：
     *
     * PackageElement 表示一个包程序元素。提供对有关包及其成员的信息的访问。
     * ExecutableElement 表示某个类或接口的方法、构造方法或初始化程序（静态或实例），包括注释类型元素。
     * TypeElement 表示一个类或接口程序元素。提供对有关类型及其成员的信息的访问。注意，枚举类型是一种类，而注解类型是一种接口。
     * VariableElement 表示一个字段、enum 常量、方法或构造方法参数、局部变量或异常参数。
     */
    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);

        mMessager = processingEnv.getMessager();
        mFiler = processingEnv.getFiler();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        mProxyMap.clear();
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(HpRouterData.class);

        String javaFileName = "HomeDataUtil";
        Writer writer = null;
        try {
            Element param = null;
            for(Element element : elements) {
                param = element;
            }

            JavaFileObject javaFileObject = mFiler.createSourceFile(javaFileName, param);
            writer = javaFileObject.openWriter();

            writer.write(createJavaFile(javaFileName, elements));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    private String createJavaFile(String javaFileName, Set<? extends Element> elements) {

        StringBuilder builder = new StringBuilder();

        String header =
                "package com.tthappy.supercoolandroid.java.test;\n" +
                "import com.tthappy.supercoolandroid.base.router.entity.HomeEntity;\n" +
                "import com.tthappy.supercoolandroid.uidemo.R;\n" +
                "import java.util.ArrayList;\n" +
                "import java.util.List;\n" +
                "\n" +
                "public class " + javaFileName + " {\n" +
                "    public List<HomeEntity> getList() {\n" +
                "       List<HomeEntity> list = new ArrayList<>();\n";

        builder.append(header);

        for (Element element : elements) {
            TypeElement classElement = (TypeElement) element;
            HpRouterData annotation = classElement.getAnnotation(HpRouterData.class);
            String line = "       list.add(new HomeEntity(\"" + annotation.name() + "\", " + annotation.icon() + ", \"" + annotation.desc() + "\", \"" + annotation.url() + "\"));\n";
            builder.append(line);
        }

        String footer =
                "        return list;\n" +
                "    }\n" +
                "}\n";

        builder.append(footer);
        return builder.toString();
    }

    /**
     * 这个方法的返回值是一个Set集合，集合中指定支持的注解类型的名称
     *（这里必须时完整的包名+类名，例如com.tthappy.supercoolandroid.annotation.HpRouterData）
     */
    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> supportTypes = new LinkedHashSet<>();
        supportTypes.add(HpRouterData.class.getCanonicalName());
        return supportTypes;
    }

    /**
     * 这个方法用来指定当前正在使用的Java版本，通常指定SourceVersion.latestSupported()。
     */
    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
