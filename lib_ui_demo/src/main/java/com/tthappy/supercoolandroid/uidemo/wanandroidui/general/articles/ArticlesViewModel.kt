package com.tthappy.supercoolandroid.uidemo.wanandroidui.general.articles

import com.tthappy.supercoolandroid.mvvm.event.SingleLiveData
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.wanandroid.ArticlesEntity
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.wanandroid.RegisterRequest
import com.tthappy.supercoolandroid.uidemo.network.retrofit.UiDemoRetrofit
import com.uber.autodispose.autoDispose
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/13 14:14
 * Update Date:
 * Modified By:
 * Description:
 */
class ArticlesViewModel: BaseViewModel() {

    val liveData = SingleLiveData<List<ArticlesEntity>>()
    fun getArticles() {
        UiDemoRetrofit.create().getArticles()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .bindLoadingCount()
            .autoDispose(this)
            .subscribe({
                liveData.postValue(it.data)
            }, {

            })
    }
}