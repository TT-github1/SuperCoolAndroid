package com.tthappy.supercoolandroid.mvvm.viewmodel

import com.tthappy.supercoolandroid.mvvm.event.SingleLiveData
import io.reactivex.Observable

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/8 11:34
 * Update Date:
 * Modified By:
 * Description:
 */
open class BaseViewModel: LifecycleViewModel() {

    var currentLoadingCount = 0
    val loadingCountLiveData = SingleLiveData<Int>()

    protected fun <T> Observable<T>.bindLoadingCount(): Observable<T> =
            doOnSubscribe {
                currentLoadingCount += 1
                loadingCountLiveData.postValue(currentLoadingCount)
            }.doFinally {
                currentLoadingCount -= 1
                loadingCountLiveData.postValue(currentLoadingCount)
            }
}