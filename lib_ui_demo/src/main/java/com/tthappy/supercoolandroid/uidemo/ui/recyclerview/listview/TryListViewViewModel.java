package com.tthappy.supercoolandroid.uidemo.ui.recyclerview.listview;

import com.tthappy.supercoolandroid.mvvm.event.SingleLiveData;
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/11 11:43
 * Update Date:
 * Modified By:
 * Description:
 */
public class TryListViewViewModel extends BaseViewModel {

    SingleLiveData<List<String>> listLiveData = new SingleLiveData<>();
    public void getList() {
        List<String> list = new ArrayList<>();
        list.add("张三");
        list.add("张三");
        list.add("张三");
        list.add("张三");
        list.add("张三");
        list.add("张三");
        listLiveData.postValue(list);
    }

}
