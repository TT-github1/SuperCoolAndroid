package com.tthappy.supercoolandroid.uidemo.ui.uitest;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.annotation.HpRouterData;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils;
import com.tthappy.supercoolandroid.log.HpLog;
import com.tthappy.supercoolandroid.uidemo.R;

@HpRouterData(name = "UI测试专用入口", icon = "R.drawable.icon_double_drag_bar_pentagon", desc = "UI测试专用入口", url = BridgeConstansUIDemo.UITEST)
@Route(path = BridgeConstansUIDemo.UITEST)
public class UITestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ui_test_demo);

        initView();
        HpLog.INSTANCE.e("onCreate: ");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        HpLog.INSTANCE.e("onRestart: ");
    }

    @Override
    protected void onStart() {
        super.onStart();

        HpLog.INSTANCE.e("onStart: ");

//        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        HpLog.INSTANCE.e("onResume: ");
    }

    @Override
    protected void onPause() {
        super.onPause();

        HpLog.INSTANCE.e("onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();

        HpLog.INSTANCE.e("onStop: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        HpLog.INSTANCE.e("onDestroy: ");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        HpLog.INSTANCE.e("onNewIntent: ");
    }

    private void initView() {
        TextView tvText = findViewById(R.id.tv_text);
        tvText.setOnClickListener(v -> {
            BridgeUtils.bridgeWithUrl(BridgeConstansUIDemo.LIFECYCLE_SHOW);
        });

        TextView tvImg = findViewById(R.id.tv_img);
        tvImg.setOnClickListener(v -> {

        });

        TextView tvLink = findViewById(R.id.tv_link);
        tvLink.setOnClickListener(v -> {

        });

        TextView tvMore = findViewById(R.id.tv_more);
        tvMore.setOnClickListener(v -> {

        });
    }
}