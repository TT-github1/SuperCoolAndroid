package com.tthappy.supercoolandroid.uidemo.data.entity.entity.wanandroid

/**
 * Author:      tfhe
 * Create Date: Created in 2022/3/31 16:59
 * Update Date:
 * Modified By:
 * Description:
 */
data class RegisterRequest(
    val username: String,
    val password: String,
    val repassword: String
)
