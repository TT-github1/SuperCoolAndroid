package com.tthappy.supercoolandroid.uidemo.ui.skills.photoandpicture;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity;
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityTakePhotoDemoBinding;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

@Route(path = BridgeConstansUIDemo.TAKEPHOTO)
public class TakePhotoActivity extends BaseActivity<ActivityTakePhotoDemoBinding, BaseViewModel> {

    private Uri uri;
    private File outputImage;
    private ImageView ivPhoto;

    @Override
    public void initView() {
//        ToolbarHelper helper = getToolbarHelper();
//        helper.enableBack(context);
//        helper.setTitle("照相馆");

        TextView tenYuan = findViewById(R.id.tv_take_photo);
        ivPhoto = findViewById(R.id.iv_photo_show);

        TextView liangYi = findViewById(R.id.tv_from_photo);

        tenYuan.setOnClickListener(v -> {
            //创建将来用来保存相片的文件
            outputImage = new File(getExternalCacheDir(), "output_image.jpg");
            //如果文件已经存在就删球了它
            if (outputImage.exists()) {
                outputImage.delete();
            }
            //创建新文件？
            try {
                outputImage.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //获取文件资源标识
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(this, "com.tthappy.supercool.fileprovider", outputImage);
            } else {
                uri = Uri.fromFile(outputImage);
            }

            //前往照相，别忘了投币哦，十块钱一次
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, 1);
        });

        liangYi.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, 2);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));
                    ivPhoto.setImageBitmap(rotateIfRequired(bitmap));

                    ContentResolver resolver = getContentResolver();
                    ContentValues values = new ContentValues();

                    values.put(MediaStore.Images.Media.DISPLAY_NAME, "ssuper.jpg");
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                    //非常之重要的代码，可以指定存储位置
                    values.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_PICTURES + "/ssuper");

                    Uri normalUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    Uri newUri = resolver.insert(normalUri, values);
//                    e(newUri.toString() + "\n" + values.toString());

                    OutputStream outputStream = getContentResolver().openOutputStream(newUri);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                    outputStream.close();

                    Uri uri = Uri.parse("file://" + "/storage/emulated/0/Pictures/ssuper/ssuper.jpg");
//                    e(uri.toString());
//                    e(uri.getPath());
                    Objects.requireNonNull(getMActivity()).sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + Environment.getExternalStorageDirectory())));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (requestCode == 2) {
            assert data != null;
            Bitmap bitmap = getBitmapFromUri(data.getData());
            ivPhoto.setImageBitmap(bitmap);
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) {
        ContentResolver resolver = getContentResolver();
        try {
            ParcelFileDescriptor parcelFileDescriptor = resolver.openFileDescriptor(uri, "r");
            return BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Bitmap rotateIfRequired(Bitmap bitmap) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(outputImage.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert exifInterface != null;
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateBitmap(bitmap, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateBitmap(bitmap, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateBitmap(bitmap, 270);
            default:
                return bitmap;
        }
    }

    private Bitmap rotateBitmap(Bitmap bitmap, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return rotateBitmap;
    }

    @Override
    public void initData() {

    }


    @Override
    public void observeData() {

    }
}