package com.tthappy.supercoolandroid.mvvm.viewmodel

import androidx.lifecycle.ViewModelProvider
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity
import java.lang.reflect.ParameterizedType

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/24 9:55
 * Update Date:
 * Modified By:
 * Description:
 */

//获取目标泛型
fun <VM: BaseViewModel> provideViewModelWithGeneric(any: Any): VM {
    any.allParameterizedType.forEach{ parameterizedType ->
        parameterizedType.actualTypeArguments.forEach {
            try {
                return ViewModelProvider(any as BaseActivity<*, *>).get(it as Class<VM>)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    throw IllegalArgumentException("There is no generic of ViewModel.")
}

// 获取实例的带实际泛型参数的父类列表
private val Any.allParameterizedType: List<ParameterizedType>
    get() {
        val targetList = mutableListOf<ParameterizedType>()
        var parameterizedType = javaClass.genericSuperclass
        var superClass = javaClass.superclass
        while(superClass != null) {
            if (parameterizedType is ParameterizedType) targetList.add(parameterizedType)
            parameterizedType = superClass.genericSuperclass
            superClass = superClass.superclass
        }
        return targetList
    }