package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/5 15:33
 * Update Date:
 * Modified By:
 * Description:
 */
public class JavaTest_29 {
    public static void main(String[] args) {
        String msg1 = "00110101";
        String msg2 = "01000110";
    }

    private static void find(String msg1, String msg2) {
        
    }

    public int strStr(String haystack, String needle) {
        if(needle.equals("")) return 0;

        int i = 0;
        int j = 0;

        int[] next = nextArr(needle);

        int len = haystack.length();

        for(; i < len; i++) {
            while(j >= 0 && haystack.charAt(i) != needle.charAt(j)) {
                j = next[j - 1];
            }
            if(haystack.charAt(i) == needle.charAt(j)) j++;
            if(j == needle.length()) return i;
        }

        return -1;
    }

    private int[] nextArr(String needle) {
        if(needle.equals("")) return new int[0];
        int j = 0;
        int i = 1;
        int len = needle.length();
        int[] next = new int[len];

        for(; i < len; i++) {
            while(j > 0 && needle.charAt(i) != needle.charAt(j)) {
                j = next[j - 1];
            }
            if(needle.charAt(i) == needle.charAt(j)) j++;
            next[i] = j;
        }

        return next;
    }
}
