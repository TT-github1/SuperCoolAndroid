package com.tthappy.supercoolandroid.network

import retrofit2.Retrofit

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/11 17:01
 * Update Date:
 * Modified By:
 * Description:
 */
object RetrofitUtils {

    fun get(): Retrofit = RetrofitCreator(NetworkStrategy("https://wanandroid.com")).retrofit

    fun get(url: String) : Retrofit = RetrofitCreator(NetworkStrategy(url)).retrofit

}