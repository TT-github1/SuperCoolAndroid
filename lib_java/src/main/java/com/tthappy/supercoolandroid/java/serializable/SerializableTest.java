package com.tthappy.supercoolandroid.java.serializable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/5 12:21
 * Update Date:
 * Modified By:
 * Description:
 */
public class SerializableTest {

    public static void main(String[] args) {
        output();
//        intput();
    }


    private static void output() {
        try {
            FileOutputStream fos = new FileOutputStream("C:\\Users\\ad\\Documents\\SuperCoolAndroid\\lib_java\\src\\main\\java\\com\\tthappy\\supercoolandroid\\java\\serializable\\output.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(new Person());
            oos.close();
            fos.close();
            System.out.println("请到文件中查看输出结果");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void intput() {
        try {
//            System.out.println(System.getProperties().getProperty());
            FileInputStream fis  = new FileInputStream("C:\\Users\\ad\\Documents\\SuperCoolAndroid\\lib_java\\src\\main\\java\\com\\tthappy\\supercoolandroid\\java\\serializable\\output.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            Person person = (Person) ois.readObject();
            System.out.println(person);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static class AAA extends Thread {
        @Override
        public void run() {
            super.run();
        }
    }
}
