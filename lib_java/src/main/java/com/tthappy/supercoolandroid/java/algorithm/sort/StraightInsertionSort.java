package com.tthappy.supercoolandroid.java.algorithm.sort;

import com.tthappy.supercoolandroid.java.datamock.HpDataLib;
import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/1 17:15
 * Update Date:
 * Modified By:
 * Description: 直接插入排序
 *
 *              将数组自己在脑内脑补成两个区域，前面是有序区，刚开始只有第一个元素，
 *              后面全部是无序区，之后开始遍历，每次遍历，取无序区的第一个元素，往有序区
 *              按顺序插入，遍历完了，也就排序好了
 */

public class StraightInsertionSort {

    public static void main(String[] args) {
        int[] arr = HpDataLib.getArrI(15);
        System.out.println(Arrays.toString(arr));
        sort_new(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void sort(int[] arr) {
        // 有序区和无序区的分界线，同时也是无序区第一个元素
        int i;
        // 有序区从后向前的指针
        int j;
        // 当要将无序区的第一个元素往有序区中间（注意不是最后）插入时，用来迁移数据的指针
        int k;

        for (i = 1; i < arr.length; i++) {
            // 从有序区后面往前进行比对，因为有序区已经排成从小到大的顺序，
            // 因此找到的第一个比arr[i]小的元素，就是arr[i]将要插入的位置的前一个元素
            for (j = i - 1; j >= 0; j--) {
                if (arr[j] < arr[i]) {
                    break;
                }
            }

            // 判断是否是插入在有序区的末尾
            if (j != i - 1) {
                int tmp = arr[i];
                // 在中间插入，后面的元素挨个往后移动
                for (k = i - 1; k > j; k--) {
                    arr[k + 1] = arr[k];
                }
                // 其实就是arr[j+1] = tmp;
                arr[k + 1] = tmp;
            }
        }
    }

    public static void sort_new(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if (arr[j] > arr[j+1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                }
                else break;
            }
        }
    }
}
