package com.tthappy.supercoolandroid.java.datastruct.tree;

import java.util.Stack;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/8 9:45
 * Update Date:
 * Modified By:
 * Description:
 */
public class AVLTree<T extends Comparable<T>> {

    public Node head;

    public void add(T data) {
        if (data == null) {
            System.out.println("插入的数据不能为空");
            return;
        }
        head = add(head, data);
    }

    private Node add(Node tree, T data) {
        if (tree == null) {
            tree = new Node(data);
        } else {
            int tmp = data.compareTo(tree.data);

            if (tmp < 0) {
                tree.lChild = add(tree.lChild, data);
                /*
                 *  重点3 :
                 *  在插入节点后，需要判断当前树是否平衡，不平衡就进行相应的旋转操作
                 */
                if (getHeight(tree.lChild) - getHeight(tree.rChild) == 2) {
                    /*
                     *  重点5 :
                     *  LL、LR、add等方法均需要返回node节点
                     */
                    if (data.compareTo(tree.lChild.data) < 0) tree = LL(tree);
                    else tree = LR(tree);
                }
            } else if (tmp > 0) {
                tree.rChild = add(tree.rChild, data);
                if (getHeight(tree.lChild) - getHeight(tree.rChild) == -2) {
                    if (data.compareTo(tree.rChild.data) > 0) tree = RR(tree);
                    else tree = RL(tree);
                }
            } else {
                System.out.println("插入重复值，值为: " + data.toString());
                return tree;
            }
        }

        /*
         *  重点1 :
         *  每次在递归体最后都会计算当前节点的高度
         *  注：插入一次数据可能计算多个节点的高度
         */
        tree.height = max(tree.lChild, tree.rChild) + 1;

        return tree;
    }

    public void insert(T data) {
        insert(head, data);
    }

    // 非递归方法实现
    private void insert(Node tree, T data) {
        Stack<Node> stack = new Stack<>();
        Node cursor = tree;
        Node target = null;
        int cmp = 0;

        while (cursor != null) {
            stack.push(cursor);
            target = cursor;
            cmp = data.compareTo(cursor.data);

            if (cmp < 0) {
                cursor = cursor.lChild;
            } else if (cmp > 0) {
                cursor = cursor.rChild;
            } else {
                System.out.println("不可以插入已经存在的数据哦");
                return;
            }
        }

        Node newNode = new Node(data);
        if (cmp < 0) target.lChild = newNode;
        else if (cmp > 0) target.rChild = newNode;
        else head = newNode;

        stack.push(newNode);

        calculateHeightAndBanlence(stack, data);
    }

    private void calculateHeightAndBanlence(Stack<Node> stack, T data) {
        while (!stack.isEmpty()) {
            Node node = stack.pop();

            int cmp = getHeight(node.lChild) - getHeight(node.rChild);
            if (cmp == -2) {
                if (data.compareTo(node.rChild.data) > 0) {
                    if (stack.isEmpty()) head = RR(node);
                    else {
                        Node parent = stack.peek();
                        if (parent.lChild == node) parent.lChild = RR(node);
                        else parent.rChild = RR(node);
                    }
                } else {
                    if (stack.isEmpty()) head = RL(node);
                    else {
                        Node parent = stack.peek();
                        if (parent.lChild == node) parent.lChild = RL(node);
                        else parent.rChild = RL(node);
                    }
                }
            } else if (cmp == 2) {
                if (data.compareTo(node.lChild.data) < 0) {
                    if (stack.isEmpty()) head = LL(node);
                    else {
                        Node parent = stack.peek();
                        if (parent.lChild == node) parent.lChild = LL(node);
                        else parent.rChild = LL(node);
                    }
                } else {
                    if (stack.isEmpty()) head = LR(node);
                    else {
                        Node parent = stack.peek();
                        if (parent.lChild == node) parent.lChild = LR(node);
                        else parent.rChild = LR(node);
                    }
                }
            } else {
                node.height = max(node.lChild, node.rChild) + 1;
            }
        }
    }

    private int max(Node node1, Node node2) {
        return Math.max(getHeight(node1), getHeight(node2));
    }

    private Node LL(Node tree) {
        Node node;

        node = tree.lChild;
        tree.lChild = node.rChild;
        node.rChild = tree;

        /*
         *  重点2 :
         *  每次进行旋转操作后，会重新计算位置变动过的两个节点的高度
         */
        tree.height = max(tree.lChild, tree.rChild) + 1;
        node.height = max(node.lChild, tree) + 1;

        return node;
    }

    private Node RR(Node tree) {
        Node node;

        node = tree.rChild;
        tree.rChild = node.lChild;
        node.lChild = tree;

        tree.height = max(tree.lChild, tree.rChild) + 1;
        node.height = max(node.rChild, tree) + 1;

        return node;
    }

    private Node LR(Node tree) {
        /*
         *  重点4 :
         *  LR旋转的操作其实分两部，先对失衡节点的左孩子进行RR旋转，将LR型转变成LL型
         *  然后再对失衡节点进行LL旋转，完成操作
         */
        tree.lChild = RR(tree.lChild);
        return LL(tree);
    }

    private Node RL(Node tree) {
        tree.rChild = LL(tree.rChild);
        return RR(tree);
    }

    private int getValue(T data) {
        return data.toString().length();
    }

    public int getHeight(Node tree) {
        return tree == null ? 0 : tree.height;
    }

    public void preOrder(Node tree) {
        if (tree == null) return;
        visit(tree);
        preOrder(tree.lChild);
        preOrder(tree.rChild);
    }

    public void visit(Node node) {
        System.out.println(node.data);
    }

    public void showTree() {
        preOrder(head);
    }

    public void remove(T data) {
        if (data == null) {
            System.out.println("不能删除空数据");
            return;
        }
        head = remove(head, data);
    }

    private Node remove(Node tree, T data) {
        if (tree == null) return null;

        int cmp = data.compareTo(tree.data);
        if (cmp < 0) {

            tree.lChild = remove(tree.lChild, data);
            /*
             *  重点6 :
             *  在删除节点后，需要判断当前树是否平衡，不平衡就进行相应的旋转操作
             */
            if (getHeight(tree.lChild) - getHeight(tree.rChild) == -2) {
                //相等的情况使用RL操作和RR操作，虽然结果不同，但都能使结果达到平衡
                //但是RL操作比RR操作更为复杂，所以将相等的情况使用RR操作进行平衡
                if (getHeight(tree.rChild.lChild) > getHeight(tree.rChild.rChild)) tree = RL(tree);
                else tree = RR(tree);
            }

        } else if (cmp > 0) {

            tree.rChild = remove(tree.rChild, data);
            if (getHeight(tree.lChild) - getHeight(tree.rChild) == 2) {
                if (getHeight(tree.lChild.rChild) > getHeight(tree.lChild.lChild)) tree = LR(tree);
                else tree = LL(tree);
            }

        } else {

            if (tree.lChild != null && tree.rChild != null) {

                if (getHeight(tree.lChild) > getHeight(tree.rChild)) {
                    /*
                     *  如果tree的左子树比右子树高
                     *  1)找出tree的左子树中的最大节点
                     *  2)将该最大节点的值赋值给tree
                     *  3)删除该最大节点
                     *  这类似于用"tree的左子树中最大节点"做"tree"的替身
                     *  采用这种方式的好处是：删除"tree的左子树中最大节点"之后，AVL树仍然是平衡的
                     */
                    Node max = getMax(tree.lChild);
                    tree.data = max.data;
                    tree.lChild = remove(tree.lChild, max.data);
                } else {
                    Node min = getMin(tree.rChild);
                    tree.data = min.data;
                    tree.rChild = remove(tree.rChild, min.data);
                }

            } else {
                tree = tree.lChild != null ? tree.lChild : tree.rChild;
            }

        }

        return tree;
    }

    private Node getMax(Node tree) {
        return tree.rChild == null ? tree : getMax(tree.rChild);
    }

    private Node getMin(Node tree) {
        return tree.lChild == null ? tree : getMax(tree.lChild);
    }

    public class Node implements PrintTree.PrintNode<T> {
        int height;
        Node lChild;
        T data;
        Node rChild;

        public Node(T data) {
            this(data, null, null, 0);
        }

        public Node(T data, Node lChild, Node rChild, int height) {
            this.data = data;
            this.lChild = lChild;
            this.rChild = rChild;
            this.height = height;
        }

        @Override
        public T getData() {
            return data;
        }

        @Override
        public PrintTree.PrintNode<T> getLChild() {
            return lChild;
        }

        @Override
        public PrintTree.PrintNode<T> getRChild() {
            return rChild;
        }
    }
}
