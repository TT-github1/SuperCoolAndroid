package com.tthappy.supercoolandroid.views.dialog

import android.app.Activity
import android.content.Context
import android.text.method.LinkMovementMethod
import android.text.method.ScrollingMovementMethod
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatDialog
import com.tthappy.supercoolandroid.utils.measure.DimenUtils.dp2px
import com.tthappy.supercoolandroid.views.R
import com.tthappy.supercoolandroid.views.databinding.LayoutCommonDialogBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2021/8/20 16:53
 * Update Date:
 * Modified By:
 * Description:
 */
object CommonDialogFactory {
    /**
     * Create an empty dialog.
     *
     * @param context The context.
     * @return The dialog
     */
    fun create(context: Context): CommonDialog {
        return CommonDialog(context)
    }

    class CommonDialog(context: Context) : AppCompatDialog(context, R.style.HpCommonDialog) {
        private var viewBinding: LayoutCommonDialogBinding = LayoutCommonDialogBinding.inflate(layoutInflater)

        private var mClickDismissable = true
        private var mEnableShowSoftKeyBoard = true
        private var mWithAnimation = true
        private var mContext: Context

        init {
            setContentView(viewBinding.root)
            window?.setBackgroundDrawableResource(android.R.color.transparent)
            mContext = context
            viewBinding.amCommonDialogContent.movementMethod = ScrollingMovementMethod.getInstance()
            viewBinding.amCommonDialogContent.movementMethod = LinkMovementMethod.getInstance()
            viewBinding.amCommonDialogButtonLeft.setOnClickListener(DismissClickListener())
            viewBinding.amCommonDialogButtonRight.setOnClickListener(DismissClickListener())
            setDialogWidth(300)
            cancelable(true)
        }

        fun dismissable(clickDismissable: Boolean): CommonDialog {
            mClickDismissable = clickDismissable
            return this
        }

        fun withAnimation(withAnimation: Boolean): CommonDialog {
            mWithAnimation = withAnimation
            return this
        }

        fun cancelable(cancelable: Boolean): CommonDialog {
            setCancelable(cancelable)
            setCanceledOnTouchOutside(cancelable)
            return this
        }

        fun title(content: CharSequence?): CommonDialog {
            viewBinding.amCommonDialogTitleContainer.visibility = View.VISIBLE
            viewBinding.amCommonDialogTitle.text = content
            return this
        }

        fun content(content: CharSequence?): CommonDialog {
            viewBinding.amCommonDialogContent.visibility = View.VISIBLE
            viewBinding.amCommonDialogContent.text = content
            return this
        }

        fun hideContent(): CommonDialog {
            viewBinding.amCommonDialogContent.visibility = View.GONE
            return this
        }

        fun view(view: View?): CommonDialog {
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParams.setMargins(
                dp2px(mContext, 10f), dp2px(mContext, 5f),
                dp2px(mContext, 10f), dp2px(mContext, 5f)
            )
            return view(view, layoutParams)
        }

        fun view(view: View?, layoutParams: LinearLayout.LayoutParams?): CommonDialog {
            viewBinding.amCommonDialogViewContainer.addView(view, layoutParams)
            return this
        }

        fun left(text: CharSequence?): CommonDialog {
            viewBinding.amCommonDialogButtonLeft.text = text
            return this
        }

        fun left(color: Int): CommonDialog {
            viewBinding.amCommonDialogButtonLeft.setTextColor(color)
            return this
        }

        fun left(listener: View.OnClickListener): CommonDialog {
            viewBinding.amCommonDialogButtonLeft.setOnClickListener(DismissClickListener(listener))
            return this
        }

        fun left(text: CharSequence?, listener: View.OnClickListener): CommonDialog {
            left(text)
            left(listener)
            return this
        }

        fun hideLeft(): CommonDialog {
            viewBinding.amCommonDialogButtonLeft.visibility = View.GONE
            return this
        }

        fun right(text: CharSequence?): CommonDialog {
            viewBinding.amCommonDialogButtonRight.text = text
            return this
        }

        fun right(color: Int): CommonDialog {
            viewBinding.amCommonDialogButtonRight.setTextColor(color)
            return this
        }

        fun right(listener: View.OnClickListener): CommonDialog {
            viewBinding.amCommonDialogButtonRight.setOnClickListener(DismissClickListener(listener))
            return this
        }

        fun right(text: CharSequence?, listener: View.OnClickListener): CommonDialog {
            right(text)
            right(listener)
            return this
        }

        fun hideButtons(): CommonDialog {
            viewBinding.amButtonContainer.visibility = View.GONE
            return this
        }

        fun paddingHorizontal(padding: Int): CommonDialog {
            viewBinding.root.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            val dm = DisplayMetrics()
            (mContext as Activity).windowManager.defaultDisplay.getMetrics(dm)
            window?.setLayout(
                dm.widthPixels - dp2px(mContext, (padding * 2).toFloat()),
                window!!.attributes.height
            )
            return this
        }

        private fun setDialogWidth(width: Int) {
            viewBinding.root.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            val dm = DisplayMetrics()
            (mContext as Activity).windowManager.defaultDisplay.getMetrics(dm)
            window?.setLayout(dp2px(mContext, width.toFloat()), window!!.attributes.height)
        }

        override fun show() {
            if (mContext is Activity && ((mContext as Activity).isFinishing || (mContext as Activity).isDestroyed)) {
                return
            }
            val window = window
            if (window != null) {
                if (mWithAnimation) {
                    window.setWindowAnimations(R.style.AmarCommonDialogWindowAnim)
                }
            }
            super.show()
        }

        private inner class DismissClickListener(val listener: View.OnClickListener? = null) :
            View.OnClickListener {

            override fun onClick(v: View) {
                listener?.onClick(v)
                if (mClickDismissable && isShowing) {
                    dismiss()
                }
            }
        }
    }
}