package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/28 14:35
 * Update Date:
 * Modified By:
 * Description: 编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 ‘1’ 的个数（也被称为 汉明重量)
 */
public class JavaTest_16 {
    public static void main(String[] args) {
        System.out.println(count(7));
    }

    private static int count(int n) {
        int index = 32;
        int result = 0;
        for (int i = 0; i < index; i++) {
            if (((n >> i) & 1) == 1) result++;
        }
        return result;
    }
}
