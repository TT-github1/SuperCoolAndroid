package com.tthappy.supercoolandroid.uidemo.ui.skills.lifecycle

import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tthappy.supercoolandroid.uidemo.R

class LifecycleShowAdapter: BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_lifecycle_show) {
    override fun convert(holder: BaseViewHolder, item: String) {
        holder.setText(R.id.tv_title, item)
        holder.setBackgroundColor(R.id.cl_root, ContextCompat.getColor(context, if (holder.layoutPosition % 2 == 0) R.color.main_line else R.color.main_white))
    }
}