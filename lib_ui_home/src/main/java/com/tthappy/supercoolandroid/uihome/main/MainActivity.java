package com.tthappy.supercoolandroid.uihome.main;


import android.content.Intent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.tthappy.supercoolandroid.base.router.BridgeConstants;
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity;
import com.tthappy.supercoolandroid.uihome.R;
import com.tthappy.supercoolandroid.uihome.databinding.ActivityMainBinding;
import com.tthappy.supercoolandroid.uihome.main.category.SecondFragment;
import com.tthappy.supercoolandroid.uihome.main.category.ThirdFragment;
import com.tthappy.supercoolandroid.uihome.main.category.home.HomeFragment;
import com.tthappy.supercoolandroid.uihome.main.category.mine.MineFragment;
import com.tthappy.supercoolandroid.uihome.main.entity.TabEntity;

import java.util.ArrayList;

@Route(path = BridgeConstants.MAIN)
public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> {

    private HomeFragment fragment1;
    private SecondFragment fragment2;
    private ThirdFragment fragment3;
    private MineFragment fragment4;
    private FragmentTransaction transaction;
    private final String[] mTitles = {"首页", "次页", "山治", "我的"};
    private final ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();

    @Override
    public void initView() {
        for (String mTitle : mTitles) {
            mTabEntities.add(new TabEntity(mTitle, 0, 0));
        }
        viewBinding.commonTabLayout.setTabData(mTabEntities);
        viewBinding.commonTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                switchFragment(position);
                if (position == 0) {
                    viewBinding.background.setVisibility(View.VISIBLE);
                } else {
                    viewBinding.background.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabReselect(int position) {
            }
        });

//        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R ||
//                Environment.isExternalStorageManager()
//        ) {
//            Toast.makeText(this, "已获得访问所有文件的权限", Toast.LENGTH_SHORT).show();
//        } else {
//            Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
//            startActivity(intent);
//        }

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/zip");
        startActivity(intent);

        switchFragment(0);

        viewBinding.getRoot().setOnClickListener((view -> {
            Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            i.setType("*/*");
            startActivityForResult(i, 1);
        }));

//        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
//        intent.type = "*/*" //无类型限制
// 有类型限制是这样的:
// intent.setType(“image/*”);//选择图片
// intent.setType(“audio/*”); //选择音频
// intent.setType(“video/*”); //选择视频 （mp4 3gp 是android支持的视频格式）
// intent.setType(“video/*;image/*”);//同时选择视频和图片
// 有类型限制是这样的:
// intent.setType(“image/*”);//选择图片
// intent.setType(“audio/*”); //选择音频
// intent.setType(“video/*”); //选择视频 （mp4 3gp 是android支持的视频格式）
// intent.setType(“video/*;image/*”);//同时选择视频和图片
// intent.addCategory(Intent.CATEGORY_OPENABLE)
//        startActivityForResult(intent, 1)
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        ToastUtils.showLong(data.getData().toString().substring(20));
        viewBinding.test.setText(data.getData().toString());
    }

    private void switchFragment(int position) {
        transaction = getSupportFragmentManager().beginTransaction();
        hideFragments();
        switch (position) {
            case 1:
                if (null == fragment2) {
                    fragment2 = SecondFragment.getInstance();
                    transaction.add(R.id.fl_container, fragment2, "second");
                } else {
                    transaction.show(fragment2);
                }
                break;
            case 2:
                if (null == fragment3) {
                    fragment3 = ThirdFragment.getInstance();
                    transaction.add(R.id.fl_container, fragment3, "third");
                } else {
                    transaction.show(fragment3);
                }
                break;
            case 3:
                if (null == fragment4) {
                    fragment4 = MineFragment.getInstance();
                    transaction.add(R.id.fl_container, fragment4, "fourth");
                } else {
                    transaction.show(fragment4);
                }
                break;
            default:
                if (null == fragment1) {
                    fragment1 = HomeFragment.getInstance();
                    transaction.add(R.id.fl_container, fragment1, "main");
                } else {
                    transaction.show(fragment1);
                }
                break;
        }
        transaction.commit();
    }

    private void hideFragments() {
        if (null != fragment1) {
            transaction.hide(fragment1);
        }
        if (null != fragment2) {
            transaction.hide(fragment2);
        }
        if (null != fragment3) {
            transaction.hide(fragment3);
        }
        if (null != fragment4) {
            transaction.hide(fragment4);
        }
    }

    @Override
    public void initData() {
    }

    @Override
    public void observeData() {
    }
}