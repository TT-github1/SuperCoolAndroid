package com.tthappy.supercoolandroid.uidemo.ui.equipmentattribute;

import android.util.DisplayMetrics;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.annotation.HpRouterData;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity;
import com.tthappy.supercoolandroid.uidemo.R;
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal.DeviceEntity;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityDeviceBinding;
import com.tthappy.supercoolandroid.utils.measure.DimenUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/24 15:51
 * Update Date:
 * Modified By:
 * Description: 与本设备有关的一些基础属性或者信息的展示
 */

@HpRouterData(name = "设备基础信息", icon = "R.drawable.icon_double_drag_bar_pentagon", desc = "与本设备有关的一些基础属性或者信息的展示", isNew = false, url = BridgeConstansUIDemo.EQUIPMENTATTRIBUTE)
@Route(path = BridgeConstansUIDemo.EQUIPMENTATTRIBUTE)
public class DeviceActivity extends BaseTitleActivity<ActivityDeviceBinding, DeviceViewModel> {

    private DisplayMetrics metrics;
    private final DeviceAdapter mAdapter = new DeviceAdapter();

    @Nullable
    @Override
    public String providerTitle() {
        return "设备参数";
    }

    @Override
    public void initView() {
        metrics = getResources().getDisplayMetrics();

        viewBinding.rvContainer.setAdapter(mAdapter);
        viewBinding.rvContainer.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void initData() {
        List<DeviceEntity> list = new ArrayList<>();
        list.add(new DeviceEntity("屏幕宽度（像素）", metrics.widthPixels + " px", "暂无"));
        list.add(new DeviceEntity("屏幕高度（像素）", metrics.heightPixels + " px", "暂无"));
        list.add(new DeviceEntity("屏幕适配前宽度（DP）", DimenUtils.px2dp(metrics.widthPixels) + " dp", "暂无"));
        list.add(new DeviceEntity("屏幕适配后宽度（DP）", DimenUtils.px2dp(this, metrics.widthPixels) + " dp", "暂无"));
        list.add(new DeviceEntity("屏幕适配前高度（DP）", DimenUtils.px2dp(metrics.heightPixels) + " dp", "暂无"));
        list.add(new DeviceEntity("屏幕适配后高度（DP）", DimenUtils.px2dp(this, metrics.heightPixels) + " dp", "暂无"));

        mAdapter.setNewInstance(list);
    }

    @Override
    public void observeData() { }
}