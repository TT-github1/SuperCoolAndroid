package com.tthappy.supercoolandroid.java.datastruct.tree;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/21 9:55
 * Update Date:
 * Modified By:
 * Description: 2-3-4树对应的红黑树（2-3-4树的单个节点最多有3个键和4个子节点）
 *              注：本类是根据2-3-4树的插入和删除操作的思想来实现红黑树，
 *              所以某些节点的红黑值可能与直接针对红黑树性质来实现红黑树的节点的红黑值有出入，
 *              但是这并不影响，因为本类仍是一颗正确的红黑树， 仍然符合红黑树的5条性质
 */
public class RedBlackTree<E extends Comparable<E>> {

    public static final boolean RED = true;
    public static final boolean BLACK = false;

    public Node head;

    public void add(E data) {
        head = add(head, data);
        head.color = BLACK;
    }

    private Node add(Node node, E data) {
        if (node == null) return new Node(data, RED);

        // 若当前节点为4-节点（即节点已满），将两条红链涂黑，即将当前节点分解为3个节点
        if (isRed(node.lChild) && isRed(node.rChild)) flipColors(node);

        // 按普通二叉操作数的插入操作插入节点
        int cmp = data.compareTo(node.data);
        if (cmp < 0) {
            node.lChild = add(node.lChild, data);
        } else if (cmp > 0) {
            node.rChild = add(node.rChild, data);
        } else {
            node.data = data;
        }

        // 插入完成后，调整节点关系，使之符合红黑树性质
        // 本来按照2-3-4树对应的红黑树的性质，是允许红色右链的，但这样的话，
        // 当节点插入后我们需要对红黑树第四点性质（不允许连续的两个红色节点相连，指上下节点）
        // 进行的判断就有四种（LL、LR、RL、RR），情况比较多，
        // 所以我们这样操作
        // 当插入节点后，如果发现了单红色右链（即左链为黑），就直接通过旋转，将红色右链转换为红色左链
        // 这样在往回递归及后续插入的情况中就不会存在 单红色右链 的情况
        // 所以只会剩下LL（左子节点为红且左子节点的左子节点也为红）的情况，针对这种情况单独处理就好
        if (isRed(node.rChild) && !isRed(node.lChild)) node = rotateLeft(node);
        // LL的情况
        if (isRed(node.lChild) && isRed(node.lChild.lChild)) node = rotateRight(node);

        return node;
    }

    private Node rotateLeft(Node node) {
        Node rChild = node.rChild;
        node.rChild = rChild.lChild;
        rChild.lChild = node;

        rChild.color = node.color;
        node.color = RED;

        return rChild;
    }

    private Node rotateRight(Node node) {
        Node lChild = node.lChild;
        node.lChild = lChild.rChild;
        lChild.rChild = node;

        lChild.color = node.color;
        node.color = RED;

        return lChild;
    }

    // flip: 快速翻动
    private void flipColors(Node node) {
        node.color = RED;
        node.lChild.color = BLACK;
        node.rChild.color = BLACK;
    }

    private boolean isRed(Node node) {
        return node != null && node.color == RED;
    }

    class Node implements PrintTree.RedBlackPrintNode<E> {

        E data;
        boolean color;
        Node lChild, rChild;

        public Node(E data, boolean color) {
            this.data = data;
            this.color = color;
        }

        @Override
        public E getData() {
            return data;
        }

        @Override
        public PrintTree.PrintNode<E> getLChild() {
            return lChild;
        }

        @Override
        public PrintTree.PrintNode<E> getRChild() {
            return rChild;
        }

        @Override
        public boolean isRed() {
            return color == RED;
        }
    }
}
