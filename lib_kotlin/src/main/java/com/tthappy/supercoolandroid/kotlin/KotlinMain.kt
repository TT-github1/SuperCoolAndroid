package com.tthappy.supercoolandroid.kotlin

class KotlinMain {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            KotlinMain().print { println("hhh") }
        }
    }

    inline fun print(listener: () -> Unit) {
        listener()
    }

}