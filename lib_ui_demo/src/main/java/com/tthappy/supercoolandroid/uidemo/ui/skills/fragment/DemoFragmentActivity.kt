package com.tthappy.supercoolandroid.uidemo.ui.skills.fragment

import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.R
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityFragmentDemoBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/22 16:14
 * Update Date:
 * Modified By:
 * Description:
 */
@HpRouterData(
    name = "FRAGMENT",
    icon = "R.drawable.icon_double_drag_bar_pentagon",
    desc = "FRAGMENT",
    isNew = false,
    url = BridgeConstansUIDemo.FRAGMENT
)
@Route(path = BridgeConstansUIDemo.FRAGMENT)
class DemoFragmentActivity: BaseTitleActivity<ActivityFragmentDemoBinding, BaseViewModel>() {
    override fun initView() {
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.fragment_container, DemoFragmentFragment(R.color.main_black), null)
            .addToBackStack("name")
            .commit()

        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.fragment_container, DemoFragmentFragment(R.color.main_blue), null)
            .addToBackStack("name")
            .commit()

        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.fragment_container, DemoFragmentFragment(R.color.test_green), null)
            .addToBackStack("name")
            .commit()

        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.fragment_container, DemoFragmentFragment(R.color.main_black), null)
            .addToBackStack("name")
            .commit()

        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.fragment_container, DemoFragmentFragment(R.color.main_blue), null)
            .addToBackStack("name")
            .commit()

        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.fragment_container, DemoFragmentFragment(R.color.test_green), null)
            .addToBackStack("name")
            .commit()
    }

    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = "fragment的创建"
}