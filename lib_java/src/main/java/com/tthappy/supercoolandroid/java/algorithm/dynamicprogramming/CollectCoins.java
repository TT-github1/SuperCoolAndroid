package com.tthappy.supercoolandroid.java.algorithm.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/24 16:48
 * Update Date:
 * Modified By:
 * Description: 凑硬币
 */
public class CollectCoins {
    public static void main(String[] args) {
        doMainForce(11);
        doMainForce(14);
        doMainMemory(44);
        doMainMemory(66);
        doMainDynamicProgramming(11);
        doMainDynamicProgramming(66);
    }

    private static List<Integer> getList() {
        List<Integer> coins = new ArrayList<>();
        coins.add(1);
        coins.add(2);
        coins.add(5);
        return coins;
    }

    private static void doMainForce(int amount) {
        System.out.println(collectionByForce(amount, getList()));
    }

    private static void doMainMemory(int amount) {
        int[] dp = new int[amount + 1];
        System.out.println(collectionByMemory(amount, getList(), dp));
    }

    private static void doMainDynamicProgramming(int amount) {
        System.out.println(collectionByDynamicProgramming(amount, getList()));
    }

    private static int collectionByForce(int amount, List<Integer> coins) {
        if (amount < 0) return -1;
        if (amount == 0) return 0;

        int num = Integer.MAX_VALUE;
        for (Integer coin : coins) {
            int sub = collectionByForce(amount - coin, coins);
            if (sub < 0) continue;
            num = Math.min(num, sub + 1);
        }
        if (num < 0 || num == Integer.MAX_VALUE) return -1;
        return num;
    }


    private static int collectionByMemory(int amount, List<Integer> coins, int[] dp) {
        if (amount < 0) return -1;
        if (amount == 0) return 0;


        if (dp[amount] != 0 && dp[amount] != -1) return dp[amount];

        int num = Integer.MAX_VALUE;
        for (Integer coin : coins) {
            int sub = collectionByMemory(amount - coin, coins, dp);
            if (sub < 0) continue;
            num = Math.min(num, sub + 1);
        }
        num = num < 0 || num == Integer.MAX_VALUE ? -1 : num;
        dp[amount] = num;

        return dp[amount];
    }

    private static int collectionByDynamicProgramming(int amount, List<Integer> coins) {
        if (amount < 0) return -1;
        if (amount == 0) return 0;

        int[] dp = new int[amount + 1];
        Arrays.fill(dp, amount + 1);

        dp[0] = 0;

        for (int i = 0; i < amount + 1; i++) {
            for (Integer coin : coins) {
                if (i - coin < 0) continue;
                dp[i] = Math.min(dp[i], dp[i - coin] + 1);
            }
        }

        return dp[amount];
    }
}
