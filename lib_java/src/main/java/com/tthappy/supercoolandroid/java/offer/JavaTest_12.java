package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/28 9:54
 * Update Date:
 * Modified By:
 * Description: 请实现一个函数，将一个字符串中的每个空格替换成“%20”。
 *              例如，当字符串为We Are Happy.则经过替换之后的字符串为 We%20Are%20Happy。
 */
public class JavaTest_12 {
    public static void main(String[] args) {
        String msg = "We are happy";
        System.out.println(replace(msg));
    }

    private static String replace(String msg) {
        StringBuilder res = new StringBuilder();
        for(Character c : msg.toCharArray())
        {
            if(c == ' ') res.append("%20");
            else res.append(c);
        }
        return res.toString();
    }
}
