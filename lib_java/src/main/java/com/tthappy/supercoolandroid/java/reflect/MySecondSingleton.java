package com.tthappy.supercoolandroid.java.reflect;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/30 10:55
 * Update Date:
 * Modified By:
 * Description:
 */
public class MySecondSingleton {

    private MySecondSingleton(){}

    private static class SingletonInstance{
        private static final MySecondSingleton instance = new MySecondSingleton();
    }

    public MySecondSingleton getInstance() {
        return SingletonInstance.instance;
    }

}
