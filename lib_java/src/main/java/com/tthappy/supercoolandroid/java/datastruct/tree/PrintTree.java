package com.tthappy.supercoolandroid.java.datastruct.tree;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/14 10:50
 * Update Date:
 * Modified By:
 * Description:
 */
public class PrintTree<T extends Comparable<T>> {

    public static final boolean RED = true;
    public static final boolean BLACK = false;

    Node head;

    public class Node {
       T data;
       Node lChild;
       Node rChild;

       int width;
       boolean color;

       public Node(T data) {
           this(data, null, null);
       }

       public Node(T data, Node lChild, Node rChild) {
           this.data = data;
           this.lChild = lChild;
           this.rChild = rChild;
       }
    }

    /**
     *  二叉树节点类实现此接口后，可以使用PrintTreeUtils进行二叉树的可视化打印
     */
    public interface PrintNode<E extends Comparable<E>> {
        E getData();
        PrintNode<E> getLChild();
        PrintNode<E> getRChild();
        default void getSomething() {}
    }

    /**
     *  红黑树节点类实现此接口后，可以使用RedBlackPrintTreeUtils进行红黑树的可视化打印
     */
    public interface RedBlackPrintNode<E extends Comparable<E>> extends PrintNode<E> {
        boolean isRed();
    }
}
