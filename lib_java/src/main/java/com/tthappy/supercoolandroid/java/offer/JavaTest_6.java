package com.tthappy.supercoolandroid.java.offer;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/24 11:08
 * Update Date:
 * Modified By:
 * Description: 输入一个正整数数组，把数组里所有数字拼接起来排成一个数，打印能拼接出的所有数字中最小的一个。
 *              例如输入数组{3，32，321}，则打印出这三个数字能排成的最小数字为321323。
 */
public class JavaTest_6 {
    public static void main(String[] args) {
        int[] arr = {14, 13, 25};
        System.out.println(sort(arr));
    }

    private static String sort(int[] arr) {
        if (arr == null || arr.length == 0) return "";

        String[] sArr = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            sArr[i] = String.valueOf(arr[i]);
        }

        Arrays.sort(sArr, (m, n) -> {
            String mn = m + n;
            String nm = n + m;
            return mn.compareTo(nm);
        });

        StringBuilder result = new StringBuilder();
        for (String s : sArr) {
            result.append(s);
        }
        return result.toString();
    }
}
