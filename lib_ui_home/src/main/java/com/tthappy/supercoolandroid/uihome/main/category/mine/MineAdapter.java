package com.tthappy.supercoolandroid.uihome.main.category.mine;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.tthappy.supercoolandroid.uihome.R;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/11 9:56
 * Update Date:
 * Modified By:
 * Description:
 */
public class MineAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public MineAdapter() {
        super(R.layout.item_mine);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, String s) {

    }
}
