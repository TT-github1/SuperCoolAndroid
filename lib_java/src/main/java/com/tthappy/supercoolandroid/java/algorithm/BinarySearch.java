package com.tthappy.supercoolandroid.java.algorithm;

/**
 * 二分查找
 */
public class BinarySearch {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8}; //需要排好序，但不用写排序的算法
        System.out.println(binarySearch(arr, 3));
        System.out.println(bsRecursion(arr, 3, 0, arr.length - 1));
    }

    private static int binarySearch(int[] arr, int target) {
        int min = 0;
        int max = arr.length - 1;
        int mid = (min + max) / 2;
        while (max >= min) {
            if (arr[mid] == target) {
                return mid;
            } else if (arr[mid] > target) {
                max = mid - 1;
            } else {
                min = mid + 1;
            }
            mid = (min + max) / 2;
        }
        return -1;
    }

    private static int bsRecursion(int[] arr, int target, int min, int max) {
        if (min <= max) {
            int mid = (min + max) / 2;
            if (arr[mid] == target) return mid;
            else if (arr[mid] < target) {
                return bsRecursion(arr, target, mid + 1, max);
            } else {
                return bsRecursion(arr, target, min, mid - 1);
            }
        }
        return -1;
    }
}
