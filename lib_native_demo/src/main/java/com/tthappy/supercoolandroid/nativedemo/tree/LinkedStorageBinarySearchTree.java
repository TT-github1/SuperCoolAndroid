package com.tthappy.supercoolandroid.nativedemo.tree;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/1/26 10:59
 * Update Date:
 * Modified By:
 * Description:
 */
public class LinkedStorageBinarySearchTree {
    static {
        System.loadLibrary("nativedemo");
    }

    public native void add(int data);
    public native void showTree();
    public native int contain(int data);
}
