package com.tthappy.supercoolandroid.network.interceptor

import com.tthappy.supercoolandroid.log.HpLog
import okhttp3.Interceptor
import okhttp3.Response
import okio.Buffer

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/10 10:39
 * Update Date:
 * Modified By:
 * Description:
 */
class LogInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        synchronized(LogInterceptor::class.java) {
            HpLog.e("${request.method}  ${request.url}"::toString)
            val buffer = Buffer()
            request.body?.writeTo(buffer)
            HpLog.e(buffer.readUtf8()::toString)
        }



        val response = chain.proceed(request)
        synchronized(LogInterceptor::class.java) {
            HpLog.e(                                           //这个.string()是输出关键
                    response.peekBody(1024 * 1024.toLong()).string()::toString
            )
        }

        return response
    }
}