package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/28 10:37
 * Update Date:
 * Modified By:
 * Description: 在一个字符串(0<=字符串长度<=10000，全部由字母组成)中找到第一个只出现一次的字符,并返回它的位置,
 *              如果没有则返回 -1（需要区分大小写）。
 */
public class JavaTest_13 {
    public static void main(String[] args) {
        String msg = "eeakdknagka";
        System.out.println(find(msg));
    }

    private static int find(String msg) {
        for (int i = 0; i < msg.length(); i++) {
            char c = msg.charAt(i);
            if (msg.indexOf(c) == msg.lastIndexOf(c)) return i;
        }
        return -1;
    }
}
