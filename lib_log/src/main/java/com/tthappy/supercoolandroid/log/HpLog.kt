package com.tthappy.supercoolandroid.log

import com.tthappy.supercoolandroid.log.timber.Timber


/**
 * Author:      tfhe
 * Create Date: Created in 2021/8/4 9:20
 * Update Date:
 * Modified By:
 * Description:
 */
object HpLog {

    fun init(debug: Boolean) {
        if (debug) {
            Timber.plant(HpDebugTree())
        }
    }

//    inline fun w(crossinline message: () -> Any?) = Timber.tag(sGlobalTag).w(createLog(message))
//    inline fun w(tag: String, crossinline message: () -> Any?) = Timber.tag(tag).w(createLog(message))
    fun e(message: String) = e(message::toString)
    inline fun e(crossinline message: () -> Any?) = Timber.tag(sGlobalTag).e(createLog(message))
//    inline fun e(tag: String, crossinline message: () -> Any?) = Timber.tag(tag).e(createLog(message))

    inline fun createLog(message: () -> Any?): String {
        return LogCreator.create(message())
    }
}