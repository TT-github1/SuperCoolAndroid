package com.tthappy.supercoolandroid.java.algorithm.wholesort;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/2 9:52
 * Update Date:
 * Modified By:
 * Description: 全排序
 *
 *
 */

public class WholeSort {

    /**
     *  给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。
     *  示例 1：
     *  输入：nums = [1,2,3]
     *  输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
     *  示例 2：
     *  输入：nums = [0,1]
     *  输出：[[0,1],[1,0]]
     *  示例 3：
     *  输入：nums = [1]
     *  输出：[[1]]
     *  提示：
     *  1 <= nums.length <= 6
     *  -10 <= nums[i] <= 10
     *  nums 中的所有整数 互不相同
     */
    public static void main(String[] args) {
        permute(new int[]{1, 2, 3});

        for (List<Integer> integers : resPermute) {
            for (Integer integer : integers) {
                System.out.print(integer);
            }
            System.out.println();
        }
    }

    public static List<List<Integer>> permute(int[] nums) {
        dfsPermute(nums, new LinkedHashSet<>());
        return resPermute;
    }

    static List<List<Integer>> resPermute = new ArrayList<>();

    public static void dfsPermute(int[] nums, LinkedHashSet<Integer> path) {
        if (path.size() == nums.length) {
            resPermute.add(new ArrayList<>(path));
        }

        for (int num : nums) {
            if (path.contains(num)) {
                continue;
            }

            path.add(num);
            dfsPermute(nums, path);
            path.remove(num);
        }
    }
}