package com.tthappy.supercoolandroid.java.offer;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/5 14:26
 * Update Date:
 * Modified By:
 * Description: 给一个整数数组，使这个整数数组按顺序组成的整数的值加1 即[1, 2] -> [1, 3] ; [2, 9] -> [3, 0]
 */
public class JavaTest_28 {

    private static int[] arr = {1, 1, 1};

    public static void main(String[] args) {
        add(arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

    private static void add(int end) {
        if (arr.length != 0 && end == 0 && arr[end] == 9) {
            arr = new int[arr.length + 1];
            arr[0] = 1;
            return;
        }
        if (arr[end] == 9) {
            arr[end] = 0;
            add(end - 1);
        }
        else arr[end] += 1;
    }
}
