package com.tthappy.supercoolandroid.uidemo.ui.okhttp

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityOkhttpDemoBinding
import okhttp3.*
import okio.IOException

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/13 11:21
 * Update Date:
 * Modified By:
 * Description:
 */
@HpRouterData(name = "okhttp", icon = "R.drawable.icon_double_drag_bar_pentagon", desc = "okhttp", isNew = false, url = BridgeConstansUIDemo.OKHTTP)
@Route(path = BridgeConstansUIDemo.OKHTTP)
class OkhttpActivity: BaseTitleActivity<ActivityOkhttpDemoBinding, OkhttpViewModel>() {

    val handler = object : Handler(Looper.myLooper()!!) {
        override fun handleMessage(msg: Message) {
            viewBinding.text.text = msg.data.getString("values")
        }
    }

    override fun initView() {
        Thread {
            val client = OkHttpClient.Builder().build()
            val url = "https://www.wanandroid.com/friend/json"
            val request = Request.Builder()
                    .url(url)
                    .get()
                    .build()
            val call = client.newCall(request)
            call.enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {

                }

                override fun onResponse(call: Call, response: Response) {
                    handler.sendMessage(
                        Message().apply {
                            data = Bundle().apply {
                                putString("values", response.body?.string())
                            }
                        }
                    )
                }
            })
        }.start()

        Thread {
            val client2 = OkHttpClient.Builder().build()
            val url2 = "https://www.wanandroid.com/friend/json"
            val request2 = Request.Builder()
                    .url(url2)
                    .get()
                    .build()
            val call2 = client2.newCall(request2)
            val response = call2.execute()
            handler.sendMessage(
                    Message().apply {
                        data = Bundle().apply {
                            putString("values", response.body?.string())
                        }
                    }
            )
        }.start()
    }
    override fun initData() {
        viewModel.sbtest()
    }

    override fun observeData() {
        viewModel.result.observe(this) {
            viewBinding.text.text = it.toString()
        }
    }

    override fun providerTitle() = "非常的OK"
}