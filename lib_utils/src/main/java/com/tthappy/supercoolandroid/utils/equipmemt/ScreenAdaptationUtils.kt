package com.tthappy.supercoolandroid.utils.equipmemt

import android.app.Activity
import android.app.Application
import android.content.ComponentCallbacks
import android.content.res.Configuration
import android.util.DisplayMetrics

/**
 *  今日头条的屏幕适配方案
 *  核心思想是按照某个标准（比如按照UI设计图的375px），在某个方向上（比如屏幕宽度），将这个长度平均分到这个标准上
 *  （就比如UI设计图宽度是按375px设计，我们就把屏幕分成375份，一份就是1dp，这样就适配了）
 *  （另外还有一种smallestWidth适配方案，以后研究）
 */

@Suppress("unused")
object ScreenAdaptationUtils {

    const val WIDTH = 0
    const val HEIGHT = 1
    lateinit var displayMetrics: DisplayMetrics
    private var appDensity: Float = 0f
    /**
     * 字体的缩放因子，正常情况下和density相等，但是调节系统字体大小后会改变这个值
     */
    var appScaleDensity: Float = 0f

    @JvmStatic
    fun initAppDensity(application: Application) {
        if (!this::displayMetrics.isInitialized) displayMetrics = application.resources.displayMetrics
        if (appDensity == 0f) {
            appDensity = displayMetrics.density
            appScaleDensity = displayMetrics.scaledDensity

            //添加字体变化的监听（如果不加，当app启动后，去改变系统字体大小，此时app内的页面无法同时改变字体大小，此时就需要杀掉进程重新进入，字体的改变才会生效，
            //添加后，可以当改变系统字体大小时回调下面的函数，使app内的字体同步改变）
            application.registerComponentCallbacks(object : ComponentCallbacks{
                override fun onConfigurationChanged(newConfig: Configuration) {
                    //字体改变后,将appScaledDensity重新赋值
                    if (newConfig.fontScale > 0) {
                        appScaleDensity = displayMetrics.scaledDensity
                    }
                }

                override fun onLowMemory() {}
            })
        }
    }


    /**
     * @param activity 不解释
     * @param orientation 页面使用的屏幕方向
     * @param designLength 设计稿使用的长度（适配的方向上的长度）
     */
    @JvmStatic
    fun resetAppDensity(activity: Activity, orientation: Int, designLength: Float) {
        val displayMetrics = activity.resources.displayMetrics
        /**
         *  dpi是屏幕像素密度，计算方法是
         *  屏幕宽度（单位像素）的平方   加上   屏幕高度（单位像素）的平方   的和   开根号   再除以   屏幕对角线的长度（单位英寸）
         *  其实就是手机屏幕对角线上的像素个数 除以 对角线的长度
         *  density是标准屏幕像素密度，计算方法是 dpi / 160
         */
        val oldDensity = displayMetrics.density

        /**
         *  widthPixels为手机屏幕物理宽度(px)
         *  px转dp的计算方法是 px = dp * density
         *  所以假设designLength入参为360，一个360dp宽的控件，就刚好占一个屏幕
         */
        val newDensity: Float = when(orientation) {
            WIDTH -> displayMetrics.widthPixels / designLength
            HEIGHT -> displayMetrics.heightPixels / designLength
            else -> displayMetrics.widthPixels / designLength
        }

        displayMetrics.density = newDensity


        val oldScaleDensity = displayMetrics.scaledDensity
        // 因为默认情况下density = scaleDensity
        // 所以 newScaleDensity / oldScaleDensity = newDensity / oldDensity
        val newScaleDensity = newDensity * (appScaleDensity / appDensity)
        displayMetrics.scaledDensity = newScaleDensity

        // 这个就是dpi
        val oldDensityDpi = displayMetrics.densityDpi
        val newDensityDpi = newDensity * 160 // 因为 density = dpi / 160, 所以 dpi = density * 160
        displayMetrics.densityDpi = newDensityDpi.toInt()

        setBitmapDefaultDensity(displayMetrics.densityDpi)
    }

    /**
     * 设置 Bitmap 的默认屏幕密度
     * 由于 Bitmap 的屏幕密度是读取配置的，导致修改未被启用
     * 所以，反射方式强行修改
     * @param densityDpi 屏幕密度
     */
    private fun setBitmapDefaultDensity(densityDpi: Int) {
        val clazz: Class<*>
        try {
            clazz = Class.forName("android.graphics.Bitmap")
            val field = clazz.getDeclaredField("sDefaultDensity")
            field.isAccessible = true
            field.set(null, densityDpi)
            field.isAccessible = false
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchFieldException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }
    }

}