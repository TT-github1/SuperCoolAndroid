package com.tthappy.supercoolandroid.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import java.lang.reflect.ParameterizedType

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/8 19:42
 * Update Date:
 * Modified By:
 * Description:
 */

//@JvmName("inflateWithGeneric")
fun <VB: ViewBinding> Any.inflateBindingWithGeneric(layoutInflater: LayoutInflater) =
        withGenericBindingClass<VB>(this) { clazz ->
            clazz.getMethod("inflate", LayoutInflater::class.java).invoke(null, layoutInflater) as VB
        }
//        .also { binding ->
//            if (this is ComponentActivity && binding is ViewDataBinding) {
//                binding.lifecycleOwner = this
//            }
//        }

//@JvmName("inflateWithGeneric")
fun <VB : ViewBinding> Any.inflateBindingWithGeneric(layoutInflater: LayoutInflater, parent: ViewGroup?, attachToParent: Boolean): VB =
        withGenericBindingClass(this) { clazz ->
            clazz.getMethod("inflate", LayoutInflater::class.java, ViewGroup::class.java, Boolean::class.java)
                    .invoke(null, layoutInflater, parent, attachToParent) as VB
        }
//        .also { binding ->
//            if (this is Fragment && binding is ViewDataBinding) {
//                binding.lifecycleOwner = viewLifecycleOwner
//            }
//        }

private fun <VB: ViewBinding> withGenericBindingClass(any: Any, block: (Class<VB>) -> VB): VB {
    any.allParameterizedType.forEach{ parameterizedType ->
        parameterizedType.actualTypeArguments.forEach {
            try {
                return block.invoke(it as Class<VB>)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    throw IllegalArgumentException("There is no generic of ViewBinding.OHHHHHHHHHHHHHH")
}

private val Any.allParameterizedType: List<ParameterizedType>
    get() {
        val genericParameterizedType = mutableListOf<ParameterizedType>()
        var parameterizedType = javaClass.genericSuperclass
        var superClass = javaClass.superclass
        while (superClass != null) {
            if(parameterizedType is ParameterizedType) {
                genericParameterizedType.add(parameterizedType)
            }
            parameterizedType = superClass.genericSuperclass
            superClass = superClass.superclass
        }
        return genericParameterizedType
    }