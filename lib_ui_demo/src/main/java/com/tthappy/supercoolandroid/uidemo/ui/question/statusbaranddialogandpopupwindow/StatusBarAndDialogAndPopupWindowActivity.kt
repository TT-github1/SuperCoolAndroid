package com.tthappy.supercoolandroid.uidemo.ui.question.statusbaranddialogandpopupwindow

import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityStatusBarAndDialogAndPopupWindowDemoBinding
import com.tthappy.supercoolandroid.views.dialog.CommonDialogFactory
import com.tthappy.supercoolandroid.utils.equipmemt.StatusBarUtils

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/22 10:18
 * Update Date:
 * Modified By:
 * Description:
 */

@HpRouterData(name = "状态栏、Dialog、PopupWindow",
    icon = "R.drawable.icon_double_drag_bar_pentagon",
    desc = "状态栏、Dialog、PopupWindow",
    isNew = false,
    url = BridgeConstansUIDemo.STATUSBARANDDIALOGANDPOPUPWINDOW)
@Route(path = BridgeConstansUIDemo.STATUSBARANDDIALOGANDPOPUPWINDOW)
class StatusBarAndDialogAndPopupWindowActivity: BaseTitleActivity<ActivityStatusBarAndDialogAndPopupWindowDemoBinding, BaseViewModel>() {
    override fun initView() {
        initListener()
    }

    private fun initListener() {
        viewBinding.tvHideStatusBar.setOnClickListener {
            StatusBarUtils.hideStatusBar(window)
        }

        viewBinding.tvShowStatusBar.setOnClickListener {
            StatusBarUtils.showStatusBar(window)
        }

        viewBinding.tvStatusBarLightMode.setOnClickListener {
            StatusBarUtils.setStatusBarFontLightMode(window)
        }

        viewBinding.tvStatusBarDarkMode.setOnClickListener {
            StatusBarUtils.setStatusBarFontDarkMode(window)
        }

        viewBinding.tvNormalDialog.setOnClickListener {
            CommonDialogFactory.create(this)
                .title("哈喽")
                .content("Hello World!")
                .show()
        }

    }

    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = "状态栏、Dialog、PopupWindow"
}