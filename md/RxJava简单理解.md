# 从简单例子理解RxJava

我们的目的是通过一个简单的例子，理解RxJava中“事件流”这个过程
这里需要大家在看本文时自己去对照源码配合着来看，源码并不复杂

先上例子

```

val myObserver = object : SingleObserver<String> {
	override fun onSubscribe(d: Disposable) {}
	override fun onSuccess(t: String) { Log.e("TAG", t)}
	override fun onError(e: Throwable) {}
}

Single.just(1)                   //  1  SingleJust   无
	.map { "$it" }               //  2  SingleMap    MapSingleObserver
	.delay(1, TimeUnit.SECONDS)  //  3  SingleDelay  Delay
	.subscribe(myObserver)       //  4  无           myObserver

```

所举的例子很简单，发射一个整数事件，然后转换为字符串，之后延迟1秒发送，最后打印。

从字面上看，就好像每执行了一行代码，就完成了一个功能，就干了一件事。就好像一条目标明确的主线任务，每个节点在干什么我们都一目了然。这对于我们在面对日益复杂的需求仍保证代码逻辑的整洁明了提供了很大帮助。

但很明显，从代码执行顺序层面上看，这里面肯定做了很多文章。

#### 事件流的三个流程
上面的例子从开始到结束可以拆分为三个流程来看待

- 流程一
我们忽略注释4，光看注释1到3行，这三行实际上做的事情是
	- 注释1，创建一个SingleJust对象
	- 注释2，创建一个SingleMap对象，并且把注释1中创建的SingleJust对象传进去
	- 注释3，创建一个SingleDelay对象，并且把注释2中创建的SingleMap对象传进去

没了，就干了这一件事情，就是一层一层地创建对象并且一层一层地包裹起来，让下一层持有上一层的引用
（当然这三行之所以能这样链式调用，是因为SingleJust、SingleMap、SingleDelay都是Single的子类，而方法just、map、delay都是Single的方法）

- 流程二
流程二和流程三都发生在注释4这一行里面，我们先说流程二
	- 首先我们由流程一，得到了一个SingleDelay对象，也就是注释4是在调用SingleDelay对象的subscribe方法。这里值得注意的是，这个subscribe方法的实现在Single类中，里面主要调用了subscribeActual方法，其中还干了一些与本文讨论主题关系不大的事情，我们不去关注，而subscribeActual方法则是由每个Single的子类自己去实现的真正的subscribe逻辑。
	- 在SingleDelay对象的subscribeActual方法里，调用了之前存下来的SingleMap对象的subscribe方法并且传入了一个Delay对象，这个Delay对象是SingleObserver的子类，并且这个Delay对象包裹了我们自定义的myObserver，也就是我们的匿名SingleObserver
	- 然后在SingleMap对象的subscribe方法（subscribeActual方法）里，调用了之前存下来的SingleJust对象的subscribe方法并且传入了一个MapSingleObserver对象，这个MapSingleObserver对象是SingleObserver的子类，并且这个MapSingleObserver对象包裹了上一步的Delay对象
	- 最后在SingleJust对象的subscribe方法（subscribeActual方法）里，调用了传进这个方法里的SingleObserver对象的onSuccess方法，标志着流程二的结束

回看流程二，我们发现，他是逆着流程一的方向，又在一层一层的创建对象，并且包裹起来，这其实相当于是在逐层传递一个一个的回调函数，让上一层持有下一层的回调，在上一层自己的功能逻辑处理完后，可以去回调这个函数

- 流程三
流程二中结尾的onSuccess方法，同时也是流程三的开始
	- 在SingleJust对象的subscribeActual方法中，传进来的SingleObserver对象，就是流程二中倒数第二步的MapSingleObserver对象，所以在这个方法里，主要干的事情，其实就是调用MapSingleObserver对象的onSuccess方法
	- 在MapSingleObserver对象的onSuccess方法里主要干了两件事，一是先完成MapSingleObserver对象自身的逻辑功能，即注释2的map函数的逻辑功能，将整数转换为字符串；二是调用流程二中传进来的Delay对象的onSuccess方法
	- 在Delay对象的onSuccess方法里也是干两件事，一是完成Delay对象自身的逻辑功能，即注释3的delay函数的逻辑功能，延迟1秒发送（如何切换线程不在本文讨论范围）；二是调用流程二中传进来的我们自定义的myObserver的onSuccess方法，即完成打印，流程三结束。整个事件流程结束
在流程三中，我们再一次顺着流程一的方向，一步一步执行功能逻辑并且往下通知下一层，直至整个事件流程结束。

#### 总结
RxJava事件流的整个流程就是从上往下、从下往上最后再从上往下，整个代码执行流程是复杂的，但是我们再使用的时候，它呈现给我们的效果是清晰简洁的

本文属于个人理解，如有错误，还请大家多多指正