package com.tthappy.supercoolandroid.java.algorithm.sort;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/2 14:45
 * Update Date:
 * Modified By:
 * Description: 堆排序
 *
 *              用数组构造大根堆（大顶堆），然后将堆顶元素和堆尾元素交换，之后把这个交换后的堆顶元素视为排除出堆外。
 *              用剩余元素重新构造大根堆，之后重复这个过程，直至数组遍历完成
 */

public class HeapSort {

    public static void main(String[] args) {
        int[] arr = {2, 4, 3, 8};

    }

    public static void sort(int[] arr) {

    }

    public static void adjustHeap(int[] arr, int start, int end) {

    }

    public static void createHeap(int[] arr) {
        for (int i = arr.length / 2 -1; i >= 0; i--) {

        }
    }

}
