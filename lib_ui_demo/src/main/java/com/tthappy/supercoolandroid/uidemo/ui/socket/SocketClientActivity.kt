package com.tthappy.supercoolandroid.uidemo.ui.socket

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Message
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity
import com.tthappy.supercoolandroid.uidemo.databinding.ActivitySocketBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/23 8:45
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.SOCKET_CLIENT)
class SocketClientActivity: BaseActivity<ActivitySocketBinding, SocketViewModel>() {

    private val uiHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            viewBinding.tvServer.x = msg.arg1.toFloat()
            viewBinding.tvServer.y = msg.arg2.toFloat()
        }
    }

    override fun initView() {
        startClient() 

        viewBinding.tvHost.listener = { x, y ->
            val msg = Message.obtain()
            msg.arg1 = x
            msg.arg2 = y
            SocketClientUtils.getHandler().sendMessage(msg)
        }
    }

    private fun startClient() {
        SocketClientUtils.startClient(uiHandler)
    }

    override fun initData() {}
    override fun observeData() {}
}