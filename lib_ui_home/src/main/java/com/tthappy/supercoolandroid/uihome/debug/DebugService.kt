package com.tthappy.supercoolandroid.uihome.debug

import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.application.HpApplication
import com.tthappy.supercoolandroid.base.application.service.IDebugService
import com.tthappy.supercoolandroid.base.router.RouterService
import com.tthappy.supercoolandroid.base.BuildConfig


/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/17 19:33
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = RouterService.DEBUG)
class DebugService: IDebugService {
    override fun init(context: Context?) {
        if (BuildConfig.DEBUG)
        HpApplication.getInstance().registerActivityLifecycleCallbacks(DebugActivityLifecycleCallbacks)
    }
}