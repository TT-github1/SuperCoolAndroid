#### 常量池
- 静态常量池
  - 字面量： String s = "a" 中的 "a"
  - 符号引用：String这个类中的 java.lang.String
  - 类和方法的信息
- 运行时常量池
  - 符号引用对应的直接引用
  - 