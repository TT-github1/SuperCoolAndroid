package com.tthappy.supercoolandroid.uidemo.ui.skills.viewmodel

import com.tthappy.supercoolandroid.mvvm.event.SingleLiveData
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/29 14:43
 * Update Date:
 * Modified By:
 * Description:
 */
class DemoVMTestViewModel: BaseViewModel() {

    fun getDataList() = mutableListOf(
        "萧峰",
        "虚竹",
        "段誉",
        "慕容复",
        "包不同",
        "风波恶"
    )

    val rightFragmentLiveData = SingleLiveData<String>()
    fun notifyRightFragment(msg: String) {
        rightFragmentLiveData.postValue(msg)
    }
}