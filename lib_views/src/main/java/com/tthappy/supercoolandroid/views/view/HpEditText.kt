package com.tthappy.supercoolandroid.views.view

import android.content.Context
import android.graphics.Rect
import android.text.TextUtils
import android.util.AttributeSet
import com.tthappy.supercoolandroid.views.R

/**
 * Author:      tfhe
 * Create Date: Created in 2022/3/31 15:22
 * Update Date:
 * Modified By:
 * Description:
 */
class HpEditText(context: Context, attrs: AttributeSet): AbstractHpEditText(context, attrs) {

    // 输入框中的文本大于这个值，就会显示清除图标
    private var mThresholdLength = 0

    init {
        setIcon(R.drawable.icon_edit_text_delete)
    }

    override fun onTextChanged(text: CharSequence, start: Int, lengthBefore: Int, lengthAfter: Int) {
        if (text.toString().trim().length > mThresholdLength && isFocused) showIcon()
        else hideIcon()
        if (getText() != null && TextUtils.isEmpty(getText().toString().trim()) && TextUtils.equals(text.toString(), " ")) getText()!!.clear()
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        if (focused && text!!.length > mThresholdLength) showIcon()
        else hideIcon()
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
    }

    override fun notifyEvent() {
        text = null
    }

//    fun setThresholdLength(thresholdLength: Int) {
//        mThresholdLength = thresholdLength
//    }

    override fun precondition() = text!!.length > mThresholdLength && isFocused
}