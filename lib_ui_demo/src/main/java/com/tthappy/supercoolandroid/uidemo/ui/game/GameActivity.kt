package com.tthappy.supercoolandroid.uidemo.ui.game

import android.annotation.SuppressLint
import android.view.MotionEvent
import androidx.appcompat.app.AlertDialog
import com.alibaba.android.arouter.facade.annotation.Route
import com.blog.tetris.operation.Operation
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityNewGameBinding

@HpRouterData(name = "俄罗斯方块", icon = "R.drawable.icon_double_drag_bar_pentagon", desc = "俄罗斯方块", url = BridgeConstansUIDemo.GAME_TCS)
@Route(path = BridgeConstansUIDemo.GAME_TCS)
class GameActivity: BaseTitleActivity<ActivityNewGameBinding, BaseViewModel>() {

    override fun initView() {
        initListener()
    }
    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = ""

    @SuppressLint("ClickableViewAccessibility")
    private fun initListener() {
        var downX = 0F
        var downY = 0F
        var direction = 0

        viewBinding.gameView.setOnTouchListener { _, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    downX = event.x
                    downY = event.y
                }
                MotionEvent.ACTION_MOVE -> {
                    //锁定方向
                    if (0 == direction) {
                        if (event.y - downY > 200) direction = 4
                        if ((event.x - downX < -80 || event.x - downX > 80)) direction = 1
                        if (event.y - downY < -200) direction = 2
                    } else {
                        when (direction) {
                            //横向滑动
                            1 -> {
                                if (event.x - downX > viewBinding.gameView.boxSize) {
                                    downX = event.x
                                    Operation.toRight()
                                } else if (event.x - downX < - viewBinding.gameView.boxSize) {
                                    downX = event.x
                                    Operation.toLeft()
                                }
                            }
                            //向上滑动
                            2 -> {
                                direction = -1
                                Operation.deformation()
                            }
                            //向下滑动
                            4 -> {
                                direction = -1
                                Operation.downBottom()
                            }
                        }
                    }
                }
                MotionEvent.ACTION_UP -> {
                    downX = 0F
                    downY = 0F
                    direction = 0
                }
            }
            true
        }


        //游戏变化回调
        Operation.changeListener = object : Operation.ChangeListener {
            override fun onChange() {
                viewBinding.gameView.refresh()
            }

            override fun gameOver(score: Long) {
                runOnUiThread {
                    val dialog = AlertDialog.Builder(this@GameActivity)
                    dialog.setTitle("提示")
                    dialog.setMessage("游戏结束! 得分:$score")
                    dialog.setPositiveButton(
                            "重新开始"
                    ) { _, _ -> Operation.startGame() }
                    dialog.setNegativeButton(
                            "取消"
                    ) { _, _ -> }
                    dialog.setCancelable(false)
                    dialog.show()
                }
            }
        }

        viewBinding.playGame.setOnClickListener {
            Operation.startGame()
        }
    }
}