package com.tthappy.supercoolandroid.baseui.utils

import android.net.Uri
import android.text.TextUtils
import com.alibaba.android.arouter.launcher.ARouter
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/18 9:25
 * Update Date:
 * Modified By:
 * Description:
 */
object BridgeUtils {
    @JvmStatic
    fun bridgeWithUrl(url: String?) {
        if (!TextUtils.isEmpty(url)) {
            ARouter.getInstance()
                .build(Uri.parse(url))
//                .withOptionsCompat(AnimUtils.initTransition())
                .navigation()
        }
    }

    @JvmStatic
    fun bridgeWebView(url: String?) {
        if (!TextUtils.isEmpty(url)) {
            ARouter.getInstance()
                .build(BridgeConstansUIDemo.WEB)
                .withString("url", url)
                .navigation()
        }
    }
}