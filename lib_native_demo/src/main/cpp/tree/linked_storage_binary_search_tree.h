//
// Created by ad on 2022/1/26.
//

#ifndef SUPERCOOLANDROID_LINKED_STORAGE_BINARY_SEARCH_TREE_H
#define SUPERCOOLANDROID_LINKED_STORAGE_BINARY_SEARCH_TREE_H

#include <stdlib.h>

struct LinkedStorageBinarySearchTree;
typedef struct LinkedStorageBinarySearchTree LSBSTree, * Node;
struct LinkedStorageBinarySearchTree {
    Node lChild;
    int data;
    Node rChild;
};

void linkedStorageBinarySearchTree_add(Node * node, int data);

int linkedStorageBinarySearchTree_compare(int data, int treeData);

void linkedStorageBinarySearchTree_node(Node * node, int data);

void linkedStorageBinarySearchTree_preVisitTree(LSBSTree * tree, void (* func) (int));

int linkedStorageBinarySearchTree_contain(LSBSTree * tree, int data);

#endif //SUPERCOOLANDROID_LINKED_STORAGE_BINARY_SEARCH_TREE_H
