package com.tthappy.supercoolandroid.uidemo.ui.recyclerview.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tthappy.supercoolandroid.uidemo.R;

import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/11 17:34
 * Update Date:
 * Modified By:
 * Description:
 */
public class TryRecyclerViewAdapter extends RecyclerView.Adapter<TryRecyclerViewAdapter.ViewHolder> {

    private List<String> list;

    public void setData(List<String> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_try_recycler_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.textView.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_name);
        }
    }
}
