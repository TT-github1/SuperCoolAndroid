package com.tthappy.supercoolandroid.uidemo.data.api

import com.tthappy.supercoolandroid.uidemo.data.entity.entity.base.BaseResult
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal.DemoListEntity
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal.WAZOfficialAccountsEntity
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.wanandroid.ArticlesEntity
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.wanandroid.RegisterRequest
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface UiDemoApi {

    @POST("/user/register")
    fun register(@Body request: RegisterRequest): Observable<BaseResult<Any?>>

    @GET("/article/top/json")
    fun getArticles(): Observable<BaseResult<List<ArticlesEntity>>>

    @GET("/wxarticle/chapters/json")
    fun sbtest(): Observable<BaseResult<List<WAZOfficialAccountsEntity>>>

    @GET("/WebProject_war_exploded/getList")
    fun getList(): Observable<DemoListEntity>

}