package com.tthappy.supercoolandroid.uihome.debug.plugins

import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils
import com.tthappy.supercoolandroid.uihome.debug.DebugPlugin
import com.tthappy.supercoolandroid.uihome.debug.activity.DebugActivity

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/18 16:44
 * Update Date:
 * Modified By:
 * Description:
 */
class UITestPlugin(activity: DebugActivity): DebugPlugin(activity) {
    override fun icon(): Nothing? = null

    override fun name() = "UI测试页面"

    override fun value() = "单纯用来测试UI单纯用来测试UI单纯用来测试UI单纯用来测试UI单纯用来测试UI单纯用来测试UI单纯用来测试UI单纯用来测试UI单纯用来测试UI单纯用来测试UI单纯用来测试UI"

    override fun doWhat() {
        BridgeUtils.bridgeWithUrl(BridgeConstansUIDemo.UITEST)
    }
}