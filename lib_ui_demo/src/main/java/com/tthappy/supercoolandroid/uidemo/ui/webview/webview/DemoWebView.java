package com.tthappy.supercoolandroid.uidemo.ui.webview.webview;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.tthappy.supercoolandroid.uidemo.ui.webview.DemoWebViewActivity;
import com.tthappy.supercoolandroid.uidemo.ui.webview.DemoWebViewInterface;
import com.tthappy.supercoolandroid.uidemo.ui.webview.webchromeclient.DemoWebChromeClient;
import com.tthappy.supercoolandroid.uidemo.ui.webview.webviewclient.DemoWebViewClient;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/10 16:39
 * Update Date:
 * Modified By:
 * Description:
 */
public class DemoWebView extends WebView {
    public DemoWebView(@NonNull Context context) {
        super(context);
        init();
    }

    public DemoWebView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DemoWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public DemoWebView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        initWebSetting();
        setWebChromeClient(new DemoWebChromeClient((DemoWebViewActivity) getContext()));
        setWebViewClient(new DemoWebViewClient());
        initJsInterface();
    }

    private void initWebSetting() {
        WebSettings webSettings = getSettings();
        if (webSettings == null) return;
        // 支持 Js 使用
        webSettings.setJavaScriptEnabled(true);


        //支持屏幕缩放
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        //不显示webview缩放按钮
        webSettings.setDisplayZoomControls(false);


        // 开启DOM缓存
        webSettings.setDomStorageEnabled(true);
        // 开启数据库缓存
        webSettings.setDatabaseEnabled(true);
        // 支持自动加载图片
//        webSettings.setLoadsImagesAutomatically(hasKitkat());
        // 设置 WebView 的缓存模式
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        // 支持启用缓存模式
        webSettings.setAppCacheEnabled(true);
        // 设置 AppCache 最大缓存值(现在官方已经不提倡使用，已废弃)
        webSettings.setAppCacheMaxSize(8 * 1024 * 1024);
        // Android 私有缓存存储，如果你不调用setAppCachePath方法，WebView将不会产生这个目录
//        webSettings.setAppCachePath(contgetCacheDir().getAbsolutePath());
        // 数据库路径
//        if (!hasKitkat()) {
//            webSettings.setDatabasePath(getDatabasePath("html").getPath());
//        }
        // 关闭密码保存提醒功能
        webSettings.setSavePassword(false);
        // 设置 UserAgent 属性
        webSettings.setUserAgentString("");
        // 允许加载本地 html 文件/false
        webSettings.setAllowFileAccess(true);
        // 允许通过 file url 加载的 Javascript 读取其他的本地文件,Android 4.1 之前默认是true，在 Android 4.1 及以后默认是false,也就是禁止
        webSettings.setAllowFileAccessFromFileURLs(false);
        // 允许通过 file url 加载的 Javascript 可以访问其他的源，包括其他的文件和 http，https 等其他的源，
        // Android 4.1 之前默认是true，在 Android 4.1 及以后默认是false,也就是禁止
        // 如果此设置是允许，则 setAllowFileAccessFromFileURLs 不起做用
        webSettings.setAllowUniversalAccessFromFileURLs(false);
    }

    private void initJsInterface() {
        addJavascriptInterface(new DemoWebViewInterface(getContext()), "AndroidUtils");
    }
}
