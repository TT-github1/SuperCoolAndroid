//
// Created by ad on 2022/1/26.
//

#include "linked_storage_binary_search_tree.h"

void linkedStorageBinarySearchTree_add(Node * pNode, int data) {
    Node node = *pNode;
    if (node) {
        int flag = linkedStorageBinarySearchTree_compare(data, node->data);
        if (flag == 0) return;
        else if (flag == -1) {
            if (node->lChild == NULL) {
                linkedStorageBinarySearchTree_node(&(node->lChild), data);
            } else {
                linkedStorageBinarySearchTree_add(&(node->lChild), data);
            }
        } else {
            if (node->rChild == NULL) {
                linkedStorageBinarySearchTree_node(&(node->rChild), data);
            } else {
                linkedStorageBinarySearchTree_add(&(node->rChild), data);
            }
        }
    } else {
        linkedStorageBinarySearchTree_node(pNode, data);
    }
}

int linkedStorageBinarySearchTree_compare(int data, int treeData) {
    if (data == treeData) return 0;
    else if (data < treeData) return -1;
    else return 1;
}

void linkedStorageBinarySearchTree_node(Node * node, int data) {
    *node = (Node) malloc(sizeof(LSBSTree));
    (*node)->lChild = NULL;
    (*node)->data = data;
    (*node)->rChild = NULL;
}

void linkedStorageBinarySearchTree_preVisitTree(LSBSTree * tree, void (* func) (int)) {
    if (!tree) return;
    func(tree->data);
    linkedStorageBinarySearchTree_preVisitTree(tree->lChild, func);
    linkedStorageBinarySearchTree_preVisitTree(tree->rChild, func);
}

int linkedStorageBinarySearchTree_contain(LSBSTree * tree, int data) {
    if (!tree) return 0;
    int flag = linkedStorageBinarySearchTree_compare(data, tree->data);
    if (flag == 0) return 1;
    else if (flag == -1) {
        if (tree->lChild == NULL) {
            return 0;
        } else {
            return linkedStorageBinarySearchTree_contain(tree->lChild, data);
        }
    } else {
        if (tree->rChild == NULL) {
            return 0;
        } else {
            return linkedStorageBinarySearchTree_contain(tree->rChild, data);
        }
    }
}