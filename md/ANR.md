#### 是什么
Application Not Responding 应用程序无响应。系统长时间无法处理完某个操作，会弹出ANR弹框。

#### 会造成ANR的四种场景
- KeyDispatchTimeout： 5秒内无法响应屏幕触摸事件或键盘输入事件 ； 日志关键字：InputDispatching Timeout
- ServiceTimeout： Service在特定的时间内未处理完成导致ANR发生。（限制：前台服务20s；后台服务200s） ； 日志关键字：Timeout executing service
- BroadcastTimeout： BroadcastReceiver在特定时间内未处理完成导致ANR发生（限制：前台广播10s；后台广播60s） ； 日志关键字：Timeout of broadcast BroadcastRecord
- ContentProviderTimeout ： 在10s内未处理完成导致ANR发生 ； 日志关键字：Timeout publishing content providers

#### 排查ANR的方法
- 在logcat中查找ANR的发生信息，包括ANR的包名类名、进程ID、ANR类型（上面四种之一）。还可以查找CPU使用情况信息（CPU usage）
- 查看ANR日志，当发生ANR时，系统会收集ANR相关的日志信息，CPU使用情况，并新生成一个traces.txt文件放在/data/anr/路径下。可以通过adb命令将文件导出
  adb pull /data/anr/<filename>
- 借助三方应用监控平台，在线查看ANR信息