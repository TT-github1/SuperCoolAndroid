package com.tthappy.supercoolandroid.log

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Author:      tfhe
 * Create Date: Created in 2021/8/3 11:30
 * Update Date:
 * Modified By:
 * Description:
 */
object LogCreator {
    fun create(content: Any?): String {
        return decorate(processBody(0, content))
    }

    /**日志装饰器*/
    private fun decorate(content: String): String {
        val sb = StringBuilder()
//                .append(TOP_BORDER).append(LINE_SEP)
//                .append(processHeader()).append(LINE_SEP)
//                .append(MIDDLE_BORDER).append(LINE_SEP)
//                .append(content).append(LINE_SEP)
//                .append(BOTTOM_BORDER)

        sb.append(TOP_BORDER)
        sb.append(LINE_SEP)

        sb.append(processHeader())
        sb.append(LINE_SEP)

        sb.append(MIDDLE_BORDER)
        sb.append(LINE_SEP)

        sb.append(content)
        sb.append(LINE_SEP)

        sb.append(BOTTOM_BORDER)

        return sb.toString()
    }

    /**日志头 - 追踪日志真实打印位置*/
    private fun processHeader(): String {
        val stackTrace = Throwable().stackTrace
        val stackOffset = getStackOffset(stackTrace)
        val targetElement = stackTrace[stackOffset]
        val fileName = targetElement.fileName
        val tName = Thread.currentThread().name
        return "thread:$tName, ${targetElement.methodName}($fileName:${targetElement.lineNumber})"
    }

    /**遍历追踪日志真实打印位置所需的偏移量*/
    private fun getStackOffset(trace: Array<StackTraceElement>): Int {
        var i = MIN_STACK_OFFSET
        while (i < trace.size) {
            val e = trace[i]
            val name = e.className
            if (name != LogCreator::class.java.name && name != HpLog::class.java.name) return i
            i++
        }
        return -1
    }

    /**日志 - 根据类型打印*/
    private fun processBody(level: Int, content: Any?): String {
        val body = when (content) {
            null -> NULL
            is String -> formatJson(content)
            is List<*> -> formatList(level, content)
            else -> content.toString()
        }
        return if (body.isEmpty()) NOTHING else body
    }

    private fun formatJson(json: String): String {
        var jsonStr = json
        try {
            if (json.startsWith("{")) {
                jsonStr = JSONObject(json).toString(4)
            } else if (json.startsWith("[")) {
                jsonStr = JSONArray(json).toString(4)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return jsonStr
    }

    private fun formatList(level: Int, content: List<*>): String {
        val space = StringBuilder()
        for (i in 1..level) {
            space.append("    ")
        }
        val sb = StringBuilder()
        sb.append("[")
        content.forEachIndexed { index, value ->
            sb.append(LINE_SEP)
                    .append(space).append("    ")
                    .append(processBody(level + 1, value))
            if (index != content.size - 1) {
                sb.append(",")
            }
            sb.append(LINE_SEP)
        }
        sb.append(space).append("]")
        return sb.toString()
    }
}