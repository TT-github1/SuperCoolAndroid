#### c与c++之HelloWorld的区别
- c++声明的头文件为 #include <iostream> （注意，没有 .h）， 而不是#include <stdio.h>
- c++需要额外声明使用一个 namespace，比如这样，using namespace std；（具体看下面）
- c++的输出语句为： cout << "Hello World！";


#### c++的命名空间
- c++并不像java，所有的变量和方法都归属到某个类中，c++中的变量和方法都存在于全局命名空间中（在未使用命名空间时），命名空间就类似java中包的作用，限制变量和方法的作用域，避免导致同名的冲突
- 标准c++把自己的整个库都定义在std命名空间中，所以std这个单词不能写错
- 可以定义自己的命名空间，语法为 namespace xxx {}   （大括号内的变量、属性和类就都会属于xxx这个命名空间）
- 但命名空间也不是非用不可，假如不声明使用命名空间，当使用到某个命名空间中的属性或方法，则需要在这个属性或方法前面加上这个命名空间和两个冒号，比如 std::cout << "Hello World"；


#### 编译这个HelloWorld
- 可以使用两种命令， g++ Hello.cpp -o Hello 和 gcc Hello.cpp -lstdc++ -o Hello
- 他们的区别：
	- g++ 会把 .c 文件当做是 C++ 语言 (在 .c 文件前后分别加上 -xc++ 和 -xnone, 强行变成 C++), 从而调用 cc1plus 进行编译.
	- g++ 遇到 .cpp 文件也会当做是 C++, 调用 cc1plus 进行编译. 
	- g++ 还会默认告诉链接器, 让它链接上 C++ 标准库.
	- gcc 会把 .c 文件当做是 C 语言. 从而调用 cc1 进行编译.
	- gcc 遇到 .cpp 文件, 会处理成 C++ 语言. 调用 cc1plus 进行编译. 
	- gcc 默认不会链接上 C++ 标准库.


#### 编译除了一步到位也可以分步编译
- 预处理： -E 生成 .i
- 编译： -S 生成 .s
- 汇编： -c 生成 .o
- 链接： 生成 .exe