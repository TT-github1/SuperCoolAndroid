package com.tthappy.supercoolandroid.views.dialog

import android.content.Context
import android.graphics.Color
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialog
import com.tthappy.supercoolandroid.views.databinding.LayoutPixelDialogBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2022/3/4 15:21
 * Update Date:
 * Modified By:
 * Description:
 */
class HpPixelDialog(context: Context): AppCompatDialog(context) {
    private val viewBinding = LayoutPixelDialogBinding.inflate(layoutInflater)

    init {
        setContentView(viewBinding.root)
        window!!.decorView.setPadding(0, 0, 0, 0 )
        val layoutParams = window!!.attributes
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        window!!.attributes = layoutParams
        window!!.decorView.setBackgroundColor(Color.TRANSPARENT)
    }
}