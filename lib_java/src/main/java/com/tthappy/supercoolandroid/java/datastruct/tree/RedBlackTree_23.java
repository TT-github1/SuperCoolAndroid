package com.tthappy.supercoolandroid.java.datastruct.tree;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/17 9:53
 * Update Date:
 * Modified By:
 * Description: 2-3树对应的红黑树（正常红黑树应该对应2-3-4树）
 */
public class RedBlackTree_23<E extends Comparable<E>> {

    private static final boolean BLACK = false;
    private static final boolean RED = true;

    public Node head;

    public void add(E data) {
        head = add(head, data);
        head.color = BLACK;
    }

    private Node add(Node node, E data) {
        if (node == null) return new Node(data, RED);

//        if (isRed(node.lChild) && isRed(node.rChild)) flipColors(node);

        int cmp = data.compareTo(node.data);
        // 常规BST插入操作
        if (cmp < 0) {
            node.lChild = add(node.lChild, data);
        } else if (cmp > 0) {
            node.rChild = add(node.rChild, data);
        } else {
            node.data = data;
        }

        // 将不符合红黑树性质的节点旋转及调整颜色
        if (isRed(node.rChild) && !isRed(node.lChild)) node = rotateLeft(node);
        if (isRed(node.lChild) && isRed(node.lChild.lChild)) node = rotateRight(node);
        if (isRed(node.lChild) && isRed(node.rChild)) flipColors(node);

        return node;
    }

    public void deleteMin() {
        if (!isRed(head.lChild) && !isRed(head.rChild)) {
            head.color = RED;
        }
        head = deleteMin(head);
        if (!isEmpty()) head.color = BLACK;
    }

    private Node deleteMin(Node node) {
        if (node.lChild == null) return null;

        // 保证当前节点的左子节点不是2-节点
        if (!isRed(node.lChild) && !isRed(node.lChild.lChild)) node = moveRedToLeft(node);
        node.lChild = deleteMin(node.lChild);
        return balance(node);
    }

    public void deleteMax() {
        if (!isRed(head.lChild) && !isRed(head.rChild)) {
            head.color = RED;
        }
        head = deleteMax(head);
        if (!isEmpty()) head.color = BLACK;
    }

    private Node deleteMax(Node node) {
        if (isRed(node.lChild)) node = rotateRight(node);

        if (node.rChild == null) return null;

        if (!isRed(node.rChild) && !isRed(node.rChild.lChild)) node = moveRedToRight(node);
        node.rChild = deleteMax(node.rChild);

        return balance(node);
    }

    public void delete(E data) {
        if (!isRed(head.lChild) && isRed(head.rChild)) {
            head.color = RED;
        }
        head = delete(head, data);
        if (!isEmpty()) head.color = BLACK;
    }

    private Node delete(Node node, E data) {
        if (data.compareTo(node.data) < 0) {
            if (!isRed(node.lChild) && !isRed(node.lChild.lChild)) {
                node = moveRedToLeft(node);
            }
            node.lChild = delete(node.lChild, data);
        } else {
            if (isRed(node.lChild)) node = rotateRight(node);
            if (data.compareTo(node.data) == 0 && (node.rChild == null)) return null;
            if (!isRed(node.rChild) && isRed(node.rChild.lChild)) node = moveRedToRight(node);
            if (data.compareTo(node.data) == 0) {
                node.data = min();
                node.rChild = deleteMin(node.rChild);
            } else node.rChild = delete(node.rChild, data);
        }

        return balance(node);
    }

    private E min() {
        return head == null ? null : min(head);
    }

    private E min(Node node) {
        return node.lChild == null ? node.data : min(node.lChild);
    }

    private Node balance(Node node) {
        if (isRed(node.rChild)) node = rotateLeft(node);

        if (isRed(node.rChild) && !isRed(node.lChild)) node = rotateLeft(node);
        if (isRed(node.lChild) && isRed(node.lChild.lChild)) node = rotateRight(node);
        if (isRed(node.lChild) && isRed(node.rChild)) flipColors(node);

        return node;
    }

    private Node moveRedToLeft(Node node) {
        // 直接将当前节点变成4节点（或5节点）
        contraryFlipColors(node);
        // 如果当前节点的左子节点是2-节点，而它的亲兄弟节点不是2-节点，将左子节点的兄弟节点的一个键移动到左子节点中
        // 实际是将兄弟节点的最小键移动到根节点，然后原根节点移动到左字节点
        if (isRed(node.rChild.lChild)) {
            node.rChild = rotateRight(node.rChild);
            node = rotateLeft(node);
        }
        return node;
    }

    private Node moveRedToRight(Node node) {
        contraryFlipColors(node);

        if (!isRed(node.lChild.lChild)) {
            node = rotateRight(node);
        }
        return node;
    }

    private Node rotateLeft(Node node) {
        Node rChild = node.rChild;
        node.rChild = rChild.lChild;
        rChild.lChild = node;

        rChild.color = node.color;
        node.color = RED;

        return rChild;
    }

    private Node rotateRight(Node node) {
        Node lChild = node.lChild;
        node.lChild = lChild.rChild;
        lChild.rChild = node;

        lChild.color = node.color;
        node.color = RED;

        return lChild;
    }

    private void flipColors(Node node) {
        node.color = RED;
        node.lChild.color = BLACK;
        node.rChild.color = BLACK;
    }

    private void contraryFlipColors(Node node) {
        node.color = BLACK;
        node.lChild.color = RED;
        node.rChild.color = RED;
    }

    private boolean isRed(Node node) {
        if (node == null) return false;
        return node.color == RED;
    }

    public boolean isEmpty() {
        return head == null;
    }

    class Node implements PrintTree.RedBlackPrintNode<E> {
        boolean color;
        E data;
        Node lChild, rChild;

        public Node(E data, boolean color) {
            this.data = data;
            this.color = color;
        }

        @Override
        public E getData() {
            return data;
        }

        @Override
        public PrintTree.PrintNode<E> getLChild() {
            return lChild;
        }

        @Override
        public PrintTree.PrintNode<E> getRChild() {
            return rChild;
        }

        @Override
        public boolean isRed() {
            return color == RED;
        }
    }
}
