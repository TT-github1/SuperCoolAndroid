########################################### 通用 ##########################################
# Entity
-keep class com.tthappy.supercoolandroid.entities.** {*;}

########################################## 三方库 ##########################################

# ARouter
-keep public class com.alibaba.android.arouter.routes.**{*;}
-keep public class com.alibaba.android.arouter.facade.**{*;}
-keep class * implements com.alibaba.android.arouter.facade.template.ISyringe{*;}
-keep interface * implements com.alibaba.android.arouter.facade.template.IProvider
-keep class * implements com.alibaba.android.arouter.facade.template.IProvider
-keep class * implements com.alibaba.android.arouter.facade.template.IInterceptor{*;}



########################################## 实体类 ##########################################

######################################## 自定义控件 ########################################

########################################## 泛型 ###########################################