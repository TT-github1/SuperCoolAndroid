package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/28 13:42
 * Update Date:
 * Modified By:
 * Description: “student. a am I”  ==>  “I am a student.”
 */
public class JavaTest_15 {
    public static void main(String[] args) {
        String msg = "happy. am I";
//        String msg = "apple. an is This";
        System.out.println(reverse(msg));
    }

    private static String reverse(String msg) {
        StringBuilder res = new StringBuilder();
        int left = msg.toCharArray().length;
        char[] msgArr = msg.toCharArray();
        for (int i = msgArr.length - 1; i >= 0; i--) {
            if (msgArr[i] == ' ') {
                res.append(msgArr, i + 1, left - i - 1).append(' ');
                if (i > 0) left = i;
            }
            if (i == 0) res.append(msgArr, 0, left);
        }
        return res.toString();
    }
}
