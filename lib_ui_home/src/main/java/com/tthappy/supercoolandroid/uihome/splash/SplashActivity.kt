package com.tthappy.supercoolandroid.uihome.splash

import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity
import com.tthappy.supercoolandroid.uihome.databinding.ActivitySplashBinding
import com.tthappy.supercoolandroid.views.dialog.HpPixelDialog
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

/**
 * Author:      tfhe
 * Create Date: Created in 2022/3/3 17:37
 * Update Date:
 * Modified By:
 * Description: 闪屏页
 */
class SplashActivity: BaseActivity<ActivitySplashBinding, SplashViewModel>() {
    override fun initView() {
        val d = Observable.timer(3, TimeUnit.SECONDS)
            .subscribe {
                BridgeUtils.bridgeWithUrl(BridgeConstansUIDemo.FLEXBOXLAYOUT)
            }

        val dialog = HpPixelDialog(this)
        dialog.show()
    }

    override fun initData() {
        val aaa = "live cycle"
        val bbb = "can`t see you anymore"
        val ccc = "repeat again book android memory out of heap replace flex box layout queue dump"
        val ddd = "gc scan survivor superman superwoman super height fifth huge small are you sure actually google onion piano soccer ball"
        val eee = "no pixel studio sprite"
        val fff = "dalvik Linear Alloc just do it now receive object"
        val ggg = "ggg kao wtf this is talking local thread root mark holy shit model module java kotlin application thread "
    }

    override fun observeData() {

    }
}