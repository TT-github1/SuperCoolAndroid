package com.tthappy.supercoolandroid.uidemo.ui.recyclerview.listview;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity;
import com.tthappy.supercoolandroid.log.HpLog;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityTryListViewBinding;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/11 11:42
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.LISTVIEW)
public class TryListViewActivity extends BaseTitleActivity<ActivityTryListViewBinding, TryListViewViewModel> {
    @Nullable
    @Override
    public String providerTitle() {
        return "ListView试炼";
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {
        viewModel.getList();
    }

    @Override
    public void observeData() {
        viewModel.listLiveData.observe(this, strings -> {
            for (String string : strings) {
                HpLog.INSTANCE.e(string::toString);
            }
        });
    }

    @Nullable
    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return super.onRetainCustomNonConfigurationInstance();
    }
}
