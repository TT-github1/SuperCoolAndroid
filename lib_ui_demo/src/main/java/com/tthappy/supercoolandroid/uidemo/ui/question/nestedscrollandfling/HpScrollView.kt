package com.tthappy.supercoolandroid.uidemo.ui.question.nestedscrollandfling

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.LinearLayout

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/26 10:58
 * Update Date:
 * Modified By:
 * Description:
 */
class HpScrollView(context: Context, attrs: AttributeSet): LinearLayout(context, attrs) {

    // 上一次触摸事件的y坐标
    private var lastY = 0f

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!super.onTouchEvent(event)) {
            val eventY = event.y

            when(event.actionMasked) {
                MotionEvent.ACTION_DOWN -> lastY = eventY
                MotionEvent.ACTION_MOVE -> {
                    val dy = lastY - eventY
                    scrollTo(0, (scrollY + dy).toInt())
                    lastY = eventY
                }
                else -> {}
            }
            return true
        }

        return false
    }

    override fun scrollTo(x: Int, y: Int) {
        val realHeight = getRealHeight()
        val newY = if (y < 0) 0
        else if (y > realHeight) realHeight
        else y
        super.scrollTo(x, newY)
    }

    private fun getRealHeight(): Int {
        var height = 0
        for(index in 0 until childCount) {
            height += getChildAt(index).height
        }
        return height - this.height
    }
}