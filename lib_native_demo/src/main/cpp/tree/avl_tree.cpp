//
// Created by ad on 2022/1/27.
//

#include "avl_tree.h"

Node avlTree_add(Node node, int data) {
    if (node) {
        int flag = avlTree_compare(node->data, data);

        if (flag < 0) {
            node->lChild = avlTree_add(node->lChild, data);

            if (avlTree_getHeight(node->lChild) - avlTree_getHeight(node->rChild) == 2) {
                if (data < node->lChild->data) node = avlTree_LL(node);
                else node = avlTree_LR(node);
            }

        } else if (flag > 0) {
            node->rChild = avlTree_add(node->rChild, data);

            if (avlTree_getHeight(node->lChild) - avlTree_getHeight(node->rChild) == -2) {
                if (data > node->rChild->data) node = avlTree_RR(node);
                else node = avlTree_RL(node);
            }

        } else {
            printf("插入重复数据，数据为： %d", data);
        }

    } else {
        node = avlTree_node(data);
    }

    node->height = MAX(avlTree_getHeight(node->lChild), avlTree_getHeight(node->rChild)) + 1;

    return node;
}

Node avlTree_node(int data) {
    Node node = (Node)malloc(sizeof(AVLTree));
    node->height = 0;
    node->lChild = nullptr;
    node->rChild = nullptr;
    node->data = data;
    return node;
}

int avlTree_compare(int nodeValue, int data) {
    return data - nodeValue;
}

int avlTree_getHeight(Node node) {
    return node ? node->height : 0;
}

Node avlTree_LL(Node node) {
    Node newNode;

    newNode = node->lChild;
    node->lChild = newNode->rChild;
    newNode->rChild = node;

    node->height = MAX(avlTree_getHeight(node->lChild), avlTree_getHeight(node->rChild)) + 1;
    newNode->height = MAX(avlTree_getHeight(newNode->lChild), node->height) + 1;

    return newNode;
}

Node avlTree_RR(Node node) {
    Node newNode;

    newNode = node->rChild;
    node->rChild = newNode->lChild;
    newNode->lChild = node;

    node->height = MAX(avlTree_getHeight(node->lChild), avlTree_getHeight(node->rChild)) + 1;
    newNode->height = MAX(node->height, avlTree_getHeight(newNode->rChild)) + 1;

    return newNode;
}

Node avlTree_LR(Node node) {
    node->lChild = avlTree_RR(node->lChild);
    return avlTree_LL(node);
}

Node avlTree_RL(Node node) {
    node->rChild = avlTree_LL(node->rChild);
    return avlTree_RR(node);
}

void avlTree_preOrder(AVLTree * tree, void (* visit)(int)) {
    if (tree == nullptr) return;

    visit(tree->data);
    avlTree_preOrder(tree->lChild, visit);
    avlTree_preOrder(tree->rChild, visit);
}

Node avlTree_remove(Node node, int data) {
    if (node == nullptr) return nullptr;

    if (data < node->data) {

        node->lChild = avlTree_remove(node->lChild, data);
        if (avlTree_getHeight(node->lChild) - avlTree_getHeight(node->rChild) == -2) {
            if (avlTree_getHeight(node->rChild->lChild) > avlTree_getHeight(node->rChild->rChild)) node = avlTree_RL(node);
            else node = avlTree_RR(node);
        }

    } else if (data > node->data) {

        node->rChild = avlTree_remove(node->rChild, data);
        if (avlTree_getHeight(node->lChild) - avlTree_getHeight(node->rChild) ==  2) {
            if (avlTree_getHeight(node->lChild->rChild) > avlTree_getHeight(node->lChild->lChild)) node = avlTree_LR(node);
            else node = avlTree_LL(node);
        }

    } else {

        if (node->lChild != nullptr && node->rChild != nullptr) {

            if (avlTree_getHeight(node->lChild) > avlTree_getHeight(node->rChild)) {
                Node max = avlTree_getMax(node->lChild);
                node->data = max->data;
                node->lChild = avlTree_remove(node->lChild, max->data);
            } else {
                Node min = avlTree_getMin(node->rChild);
                node->data = min->data;
                node->rChild = avlTree_remove(node->rChild, min->data);
            }

        } else {
            Node tmp = node;
            node = node->lChild != nullptr ? node->lChild : node->rChild;
            free(tmp);
        }

    }

    return node;
}

Node avlTree_getMax(Node tree) {
    return tree->rChild == nullptr ? tree : avlTree_getMax(tree->rChild);
}

Node avlTree_getMin(Node tree) {
    return tree->lChild == nullptr ? tree : avlTree_getMin(tree->lChild);
}