package com.tthappy.supercoolandroid.utils.toast;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.core.widget.TextViewCompat;

import com.tthappy.supercoolandroid.base.application.HpApplication;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/4/23 10:05
 * Update Date:
 * Modified By:
 * Description:
 */
@SuppressWarnings("all")
public class ToastUtils {

    private static int COLOR_DEFAULT = 0xFEFFFFFF;
    private static int mMsgColor = COLOR_DEFAULT;
    private static int mGravity = -1;
    private static int mXOffset = -1;
    private static int mYOffset = -1;
    private static int mBgResource = -1;
    private static int mBgColor = COLOR_DEFAULT;
    private static Toast mToast;

    private static final Handler HANDLER = new Handler(Looper.getMainLooper());

    private ToastUtils() {
        throw new UnsupportedOperationException("Are you sb, wa ka ka!");
    }

    public static void showShort(@NonNull final CharSequence text) {
        show(text, Toast.LENGTH_SHORT);
    }

    public static void showLong(@NonNull final CharSequence text) {
        show(text, Toast.LENGTH_LONG);
    }

    private static void show(final CharSequence text, final int duration) {
        HANDLER.post(() -> {
            cancel();
            mToast = Toast.makeText(HpApplication.getInstance(), text, duration);
            View view = mToast.getView();
            if (null == view) return;
            TextView tvMessage = view.findViewById(android.R.id.message);
            int msgColor = tvMessage.getCurrentTextColor();

            TextViewCompat.setTextAppearance(tvMessage, android.R.style.TextAppearance);
            tvMessage.setTextColor(mMsgColor != COLOR_DEFAULT ? mMsgColor : msgColor);

            if (mGravity != -1 || mXOffset != -1 || mYOffset != -1) {
                mToast.setGravity(mGravity, mXOffset, mYOffset);
            }
            setBg();
            mToast.show();
        });
    }

    private static void setBg() {
        View toastView = mToast.getView();
        if (mBgResource != -1) {
            if (toastView != null) {
                toastView.setBackgroundResource(mBgResource);
            }
        } else if (mBgColor != COLOR_DEFAULT) {
            if (toastView != null) {
                Drawable background = toastView.getBackground();
                if (null != background) {
                    background.setColorFilter(new PorterDuffColorFilter(mBgColor, PorterDuff.Mode.SRC_IN));
                } else {
                    ViewCompat.setBackground(toastView, new ColorDrawable(mBgColor));
                }
            }

        }
    }

    public static void cancel() {
        if (null != mToast) {
            mToast.cancel();
            mToast = null;
        }
    }
}
