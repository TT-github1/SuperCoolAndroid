package com.tthappy.supercoolandroid.uidemo.wanandroidui.general.articles

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tthappy.supercoolandroid.uidemo.R
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.wanandroid.ArticlesEntity

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/13 14:29
 * Update Date:
 * Modified By:
 * Description:
 */
class ArticlesAdapter: BaseQuickAdapter<ArticlesEntity, BaseViewHolder>(R.layout.item_articles) {
    override fun convert(holder: BaseViewHolder, item: ArticlesEntity) {
        holder.setText(R.id.tv_website, item.author)
        holder.setText(R.id.tv_time, item.niceDate)
        holder.setText(R.id.tv_title, item.title)
    }
}