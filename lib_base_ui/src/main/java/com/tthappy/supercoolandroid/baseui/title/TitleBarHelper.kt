package com.tthappy.supercoolandroid.baseui.title

import android.app.Activity
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import com.tthappy.supercoolandroid.baseui.R

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/10 17:36
 * Update Date:
 * Modified By:
 * Description:
 */
@Suppress("unused")
class TitleBarHelper(private val toolbar: Toolbar) {
    private val mBaseContainer: ConstraintLayout
    private val leftContainer: LinearLayout
    private val rightContainer: LinearLayout
    private val mTitle: TextView?
    private val mStatusBarMarginView: View

    fun hideToolBar() {
        toolbar.visibility = View.GONE
    }

    fun enableTransparentToolbar() {
        toolbar.setBackgroundResource(android.R.color.transparent)
    }

    fun setBackgroundRes(@DrawableRes resId: Int) {
        toolbar.setBackgroundResource(resId)
    }

    fun setBackgroundColor(@ColorRes color: Int) {
        toolbar.setBackgroundColor(toolbar.context.getColor(color))
    }

    fun setTitle(@StringRes resId: Int) {
        mTitle?.setText(resId)
    }

    fun setTitle(charSequence: CharSequence?) {
        if (null != mTitle) {
            mTitle.text = charSequence
        }
    }

    fun setTitleTextColor(@ColorRes color: Int) {
        mTitle?.setTextColor(toolbar.context.getColor(color))
    }

    fun addLeftImageView(@DrawableRes imageResourceId: Int, @IdRes viewId: Int): ImageView {
        val imageView: ImageView = ImageButton(toolbar.context)
        imageView.setBackgroundColor(Color.TRANSPARENT)
        imageView.setImageResource(imageResourceId)
        addLeftView(imageView, viewId)
        return imageView
    }

    fun addRightImageButton(@DrawableRes imageResourceId: Int, @IdRes viewId: Int): ImageButton {
        val imageButton = ImageButton(toolbar.context)
        imageButton.setBackgroundColor(Color.TRANSPARENT)
        imageButton.setImageResource(imageResourceId)
        addRightView(imageButton, viewId)
        return imageButton
    }

    fun addLeftTextView(text: String?, @IdRes viewId: Int): TextView {
        val textView = TextView(toolbar.context)
        textView.text = text
        addLeftView(textView, viewId)
        return textView
    }

    fun addLeftTextView(@StringRes stringRes: Int, @IdRes viewId: Int): TextView {
        val textView = TextView(toolbar.context)
        textView.setText(stringRes)
        addLeftView(textView, viewId)
        return textView
    }

    fun addLeftTextView(@StringRes stringRes: Int, @DrawableRes drawableRes: Int, @IdRes viewId: Int): TextView {
        val textView = addLeftTextView(stringRes, viewId)
        val drawableLeft = ContextCompat.getDrawable(toolbar.context, drawableRes)
        textView.setCompoundDrawablesWithIntrinsicBounds(drawableLeft, null, null, null)
        textView.compoundDrawablePadding = 4
        return textView
    }

    fun addRightTextView(@StringRes stringRes: Int, @IdRes viewId: Int): TextView {
        val textView = TextView(toolbar.context)
        textView.setText(stringRes)
        addRightView(textView, viewId)
        return textView
    }

    fun addRightTextView(text: String?, @IdRes viewId: Int): TextView {
        val textView = TextView(toolbar.context)
        textView.text = text
        addRightView(textView, viewId)
        return textView
    }

    fun addRightTextView(@StringRes stringRes: Int, @DrawableRes drawableRes: Int, @IdRes viewId: Int): TextView {
        val textView = addRightTextView(stringRes, viewId)
        val drawableRight = ContextCompat.getDrawable(toolbar.context, drawableRes)
        textView.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableRight, null)
        textView.compoundDrawablePadding = 4
        return textView
    }

    fun addLeftView(view: View, viewId: Int) {
        val viewLayoutParams = view.layoutParams
        val layoutParams: LinearLayout.LayoutParams = if (viewLayoutParams is LinearLayout.LayoutParams) {
            viewLayoutParams
        } else {
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        }
        addLeftView(view, viewId, layoutParams)
    }

    private fun addLeftView(view: View, viewId: Int, layoutParams: ViewGroup.LayoutParams) {
        view.id = viewId
        leftContainer.addView(view, layoutParams)
    }

    fun addRightView(view: View, viewId: Int) {
        val viewLayoutParams = view.layoutParams
        val layoutParams: LinearLayout.LayoutParams = if (viewLayoutParams is LinearLayout.LayoutParams) {
            viewLayoutParams
        } else {
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        }
        addRightView(view, viewId, layoutParams)
    }

    private fun addRightView(view: View, viewId: Int, layoutParams: ViewGroup.LayoutParams) {
        view.id = viewId
        rightContainer.addView(view, 0, layoutParams)
    }

    fun enableBack(activity: Activity) {
        enableBack(activity, R.drawable.lib_base_ui_icon_back)
    }

    fun enableBack(activity: Activity, @DrawableRes drawableRes: Int = R.drawable.lib_base_ui_icon_back) {
        val iv: View = addLeftImageView(drawableRes, R.id.lib_base_ui_toolbar_left_text_btn)
        iv.setOnClickListener { activity.onBackPressed() }
        leftContainer.setOnClickListener { activity.onBackPressed() }
    }

    fun enableDestroyBackBtn(activity: Activity): View {
        val iv: View = addLeftImageView(R.drawable.lib_base_ui_icon_back, R.id.lib_base_ui_toolbar_left_text_btn)
        iv.setOnClickListener { activity.onBackPressed() }
        return iv
    }

    fun enableRightBtn(@StringRes rightText: Int): TextView {
        val rightTextView = addRightTextView(rightText, R.id.lib_base_ui_toolbar_right_text_btn)
        rightTextView.setTextColor(toolbar.context.getColor(R.color.main_blue))
        return rightTextView
    }

    fun enableRightBtn(rightText: String?): TextView {
        val rightTextView = addRightTextView(rightText, R.id.lib_base_ui_toolbar_right_text_btn)
        rightTextView.setTextColor(toolbar.context.getColor(R.color.main_blue))
        return rightTextView
    }

    fun enableRightBtn(rightText: String?, listener: View.OnClickListener?): TextView {
        val rightTextView = addRightTextView(rightText, R.id.lib_base_ui_toolbar_right_text_btn)
        rightTextView.setTextColor(toolbar.context.getColor(R.color.main_blue))
        listener?.let {
            rightTextView.setOnClickListener(it)
        }
        return rightTextView
    }

    fun enableLeftBtn(@StringRes leftText: Int): TextView {
        val leftTextView = addLeftTextView(leftText, R.id.lib_base_ui_toolbar_left_text_btn)
        leftTextView.setTextColor(toolbar.context.getColor(R.color.main_blue))
        return leftTextView
    }

    fun enableLeftBtn(leftText: String?): TextView {
        val leftTextView = addLeftTextView(leftText, R.id.lib_base_ui_toolbar_left_text_btn)
        leftTextView.setTextColor(toolbar.context.getColor(R.color.main_blue))
        return leftTextView
    }

    /**
     * 在Toolbar与状态栏之间绘制一个空View,高度与状态栏相同，以达到排除状态栏侵入
     *
     * @param immersive 是否侵入
     */
    fun setImmersive(immersive: Boolean) {
        if (!immersive) {
            val statusBarHeightRes =
                    toolbar.resources.getIdentifier("status_bar_height", "dimen", "android")
            if (statusBarHeightRes > 0) {
                //根据资源ID获取响应的尺寸值
                val statusBarHeight =
                        toolbar.resources.getDimensionPixelSize(statusBarHeightRes)
                val set = ConstraintSet()
                set.clone(mBaseContainer)
                set.constrainHeight(R.id.toolbar_status_bar_margin, statusBarHeight)
                set.applyTo(mBaseContainer)
            }
        }
    }


    companion object {
        private const val DEFAULT_VIEW_ID = -1
    }

    init {
        requireNotNull(toolbar) { "Layout file is required to include a Toolbar with id: toolbar" }
        mBaseContainer = toolbar.findViewById(R.id.toolbar_base_container)
        leftContainer = toolbar.findViewById(R.id.toolbar_left_container)
        rightContainer = toolbar.findViewById(R.id.toolbar_right_container)
        mTitle = toolbar.findViewById(R.id.toolbar_title)
        mStatusBarMarginView = toolbar.findViewById(R.id.toolbar_status_bar_margin)
        setImmersive(false)
    }
}