package com.tthappy.supercoolandroid.log.service

import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.application.service.IHpLogService
import com.tthappy.supercoolandroid.base.router.RouterService
import com.tthappy.supercoolandroid.log.HpLog

@Route(path = RouterService.HPLOG)
class HpLogService : IHpLogService {
    override fun init(context: Context?) {
        HpLog.init(true)
    }
}