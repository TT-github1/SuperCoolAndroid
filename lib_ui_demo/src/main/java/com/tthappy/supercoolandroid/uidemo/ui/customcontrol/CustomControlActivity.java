package com.tthappy.supercoolandroid.uidemo.ui.customcontrol;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;


/**
 * Author:      tfhe
 * Create Date: Created in 2020/10/12 11:44
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.SHOW_CUSTOM_CONTROL)
public class CustomControlActivity extends AppCompatActivity {

    private CustomControlAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_control_demo);

        RecyclerView recyclerView = findViewById(R.id.rv_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new CustomControlAdapter();
        recyclerView.setAdapter(mAdapter);

        MyDefaultItemAnimator animator = new MyDefaultItemAnimator();
        animator.setAddDuration(3000);
        animator.setRemoveDuration(3000);
        animator.setChangeDuration(3000);
        animator.setMoveDuration(3000);
        recyclerView.setItemAnimator(animator);

        findViewById(R.id.tv_akjdsklf).setOnClickListener(v -> {
            mAdapter.diffChangeListData();
        });
        findViewById(R.id.tv_aksdfajfj).setOnClickListener(v -> {
//            mAdapter.normalChangeListData();
//            RecyclerViewScrollHelper.scrollToPosition(recyclerView, 20);
        });
    }

    public static class RecyclerViewScrollHelper {
        public static void scrollToPosition(RecyclerView recyclerView, int position){
            RecyclerView.LayoutManager manager1 = recyclerView.getLayoutManager();
            if (manager1 instanceof LinearLayoutManager) {
                LinearLayoutManager manager = (LinearLayoutManager) manager1;
                final TopSmoothScroller mScroller = new TopSmoothScroller(recyclerView.getContext());
                mScroller.setTargetPosition(position);
                manager.startSmoothScroll(mScroller);
            }
        }

        public static class TopSmoothScroller extends LinearSmoothScroller {
            TopSmoothScroller(Context context) {
                super(context);
            }
            @Override
            protected int getHorizontalSnapPreference() {
                return SNAP_TO_START;
            }
            @Override
            protected int getVerticalSnapPreference() {
                return SNAP_TO_START;
            }
        }
    }
}
