package com.tthappy.supercoolandroid.log.timber;

import android.util.Log;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/14 14:46
 * Update Date:
 * Modified By:
 * Description:
 */
public class SimpleTimber {



    public static abstract class Tree {

        final ThreadLocal<String> explicitTag = new ThreadLocal<>();


        String getTag() {
            String tag = explicitTag.get();
            if (tag != null) explicitTag.remove();
            return tag;
        }

        public void e(String message, Object... args) {
            prepareLog(Log.ERROR, null, message, args);
        }

        private void prepareLog(int priority, Throwable throwable, String message, Object... args) {
            String tag = getTag();
            log(priority, throwable, message, tag);
        }

        protected abstract void log(int priority, Throwable throwable, String message, String tag);

    }
}
