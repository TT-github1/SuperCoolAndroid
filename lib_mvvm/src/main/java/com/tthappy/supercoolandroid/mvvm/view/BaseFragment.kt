package com.tthappy.supercoolandroid.mvvm.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.viewbinding.ViewBinding
import com.tthappy.supercoolandroid.log.HpLog
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.views.inflateBindingWithGeneric

/**
 * Author:      A-mew
 * Create Date: Created in 2020/9/4 14:58
 * Update Date:
 * Modified By:
 * Description:
 */
abstract class BaseFragment<VB : ViewBinding, VM : BaseViewModel> : Fragment() {

    protected lateinit var viewModel: VM
    private var realViewBinding: VB? = null
    val viewBinding: VB get() = realViewBinding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        realViewBinding = inflateBindingWithGeneric(layoutInflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initMainViewModel()
        initExtraViewModels()
        super.onViewCreated(view, savedInstanceState)
        initView()
        initData()
        observeData()

        initParentObserver()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        realViewBinding = null
    }

    @Suppress("UNCHECKED_CAST")
    private fun initMainViewModel() {
        try {
            viewModel = (activity as BaseActivity<*, *>).run {
                provideViewModel()
            } as VM
        } catch (e: Exception) {
            HpLog.e(e::toString)
            throw IllegalArgumentException("check viewModel type: $viewModel")
        }
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        checkCurrentVisible(true)
    }

    private fun initParentObserver() {
        lifecycle.addObserver(LifecycleEventObserver { _, event ->
            mCurrentLifeCycleEvent = event
            checkCurrentVisible(false)
        })

        if (parentFragment is BaseFragment<*, *>) {
            (parentFragment as BaseFragment<*, *>).fragmentVisibilityChangedListener = { visible ->
                if (visible && !currentVisible && isVisible && (parentFragment as BaseFragment<*, *>).isVisible && mCurrentLifeCycleEvent == Lifecycle.Event.ON_RESUME) {
                    currentVisible = true
                    fragmentVisibilityChangedListener?.invoke(true)
                }
            }
        }
    }

    private var currentVisible = false
    private var mCurrentLifeCycleEvent: Lifecycle.Event? = null

    private fun checkCurrentVisible(transitionChange: Boolean) {
        if (currentVisible) {
            if (transitionChange && !isVisible) {
                currentVisible = false
                hideChildren()
            }
            if (!transitionChange && (mCurrentLifeCycleEvent == Lifecycle.Event.ON_PAUSE || mCurrentLifeCycleEvent == Lifecycle.Event.ON_STOP)) {
                currentVisible = false
                hideChildren()
            }
        } else {
            if (transitionChange && isVisible) {
                currentVisible = true
                fragmentVisibilityChangedListener?.invoke(true)
            }
            if (!transitionChange && mCurrentLifeCycleEvent == Lifecycle.Event.ON_RESUME && isVisible
                && (parentFragment == null || requireParentFragment().isVisible)
                && (parentFragment == null || requireParentFragment().parentFragment == null || requireParentFragment().requireParentFragment().isVisible)
            ) {
                currentVisible = true
                fragmentVisibilityChangedListener?.invoke(true)
            }
        }
    }

    private fun hideChildren() {
        val fragments =
            this.childFragmentManager.fragments
        for (childFragment in fragments) {
            if (childFragment is BaseFragment<*, *>) {
                childFragment.onFragmentVisibilityChanged(false)
            }
        }
    }

    open fun onFragmentVisibilityChanged(visible: Boolean) {
        if (visible != currentVisible) {
            currentVisible = visible
        }
    }

    abstract fun initView()
    abstract fun initData()
    abstract fun observeData()
    protected open fun initExtraViewModels() {}

    @Suppress("MemberVisibilityCanBePrivate")
    protected var fragmentVisibilityChangedListener: ((Boolean) -> Unit)? = null
}