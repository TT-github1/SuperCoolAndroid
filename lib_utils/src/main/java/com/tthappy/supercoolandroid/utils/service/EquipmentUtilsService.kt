package com.tthappy.supercoolandroid.utils.service

import android.app.Application
import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.application.service.IEquipmentUtilsService
import com.tthappy.supercoolandroid.base.router.RouterService
import com.tthappy.supercoolandroid.utils.equipmemt.ScreenAdaptationUtils

@Route(path = RouterService.EQUIPMENTUTILS)
class EquipmentUtilsService: IEquipmentUtilsService {
    override fun init(context: Context?) {
        ScreenAdaptationUtils.initAppDensity(context as Application)
    }
}