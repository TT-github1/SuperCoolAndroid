package com.tthappy.supercoolandroid.plugin.gradleconfig

import org.gradle.api.DefaultTask

class ConfigTask extends DefaultTask{

    public def base = [
            minsdk : 23,
            tarsdk : 30,
            buildversion : "30.0.0",
            versioncode : 1,
            versionname : "1.0"
    ]

    //https://developer.android.com/jetpack/androidx/versions
    public def androidx = [
            appcompat       : "androidx.appcompat:appcompat:1.3.1",
            constraintlayout: "androidx.constraintlayout:constraintlayout:2.1.3",
            recyclerview    : "androidx.recyclerview:recyclerview:1.2.1",
    ]

    // todo 考虑用2
    private static final String AUTO_DISPOSE_VERSION = "1.4.0"
    public def autodispose = [
            lifecycle     : "com.uber.autodispose:autodispose-lifecycle:$AUTO_DISPOSE_VERSION",
    ]
    //https://github.com/uber/AutoDispose
    private static final String AUTO_DISPOSE2_VERSION = "2.0.0"
    public def autodispose2 = [
            runtime         : "com.uber.autodispose2:autodispose:$AUTO_DISPOSE2_VERSION",
            //LifecycleScopeProvider
            lifecycle       : "com.uber.autodispose2:autodispose-lifecycle:$AUTO_DISPOSE2_VERSION",
            //Android extensions
            android         : "com.uber.autodispose2:autodispose-android:$AUTO_DISPOSE2_VERSION",
            //Android Architecture Components extensions
            androidlifecycle: "com.uber.autodispose2:autodispose-androidx-lifecycle:$AUTO_DISPOSE2_VERSION"
    ]

    private static final String RETROFIT_VERSION = "2.9.0"
    public def retrofit = [
            runtime: "com.squareup.retrofit2:retrofit:$RETROFIT_VERSION",
            gson   : "com.squareup.retrofit2:converter-gson:$RETROFIT_VERSION",
            rxjava2: "com.squareup.retrofit2:adapter-rxjava2:$RETROFIT_VERSION"
    ]
    // https://github.com/square/retrofit
    // https://github.com/square/retrofit/blob/master/retrofit/src/main/resources/META-INF/proguard/retrofit2.pro

    private static final String OKHTTP_VERISON = "4.8.1"
    public def okhttp = [
            runtime           : "com.squareup.okhttp3:okhttp:$OKHTTP_VERISON",
            logginginterceptor: "com.squareup.okhttp3:logging-interceptor:$OKHTTP_VERISON"
    ]
    // https://github.com/square/okhttp


    private static final String RXJAVA2_VERSION = "2.2.0"
    private static final String RXANDROID2_VERSION = "2.1.1"
    private static final String RXJAVA3_VERSION = "3.0.0"
    public def rxjava2 = [
            runtime  : "io.reactivex.rxjava2:rxjava:$RXJAVA2_VERSION",
            rxandroid: "io.reactivex.rxjava2:rxandroid:$RXANDROID2_VERSION",
            kotlin   : "io.reactivex.rxjava2:rxkotlin:$RXJAVA2_VERSION"
    ]
    public def rxjava3 = [
            runtime  : "io.reactivex.rxjava3:rxjava:rxjava$RXJAVA3_VERSION",
            rxandroid: "io.reactivex.rxjava3:rxandroid:$RXJAVA3_VERSION",
            kotlin   : "io.reactivex.rxjava3:rxkotlin:$RXJAVA3_VERSION"
    ]
    // https://github.com/ReactiveX/RxJava
    // https://github.com/ReactiveX/RxAndroid
    // https://github.com/ReactiveX/RxKotlin

    public def arouter = [
            runtime : "com.alibaba:arouter-api:1.5.2",
            compiler: "com.alibaba:arouter-compiler:1.5.2"
    ]
    // https://github.com/alibaba/ARouter

    public String timber = 'com.jakewharton.timber:timber:4.7.1'

////////////////////////////////////////////////////////////////////////////////////////////////////

    // https://github.com/CymChad/BaseRecyclerViewAdapterHelper
    public def recycleradapter = 'com.github.CymChad:BaseRecyclerViewAdapterHelper:3.0.7'

    public String commontablayout = "com.flyco.tablayout:FlycoTabLayout_Lib:2.1.2"

    public def google = [
            flexboxlayout: "com.google.android.flexbox:flexbox:3.0.0"
    ]

    public String eventbus = "org.greenrobot:eventbus:3.1.1"
}