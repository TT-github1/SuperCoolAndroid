package com.tthappy.supercoolandroid.java.algorithm.sort;

import com.tthappy.supercoolandroid.java.datamock.HpDataLib;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/1 16:22
 * Update Date:
 * Modified By:
 * Description: 选择排序（记录最大值和它的index）
 */
public class SelectSort {
    public static void main(String[] args) {
        int[] arr = HpDataLib.getArrI(16);
        int[] arr2 = new int[16];
        System.arraycopy(arr, 0, arr2, 0, arr.length);
        System.out.println(Arrays.toString(arr));
        sort(arr);
        System.out.println(Arrays.toString(arr));

        System.out.println(Arrays.toString(arr2));
        sorter(arr2);
        System.out.println(Arrays.toString(arr2));
    }

    public static void sort(int[] arr) {
        // 外层循环控制选择次数
        for(int i = 0; i < arr.length - 1; i++) {
            // 内层循环控制一次选择的有序执行
            int j = 0;
            int index = 0;
            int max = arr[j];
            for(; j < arr.length - i - 1; j++) {
                if (arr[j+1] > max) {
                    max = arr[j+1];
                    index = j + 1;
                }
            }
            int tmp = arr[index];
            arr[index] = arr[arr.length-1-i];
            arr[arr.length-1-i] = tmp;
        }
    }

    private static void sorter(int[] arr) {
        if (arr == null || arr.length < 2) return;

        int end = arr.length - 1;

        while (end > 1) {
            int index = 0;
            for (int i = 1; i <= end; i++) {
                if (arr[i] > arr[index]) index = i;
            }
            if (index != end) {
                int temp = arr[end];
                arr[end] = arr[index];
                arr[index] = temp;
            }
            end--;
        }
    }
}
