package com.tthappy.supercoolandroid.uidemo.ui.game.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.view.View
//import com.tthappy.supercoolandroid.uidemo.ui.utilsf

class FangKuaiView(context: Context): View(context), IView {

    private val mBorderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = 2f
        color = Color.RED
        style = Paint.Style.STROKE
    }

    private val mFillPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.BLACK
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(60f.toInt(), 60f.toInt())
    }

    override fun onDraw(canvas: Canvas) {
        // 画边框
//        canvas.drawLine(1f, 1f, 59f, 1f, mBorderPaint)
//        canvas.drawLine(59f, 1f, 59f, 59f, mBorderPaint)
//        canvas.drawLine(59f, 59f, 1f, 59f, mBorderPaint)
//        canvas.drawLine(1f, 59f, 1f, 1f, mBorderPaint)

        // 划线是从中间向两边扩展，比如在（0， 0）点向（0， 10）点画一条2px的线，那么就是一个（-1， -1， 11， 11）的矩形
        canvas.drawRect(1f, 1f, 59f, 59f, mBorderPaint)

        canvas.drawLine(30f, 1f, 30f, 59f, mBorderPaint)
        canvas.drawLine(1f, 30f, 59f, 30f, mBorderPaint)

        // 画里面小方格
        canvas.drawRect(2f, 2f, 29f, 29f, mFillPaint)
        canvas.drawRect(31f, 2f, 58f, 29f, mFillPaint)
        canvas.drawRect(2f, 31f, 29f, 58f, mFillPaint)
        canvas.drawRect(31f, 31f, 58f, 58f, mFillPaint)
    }
}