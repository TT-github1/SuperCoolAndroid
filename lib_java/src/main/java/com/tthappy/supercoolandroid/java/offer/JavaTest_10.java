package com.tthappy.supercoolandroid.java.offer;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/28 9:28
 * Update Date:
 * Modified By:
 * Description: 输入数字 n，按顺序打印出从 1 到最大的 n 位十进制数。比如输入 3，则打印出 1、2、3 一直到最大的 3 位数 999。
 */
public class JavaTest_10 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(getArray(3)));
    }

    private static int[] getArray(int n) {
        if (n <= 0) return new int[0];
        int target = (int) Math.pow(10, n) - 1;
        int[] res = new int[target];
        for (int i = 0; i < target; i++) {
            res[i] = i + 1;
        }
        return res;
    }
}
