package com.tthappy.supercoolandroid.baseui.title

import android.view.View
import androidx.viewbinding.ViewBinding
import com.tthappy.supercoolandroid.baseui.databinding.IncludeToolbarBinding
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/10 17:26
 * Update Date:
 * Modified By:
 * Description:
 */
abstract class BaseTitleActivity<VB: ViewBinding, VM: BaseViewModel>: BaseActivity<VB, VM>() {

    private var titleHelper: TitleBarHelper? = null

    override fun setContentView(view: View?) {
        super.setContentView(view)
        setTitle(providerTitle())
    }

    fun getTitleHelper(): TitleBarHelper {
        if (titleHelper == null) {
            titleHelper = TitleBarHelper(getViewByVB(IncludeToolbarBinding::class.java).toolbar)
        }
        return titleHelper!!
    }

    private fun <T> getViewByVB(clazz: Class<T>): T {
        val fields = viewBinding.javaClass.fields
        var tValue: T? = null
        fields.forEach {
            if (clazz == it.type) {
                tValue = it.get(viewBinding) as T
                return@forEach
            }
        }
        return tValue ?: throw IllegalArgumentException("布局中不存在想要获取的View{${clazz.simpleName}}的类型或者想要获取的View没有指定id的值，请检查布局。。。")
    }

    open fun setTitle(title: String?) {
        if (!title.isNullOrEmpty()) {
            getTitleHelper().enableBack(this)
            getTitleHelper().setTitle(title)
        }
    }

    abstract fun providerTitle(): String?
}