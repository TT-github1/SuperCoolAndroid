package com.tthappy.supercoolandroid.uidemo.ui.customcontrol.myviews.insttrumentview;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;

/**
 * Author:      tfhe
 * Create Date: Created in 2020/10/12 15:07
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.CC_PANEL)
public class InsttrumentPanelViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insttrument_panel_demo);
    }
}
