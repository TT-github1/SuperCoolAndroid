package com.tthappy.supercoolandroid.uihome.main.category.mine;

import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.tthappy.supercoolandroid.mvvm.view.BaseFragment;
import com.tthappy.supercoolandroid.uihome.databinding.FragmentMineBinding;
import com.tthappy.supercoolandroid.uihome.main.MainViewModel;
import com.tthappy.supercoolandroid.utils.equipmemt.StatusBarUtils;

import java.util.Objects;

/**
 * Author:      tfhe
 * Create Date: Created in 2020/10/10 18:21
 * Update Date:
 * Modified By:
 * Description:
 */
public class MineFragment extends BaseFragment<FragmentMineBinding, MainViewModel> {

    FragmentMineBinding viewbinding;
    private MineAdapter mAdapter;

    public static MineFragment getInstance() {
        return new MineFragment();
    }

    @Override
    public void initView() {
        viewbinding = Objects.requireNonNull(getViewBinding());
        StatusBarUtils.underStatusBar(getContext(), viewbinding.viewHeader, 30);

        initRecyclerView();
    }

    private void initRecyclerView() {
        mAdapter = new MineAdapter();
        viewbinding.rvContainer.setAdapter(mAdapter);
        viewbinding.rvContainer.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void initData() {
        mAdapter.setNewInstance(viewModel.getMineFunctionsList());
    }

    @Override
    public void observeData() { }
}
