package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/30 11:28
 * Update Date:
 * Modified By:
 * Description: 给定一个升序数组，求数组不重复元素的个数，且要将这些元素升序排列在数组最前面
 *
 *              解： 采用双指针
 */
public class JavaTest_21 {
    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 2, 3, 3, 4, 5, 6, 7, 7};
        System.out.println(delete(arr));
    }

    private static int delete(int[] arr) {
        int n = arr.length;
        if (n == 0) return 0;

        int fast = 1;
        int slow = 1;
        while (fast < n) {
            if (arr[fast] != arr[fast - 1]) {
                arr[slow] = arr[fast];
                slow++;
            }
            fast++;
        }
        return slow;
    }
}
