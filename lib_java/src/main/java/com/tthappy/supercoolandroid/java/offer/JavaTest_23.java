package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/30 15:34
 * Update Date:
 * Modified By:
 * Description: 给一个字符串，返回另一个字符串在这个字符串中第一次出现的位置
 *
 *              解： 暴力搜索和KMP
 */
public class JavaTest_23 {
    public static void main(String[] args) {
        String msg = "aaaabaaaabaaaaf";
        String target = "aaaabaaaaf";
        System.out.println(find(msg, target));
        System.out.println(forceFind(msg, target));
        System.out.println(strStr(msg, target));

//        System.out.println(Arrays.toString(getNext("aaaabaaaaf")));
//        System.out.println(Arrays.toString(getNext("abaabaaaaf")));
    }

    // 暴力破解
    private static int find(String msg, String target) {
        int ms = msg.length();
        int tar = target.length();

        for (int i = 0; i + tar <= ms; i++) {
            boolean flag = true;
            for(int j = 0; j < tar; j++) {
                if (msg.charAt(i + j) != target.charAt(j)) {
                    flag = false;
                    break;
                }
            }
            if (flag) return i;
        }

        return -1;
    }

    private static int forceFind(String msg, String target) {
        int m = msg.length();
        int n = target.length();

        for (int i = 0; i + n <= m; i++) {
            // 换下一个字符开始，重置标签
            boolean flag = true;
            for (int j = 0; j < n; j++) {
                if (msg.charAt(i + j) != target.charAt(j)) {
                    flag = false;
                    break;
                }
            }
            // 如果走完一轮，标签还没被改过，说明匹配成功
            if (flag) return i;
        }
        return -1;
    }


    /**
     *  KMP算法
     */

    private static int[] getNext(String needle) {
        int[] next = new int[needle.length()];
        char[] ndl = needle.toCharArray();
        next[0] = 0;
        int j = 0;

        for (int i = 1; i < ndl.length; i++) { // "aaaabaaaaf"
            while (j > 0 && ndl[i] != ndl[j]) { // 注意这里是while循环
                j = next[j - 1]; // 找前一位的对应的回退位置 这里最难理解，到底怎么理解
//                j = 0; // 要我我就这样写了
            }
            if (ndl[i] == ndl[j]) {
                j++;
            }
            next[i] = j;
        }

        return next;
    }

    private static int strStr(String heyStack, String needle) {
        if (needle.length() == 0) return 0;
        int[] next = getNext(needle);

        int j = 0;
        for (int i = 0; i < heyStack.length(); i++) {
            while (j > 0 && heyStack.charAt(i) != needle.charAt(j)) {
                j = next[j - 1];
            }
            if (heyStack.charAt(i) == needle.charAt(j)) {
                j++;
            }
            if (j == needle.length()) {
                return i - needle.length() + 1;
            }
        }

        return -1;
    }
}
