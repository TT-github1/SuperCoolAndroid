package com.tthappy.supercoolandroid.java.offer;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/30 14:59
 * Update Date:
 * Modified By:
 * Description: 给定一个数组，要求删去数组中所有的某个相同的元素，返回剩余元素的长度，且剩余元素都在数组最前面。（其实就是上一题的变种）
 *
 *              解： 双指针优化，当遍历到目标元素时，将数组最后一个元素移过来，这样就避免后续元素挨个往前移动。
 */
public class JavaTest_22 {
    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 2, 3, 3, 4, 5, 6, 7, 7};
        System.out.println(delete(arr, 3));
    }

    // 双指针优化
    private static int delete(int[] arr, int target) {
        int n = arr.length;
        if (n == 0) return 0;

        int left = 0;
        int right = arr.length;
        while (left < right) {
            if (arr[left] == target) {
                arr[left] = arr[right - 1];
                right--;
            }else left++;
        }

        System.out.println(Arrays.toString(arr));

        return left;
    }
}
