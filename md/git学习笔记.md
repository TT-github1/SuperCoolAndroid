## git学习笔记



#### 创建git仓库

- 创建git仓库

  git init

- 删除git仓库

  仍然git init，然后删除.git文件夹



#### 查看git信息

- 查看当前git仓库状态

  git status

- 查看git提交记录

  git log

  - 将所有的commit记录罗列
  - 其中包括commit id（很明显是个hash值）、作者（犯罪嫌疑人）、日期（犯罪日期）
  - 当记录太多，一次显示不下时，会进入一个浏览的模式，可以像操作linux浏览vim一样，使用j和k进行下上移动，以及使用q退出浏览模式

- 查看git操作记录

  git reflog



#### git操作

- 将文件的修改（或者添加、删除等）从工作区添加到暂存区

  git add <file>

- 将暂存区中的所有修改提交到本地仓库

  git commit -m "xxx"

  - -m 参数代表message，指此次提交的说明

- 回溯到某次commit

  git reset --hard <commit id>

  - 参数 --hard 将工作区和暂存区和仓库都恢复到对应的commit id的状态，如果不加，工作区会保持原样

- 丢弃工作区中的修改

  git restore <file>

  - 只会丢弃工作区中的修改，即还没有进行git add操作的修改，暂存区中的修改不会受这条指令影响（及时暂存区中有对应的文件，因为git的提交是针对修改的）

- 删除文件

  git rm <file>

  - 需要先在文件夹中使用常规删除文件的方法将文件删除，再调用此命令进行删除，最后commit才能完成从库中删除这个文件
  - 当仅在文件夹中使用常规方法删除而没有执行这条命令时，删除文件也是修改操作，可以使用之前的git restore <file> 命令撤销这个删除的修改
  
- 丢弃工作区的修改

  git checkout -- <file>

  - 只会丢弃工作区中的修改，对已经添加到暂存区的修改不产生作用

- 撤销暂存区中的修改

  git reset HEAD <file>

  - 其实只是将暂存区中的内容清除掉，但是不会删除真实的内容，就好比是你之前没有添加过到暂存区一样
  - 所以工作区中的修改该是什么样还是什么样，如果想彻底丢弃修改，还需要使用上面的丢弃工作区的修改命令



#### 建立远程仓库

- 关联远程仓库

  git remote add origin git@github.com:<github账号名>/<远程仓库名>.git

  - 注意将上面两个尖括号整个替换为对应内容
  
  - 上面origin后面跟的是SSH链接，表示使用SSH的方式进行通信（还可以使用Https的方式，换成https的链接即可）
  
  - ##### 注意：
  
    - ##### 如果你本地的电脑之前没有给你的目标远程库推送过git内容，你首次推送时，需要给你的git托管平台（比如github、gitlab等）配置SSH公钥。
  
    - ##### 首先查看你的C:\Users\ad目录下有没有.ssh文件夹，如果有，且里面有某个.pub结尾的文件，代表之前你的本地电脑生成过SSH公钥和密钥，此时拷贝.pub文件中的内容，粘贴到git托管平台的ssh配置中，就完成ssh的配置
  
    - ##### 如果没有，则需要自己生成，看下面例子
  
    - ##### 错误案例：发现本地已经存在.ssh文件夹，结果还是想自己生成ssh的公私钥，生成后把公私钥拷贝进.ssh文件夹，并配置git托管平台的ssh。结果发现git没办法push到远程库。最后删除.ssh文件，在C:\Users\ad目录下，打开cmd，输入 ssh-keygen -t rsa -C "test"，重新生成了ssh的公私钥（调用这条命令会在当前目录自动生成.ssh目录），然后把新生成的公钥配置到git托管平台，终于可以正常推送了



#### 分支管理



- ##### 创建分支

  - ###### 创建分支

    git branch branchname

  - ###### 创建分支并切换到该分支

    - git checkout -b branchname
    - git switch -c branchname（新版本）

- 查看分支

  git branch

- 切换分支

  git checkout branchname

  git switch branchname（新版本）

  - 分支切换时最好保证你的工作区和暂存区是干净的

- 合并分支

  git merge branchname

  - 合并操作指将merge后面跟的分支合并到当前分支
  - 所以在执行合并操作时，确保先切换到正确的分支（较老的），然后执行git merge branchname（较新的）

- 只合并某个分支的某次提交（例如某人在某个分支上修复了一个你当前分支也存在的bug，那么你就可以直接将他那次修改合并过来，但是他的其他提交不合并过来）

  git cherry-pick <commit id>

- 将修改内容暂时隐藏保存

  - 快速贮藏

    git stash

    - 保存当前工作区和暂存区的内容，使得工作区和暂存区变干净
    - 可以多次使用该命令，保存的内容以栈形式存放
    
  - 普通贮藏（可添加注释）
  
    git stash save "注释"
  
  - 查看已有贮藏的列表
  
    git stash list
  
  - 快速恢复贮藏
  
    git stash pop
  
    - 将最近一次贮藏恢复
    - 并删除该贮藏
  
  - 恢复对应的贮藏
  
    git stash apply <贮藏名>
  
    - 如果不加贮藏名，则恢复最近的一次贮藏
    - 不会删除该贮藏，所以可以适用于要在多个分支恢复贮藏的情况
  
  - 删除贮藏
  
    git stash drop <贮藏名>
  
  - 清空贮藏
  
    git stash clear
