package com.tthappy.supercoolandroid.java.demo;

class MovingAverage {

    private final int size;
    private int currentSize;
    private Node header;
    private Node tail;
    private double count;

    public MovingAverage(int size) {
        this.size = size;
    }

    public double next(int val) {
        Node node = new Node();
        node.value = val;

        if (currentSize < size) {
            if (header == null) header = node;
            else {
                Node temp = header;
                while (temp.next != null) temp = temp.next;
                temp.next = node;
            }
            currentSize++;
            if (currentSize == size) tail = node;
            count += val;
        } else {
            count = count - header.value + val;
            header = header.next;
            tail.next = node;
            tail = tail.next;
        }
        return count / currentSize;
    }

    public static class Node {
        int value;
        Node next;
    }
}