package com.tthappy.supercoolandroid.java.datamock;

import java.util.Random;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/23 15:48
 * Update Date:
 * Modified By:
 * Description:
 */
public class HpDataLib {

    static Random random = new Random(233);

    public static int[] getArrI(int length) {
        return getArrI(length, length * 10);
    }

    public static int[] getArrI(int length, int top) {
        return getArrI(length, 0, top);
    }

    public static int[] getArrI(int length, int bottom, int top) {
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = (int) (bottom + random.nextDouble() * (top - bottom));
        }
        return arr;
    }
}
