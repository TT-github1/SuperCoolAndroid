package com.tthappy.supercoolandroid.log

import com.tthappy.supercoolandroid.log.timber.Timber


/**
 * Author:      tfhe
 * Create Date: Created in 2021/8/4 17:25
 * Update Date:
 * Modified By:
 * Description:
 */
class HpDebugTree: Timber.DebugTree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        message.split(LINE_SEP!!.toRegex()).forEach{
            when (it) {
                TOP_BORDER, BOTTOM_BORDER, MIDDLE_BORDER -> super.log(priority, tag, it, t)
                "" -> {}
                else -> super.log(priority, tag, LEFT_BORDER + it, t)
            }
        }
    }
}