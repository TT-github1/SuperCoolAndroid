package com.tthappy.supercoolandroid.uidemo.ui.test.flexboxlayout

import android.widget.TextView
import com.alibaba.android.arouter.facade.annotation.Route
import com.google.android.flexbox.FlexboxLayout
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.uidemo.R
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityFlexboxLayoutBinding
import kotlin.random.Random

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/21 14:53
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.FLEXBOXLAYOUT)
class FlexboxLayoutActivity: BaseTitleActivity<ActivityFlexboxLayoutBinding, FlexboxLayoutViewModel>() {
    override fun providerTitle() = "FlexboxLayoutActivity"

    override fun initView() {
        viewBinding.flContainer.removeAllViews()
        for(index in 0..20) {
            val textView = TextView(this)
            textView.text = getString(R.string.test_text)
            textView.setBackgroundResource(R.color.test_blue)
//            textView.layoutParams = FlexboxLayoutManager(this, FlexDirection.COLUMN, FlexWrap.WRAP)
            viewBinding.flContainer.addView(textView)
            val layoutParams = textView.layoutParams as FlexboxLayout.LayoutParams
            layoutParams.width = Random(10).nextInt()
            layoutParams.height = 50
            layoutParams.marginStart = 10
            layoutParams.marginEnd = 10
        }

    }

    override fun initData() {

    }

    override fun observeData() {

    }
}