package com.tthappy.supercoolandroid.java.offer;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/23 15:11
 * Update Date:
 * Modified By:
 * Description: 将任意一个数组重排序，使得奇数全部在前半部分，偶数全部在后半部分，且他们的相对位置不改变
 *              例如： {3，2，1，4} => {3，1，2，4}
 *
 *              解： 双指针，奇指针一直往后遍历寻找奇数，偶指针一直记录第一个偶数的位置（也会变），奇指针找到
 *                  奇数后，将偶指针（包括）到奇指针（不包括）位置上的偶数往后移动一位，把找到的奇数插入到偶指针位置，
 *                  偶指针+1。然后重复
 *                  其实整个过程就是一直找奇数然后往前移动，直到所以奇数都找到且移动到数组前面。
 */
public class JavaTest_3 {
    public static void main(String[] args) {
        int[] arr = {5, 7, 2, 4, 3, 1, 6, 8, 9, 0};
        reOrder(arr);
        System.out.println(Arrays.toString(arr));
    }

    private static void reOrder(int[] arr) {
        if (arr == null || arr.length < 2) return;

        int ou = 0;
        while (arr[ou] % 2 == 1) {
            ou++;
        }

        int ji = ou;
        while (ji < arr.length - 1 ) {
            ji++;
            if (arr[ji] % 2 == 1) {
                int temp = arr[ji];
                // 此for循环可以数组拷贝代替 if (ji - ou >= 0) System.arraycopy(arr, ou, arr, ou + 1, ji - ou);
                for(int i = ji; i > ou; i--) {
                    arr[i] = arr[i - 1];
                }
                arr[ou] = temp;
                ou++;
            }
        }
    }
}
