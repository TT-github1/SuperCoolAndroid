package com.tthappy.supercoolandroid.java.thread;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/1 14:07
 * Update Date:
 * Modified By:
 * Description:
 */
public class AAA {
    static {
        System.out.println("AAA static first");
    }

    {
        System.out.println("AAA normal first");
    }

    public AAA() {
        System.out.println("AAA constuctor first");
    }

    static {
        System.out.println("AAA static second");
    }

    {
        System.out.println("AAA normal second");
    }
}
