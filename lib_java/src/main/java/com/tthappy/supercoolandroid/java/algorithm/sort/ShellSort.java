package com.tthappy.supercoolandroid.java.algorithm.sort;

import com.tthappy.supercoolandroid.java.datamock.HpDataLib;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/2 9:52
 * Update Date:
 * Modified By:
 * Description: 希尔排序
 *
 *              将数组均分为若干组，这个若干的取值会影响希尔排序的效率，并且有多种设计方法，
 *              但本例仅讨论若干取数组长度的一半，并且将其称为增量，分组的形式为间隔分组，
 *              例如： ABCABCABC...ABCABC
 *              然后对这些分组进行直接插入排序，完成这些操作之后，将增量对半取，然后将数组
 *              继续分为增量数组那么多组，继续分组进行直接插入排序。直到最后增量为1，即数组
 *              不可分，执行完直接插入排序，就完成了希尔排序
 */

public class ShellSort {

    public static void main(String[] args) {
        int[] arr = HpDataLib.getArrI(15);
        System.out.println(Arrays.toString(arr));
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void sort(int []arr){
        //增量gap，并逐步缩小增量
        for (int gap = arr.length / 2; gap > 0; gap /= 2) {
            //从第gap个元素，逐个对其所在组进行直接插入排序操作
            for (int i = gap; i < arr.length; i++) {
                int j = i;
                while (j - gap >= 0 && arr[j] < arr[j - gap]) {
                    //插入排序采用交换法
                    swap(arr, j, j - gap);
                    j -= gap;
                }
            }
        }
    }

    public static void swap(int[] arr, int a, int b) {
        arr[a] = arr[a] + arr[b];
        arr[b] = arr[a] - arr[b];
        arr[a] = arr[a] - arr[b];
    }
}
