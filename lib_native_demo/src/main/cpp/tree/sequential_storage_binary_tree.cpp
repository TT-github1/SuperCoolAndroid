//
// Created by ad on 2022/1/26.
//

#include "sequential_storage_binary_tree.h"

SSBTree ssbTree;

extern "C" JNIEXPORT
void JNICALL Java_com_tthappy_nativedemo_tree_SequentialStorageBinaryTree_createTree(
    JNIEnv * env,
    jobject thiz
) {
    ssbTree.size = 0;
    ssbTree.nodes = malloc(100 * sizeof(void *));
    LOG("%s\n", "**  Create SequentialStorageBinaryTree Success");
}

extern "C" JNIEXPORT
void JNICALL Java_com_tthappy_nativedemo_tree_SequentialStorageBinaryTree_add(
    JNIEnv * env,
    jobject thiz,
    jint data
){
    int * cursor = (int *)ssbTree.nodes;
    *(cursor + ssbTree.size++) = data;
    LOG("**  Add data success, data is %d\n", data);
}

extern "C" JNIEXPORT
void JNICALL Java_com_tthappy_nativedemo_tree_SequentialStorageBinaryTree_showTree(
    JNIEnv * env,
    jobject thiz
){
    int * cursor = (int *) ssbTree.nodes;
    int i = 0;
    for(; i < ssbTree.size; i++) {
        LOG("**  The %d node is %d\n", i, *cursor);
        cursor++;
    }
}
