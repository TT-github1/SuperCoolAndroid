package com.tthappy.supercoolandroid.uihome.debug.plugins

import android.content.Intent
import android.net.Uri
import com.tthappy.supercoolandroid.uihome.debug.DebugPlugin
import com.tthappy.supercoolandroid.uihome.debug.activity.DebugActivity

class DoSomeThingPlugin(mActivity: DebugActivity): DebugPlugin(mActivity) {
    override fun icon(): Nothing? = null
    override fun name() = "随便测试点什么"
    override fun value() = ""

    override fun doWhat() {
        val intent = Intent("android.intent.action.VIEW", Uri.parse("tthappy://app.supercool.com"))
        mActivity.startActivity(intent)
    }
}