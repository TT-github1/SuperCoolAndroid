package com.tthappy.supercoolandroid.java.designpattern.singleton;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/4 8:56
 * Update Date:
 * Modified By:
 * Description: 静态内部类
 */
public class ForthStaticClassSingleton {

    private ForthStaticClassSingleton() {}

    /**
     *  静态内部类的特点：
     *  外部类加载时不需要加载静态内部类，
     *  不被加载则不占用内存，（延迟加载）当外部类调用getInstance方法时，
     *  才加载静态内部类，静态属性保证了全局唯一，
     *  静态变量初始化保证了线程安全，
     *  所以这里的方法没有加synchronized关键字
     *  （JVM保证了一个类的 初始化在多线程下被同步加锁）
     */
    private static class SingletonInstance {
        private static final ForthStaticClassSingleton INSTANCE = new ForthStaticClassSingleton();
    }

    public static ForthStaticClassSingleton getInstance() {
        return SingletonInstance.INSTANCE;
    }
}
