package com.tthappy.supercoolandroid.uidemo.ui.question.nestedscrollandfling

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.ViewConfiguration
import android.widget.LinearLayout
import android.widget.Scroller

class HpScrollAndFlingView(context: Context, attrs: AttributeSet): LinearLayout(context, attrs) {

    // 上一次触摸事件的y坐标
    private var lastY = 0f
    // 用来算速度的工具（通过在每次的触摸事件中打点）
    private lateinit var mVelocityTracker: VelocityTracker
    // 用来根据传入的速度算当前应该滚动到的位置的工具
    private val scroller by lazy { Scroller(context) }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!super.onTouchEvent(event)) {
            if (!this::mVelocityTracker.isInitialized) mVelocityTracker = VelocityTracker.obtain()
            var isActionUp = false
            // 拷贝一份触摸事件，我猜是为了防止污染原事件
            val eventCopy = MotionEvent.obtain(event)

            val eventY = event.y

            when(event.actionMasked) {
                MotionEvent.ACTION_DOWN -> lastY = eventY
                MotionEvent.ACTION_MOVE -> {
                    val dy = lastY - eventY
                    scrollTo(0, (scrollY + dy).toInt())
                    lastY = eventY
                }
                MotionEvent.ACTION_UP -> {
                    // 最后一次打点
                    mVelocityTracker.addMovement(eventCopy)
                    isActionUp = true
                    // 设定一个最大速度，速度太快体验也不好
                    val maxV = ViewConfiguration.get(context).scaledMaximumFlingVelocity.toFloat()
                    // 这里的 1000 是你想要的速度单位。值1提供像素/毫秒，1000提供像素/秒
                    mVelocityTracker.computeCurrentVelocity(1000, maxV)
                    val yVelocity = -mVelocityTracker.getYVelocity(event.getPointerId(0))

                    startFling(yVelocity.toInt())

                    mVelocityTracker.clear()
                }
                else -> {}
            }

            if (!isActionUp) {
                // 每次触摸事件打点
                mVelocityTracker.addMovement(eventCopy)
            }
            eventCopy.recycle()

            return true
        }

        return false
    }

    private val refreshRunnable = Runnable {
        if (scroller.computeScrollOffset()) {
            scrollTo(0, scroller.currY)
            postOnAnimationFun()
        }
    }

    private fun postOnAnimationFun() {
        // 使Runnable在下一个动画时间步长上执行
        postOnAnimation (refreshRunnable)
    }

    private fun startFling(velocity: Int) {
        // 通知scroller开始计算应该活动到的位置
        scroller.fling(0, scrollY, 0, velocity, Int.MIN_VALUE, Int.MAX_VALUE, Int.MIN_VALUE, Int.MAX_VALUE)

        postOnAnimationFun()
    }

    override fun scrollTo(x: Int, y: Int) {
        val realHeight = getRealHeight()
        val newY = if (y < 0) 0
        else if (y > realHeight) realHeight
        else y
        super.scrollTo(x, newY)
    }

    private fun getRealHeight(): Int {
        var height = 0
        for(index in 0 until childCount) {
            height += getChildAt(index).height
        }
        return height - this.height
    }

}