package com.tthappy.supercoolandroid.uidemo.ui.customcontrol.myviews.doubledragbar;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.Keep;
import androidx.annotation.Nullable;

import com.tthappy.supercoolandroid.uidemo.R;
import com.tthappy.supercoolandroid.utils.measure.DimenUtils;

import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/3/24 19:07
 * Update Date:
 * Modified By:
 * Description: 自定义的双向滑动的滚动条，使用前需要手动setData传数据进去，
 * 数据需要使用本类的静态内部类PointData的List形式传入，
 * 具体示例可参考发现好企业筛选条件页面的成立年限筛选滚动条
 */
@SuppressWarnings("unused, ClickableViewAccessibility")
public class DoubleDragBar extends View {

    private Context mContext;

    private float mCurrentLeftX = 0;
    private float mCurrentRightX = 0;

    private float mLastActionDownX = 0;
    private float mLastActionDownY = 0;

    private int mCurrentLeftArrayIndex = 0;
    private int mCurrentRightArrayIndex = 0;

    private Paint mainPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    //本控件宽高
    private float mMeasureWidth = 0;

    //是否让控件颜色渐变
    private boolean enableGradient = false;
    //是否开启指标归位的动画
    private boolean enableAnimation = true;
    //左右指标动画
    private ObjectAnimator leftCursorAnimator;
    private ObjectAnimator rightCursorAnimator;
    //动画时长
    private int animationTime = 200;

    //五边形控件宽高(按正方形算)
    private float cursorWidth;
    private float cursorHeight;
    //五边形文字的颜色
    @ColorInt
    private int cursorTextColor;
    //五边形文字的字号
    private float cursorTextSize = 0;
    //五边形文字的基线距离五边形顶部的高度
    private float cursorTextBaseLineHeight;
    //五边形的Bitmap
    private Bitmap cursorBitmap;
    //展示五边形图片的区域
    private Rect cursorBitmapRect;
    //五边形图片在屏幕上展示的位置
    private Rect cursorShowRect;
    //五边形与园的间距
    private float marginBetweencursorCircle;
    //小圆个数
    private int littleCircleNumber = 6;
    //小圆半径
    private float rLittleCircle;
    //小圆颜色
    @ColorInt
    private int littleCircleColor = 0;
    //相邻小圆的圆心距离
    private float lengthCircleToCircle = 0;
    //定义存储各个小圆圆心横坐标的数组
    private float[] arrayCircleCenter;
    //大圆半径
    private float rHugeCircle;
    //大圆阴影半径
    private float rHugeCircleShadow;
    //大圆阴影颜色
    @ColorInt
    private int hugeCircleShadowColor = 0;
    //画渐变的笔
    private Paint gradientPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    //细长条高度
    private float barLineHeight;
    //小圆与文字的间距
    private float marginBetweenCircleWorld;
    //文字的字号
    private float barLineTextSize = 0;
    //画文字的笔
    private Paint worldPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    //左指标上五边形最后一次所记录的值
    private String mLeftLastMask;
    //右指标上五边形最后一次所记录的值
    private String mRightLastMask;

    //允许向右滑动
    private boolean enableSlideToRight = false;
    //允许向左滑动
    private boolean enableSlideToLeft = false;
    //是否正在滑动
    private boolean isMoving = false;

    //各个小圆点上对应的数据
    private List<PointData> mData;

    //是否初始化过参数
    private boolean hasInitProperty = false;

    private OnLOrRMaskChangeListener mListener;

    public DoubleDragBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.DoubleDragBar);
        barLineHeight = ta.getDimension(R.styleable.DoubleDragBar_barLineHeight, DimenUtils.dp2px(4));
        cursorTextColor = ta.getColor(R.styleable.DoubleDragBar_cursorTextColor, Color.WHITE);
        littleCircleColor = ta.getColor(R.styleable.DoubleDragBar_littleCircleColor, Color.parseColor("#506FFA"));
        hugeCircleShadowColor = ta.getColor(R.styleable.DoubleDragBar_shadowCircleColor, Color.parseColor("#2601A5FD"));
        rLittleCircle = ta.getDimension(R.styleable.DoubleDragBar_rLittleCircle, DimenUtils.dp2px(6));
        rHugeCircle = ta.getDimension(R.styleable.DoubleDragBar_rHugeCircle, DimenUtils.dp2px(8));
        rHugeCircleShadow = ta.getDimension(R.styleable.DoubleDragBar_rHugeCircleShadow, DimenUtils.dp2px(10));
        cursorTextSize = ta.getDimension(R.styleable.DoubleDragBar_cursorTextSize, DimenUtils.dp2px(8));
        barLineTextSize = ta.getDimension(R.styleable.DoubleDragBar_barLineTextSize, DimenUtils.dp2px(10));
        cursorWidth = ta.getDimension(R.styleable.DoubleDragBar_cursorWidth, DimenUtils.dp2px(24));
        cursorHeight = ta.getDimension(R.styleable.DoubleDragBar_cursorHeight, DimenUtils.dp2px(24));
        cursorTextBaseLineHeight = ta.getDimension(R.styleable.DoubleDragBar_cursorTextBaseLineHeight, DimenUtils.dp2px(12));
        marginBetweencursorCircle = ta.getDimension(R.styleable.DoubleDragBar_cursorTextBaseLineHeight, DimenUtils.dp2px(8));
        marginBetweenCircleWorld = ta.getDimension(R.styleable.DoubleDragBar_cursorTextBaseLineHeight, DimenUtils.dp2px(6));
        enableGradient = ta.getBoolean(R.styleable.DoubleDragBar_enableGradient, false);
        enableAnimation = ta.getBoolean(R.styleable.DoubleDragBar_enableAnimation, true);
        animationTime = ta.getInteger(R.styleable.DoubleDragBar_animationTime, 500);
        int cursorIconId = ta.getResourceId(R.styleable.DoubleDragBar_cursorIconId, R.drawable.icon_double_drag_bar_pentagon);
        ta.recycle();

        cursorBitmap = BitmapFactory.decodeResource(mContext.getResources(), cursorIconId);
        cursorBitmapRect = new Rect(0, 0, cursorBitmap.getWidth(), cursorBitmap.getHeight());
        cursorShowRect = new Rect(0, 0, (int) cursorWidth, (int) cursorHeight);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mMeasureWidth = getMeasuredWidth();

        if (!hasInitProperty) {
            //初始化两个指标的横坐标
            mCurrentLeftX = cursorWidth / 2;
            mCurrentRightX = mMeasureWidth - cursorWidth / 2;
            mCurrentLeftArrayIndex = 0;
            mCurrentRightArrayIndex = littleCircleNumber - 1;
            if (null != mData && !mData.isEmpty()) {
                mLeftLastMask = mData.get(0).getDescribe();
                mRightLastMask = mData.get(mData.size() - 1).getDescribe();
            }

            //计算圆心距离
            lengthCircleToCircle = (mMeasureWidth - cursorWidth) / (littleCircleNumber - 1);

            //定义圆心数组
            if (null == arrayCircleCenter) {
                arrayCircleCenter = new float[littleCircleNumber];
                for (int i = 0; i < arrayCircleCenter.length; i++) {
                    arrayCircleCenter[i] = cursorWidth / 2 + lengthCircleToCircle * i;
                }
            }

            hasInitProperty = true;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //画底部灰色背景
        drawGrayBackground(canvas);
        //绘制可拖动的控件（蓝色长条和多个蓝色小圆）
        drawDragBlueBackground(canvas);
        //绘制左指标
        drawCursor(canvas, mCurrentLeftX, mLeftLastMask);
        //绘制右指标
        drawCursor(canvas, mCurrentRightX, mRightLastMask);
    }

    private void drawGrayBackground(Canvas canvas) {
        mainPaint.setColor(Color.parseColor("#F4F4F4"));
        canvas.drawRect(
                cursorWidth / 2,
                cursorHeight + marginBetweencursorCircle + rLittleCircle - barLineHeight / 2,
                mMeasureWidth - cursorWidth / 2,
                cursorHeight + marginBetweencursorCircle + rLittleCircle + barLineHeight / 2,
                mainPaint
        );
        mainPaint.setColor(Color.parseColor("#EBEBEB"));
        for (int i = 0; i < littleCircleNumber; i++) {
            canvas.drawOval(
                    arrayCircleCenter[i] - rLittleCircle,
                    cursorHeight + marginBetweencursorCircle,
                    arrayCircleCenter[i] + rLittleCircle,
                    cursorHeight + marginBetweencursorCircle + rLittleCircle * 2,
                    mainPaint
            );

            if (null != mData) {
                worldPaint.setColor(Color.GRAY);
                worldPaint.setTextSize(barLineTextSize);
                canvas.drawText(mData.get(i).getDescribe(), arrayCircleCenter[i] - worldPaint.measureText(mData.get(i).getDescribe()) / 2, cursorHeight + marginBetweencursorCircle + rLittleCircle * 2 + marginBetweenCircleWorld + barLineTextSize, worldPaint);
            }

        }
    }

    private void drawDragBlueBackground(Canvas canvas) {
        //蓝色长条
        mainPaint.setColor(littleCircleColor);
        setBarLineGradient();
        canvas.drawRect(
                Math.max(mCurrentLeftX, cursorWidth / 2),
                cursorHeight + marginBetweencursorCircle + rLittleCircle - barLineHeight / 2,
                Math.min(mCurrentRightX, mMeasureWidth - cursorWidth / 2),
                cursorHeight + marginBetweencursorCircle + rLittleCircle + barLineHeight / 2,
                enableGradient ? gradientPaint : mainPaint
        );

        //多个小圆
        for (int i = mCurrentLeftArrayIndex; i <= mCurrentRightArrayIndex; i++) {
            setLittleCircleGradient(i);
            canvas.drawOval(
                    arrayCircleCenter[i] - rLittleCircle,
                    cursorHeight + marginBetweencursorCircle,
                    arrayCircleCenter[i] + rLittleCircle,
                    cursorHeight + marginBetweencursorCircle + rLittleCircle * 2,
                    enableGradient ? gradientPaint : mainPaint
            );
        }
    }

    //绘制指标
    private void drawCursor(Canvas canvas, float mCurrentX, String mLastMask) {
        //绘制左指标
        //可拖动的阴影(左)
        mainPaint.setColor(hugeCircleShadowColor);
        drawHugeCircleShadow(canvas, mCurrentX);
        //可拖动的大圆(左)
        mainPaint.setColor(Color.WHITE);
        drawHugeCircle(canvas, mCurrentX);
        //可拖动的小圆(左)
        setMoveLittleCicleGradient(mCurrentX);
        mainPaint.setColor(littleCircleColor);
        drawLittleCircle(canvas, mCurrentX);
        //五边形(左)
        drawcursor(canvas, mCurrentX);
        //五边形上的文字
        worldPaint.setColor(cursorTextColor);
        worldPaint.setTextSize(cursorTextSize);
        drawMaskText(canvas, mCurrentX, mLastMask);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                onActionDown(eventX, eventY);
                break;
            case MotionEvent.ACTION_MOVE:
                onActionMove(eventX);
                break;
            case MotionEvent.ACTION_UP:
                onActionUp(eventX, eventY);
                break;
        }
        return true;
    }

    private void onActionDown(float eventX, float eventY) {
        //判断手指落点是否在允许向右滑动的区域内（宽为五边形的宽度再往左加大30px往右加大10px，高为五边形的顶部到小圆的底部的长度再向下加长50px）
        if (eventX > mCurrentLeftX - cursorWidth / 2 - 30 && eventX < mCurrentLeftX + cursorWidth / 2 + 10 && eventY < cursorHeight + marginBetweencursorCircle + rLittleCircle * 2 + 50) {
            enableSlideToRight = true;
            if (enableAnimation) {
                if (null != leftCursorAnimator) leftCursorAnimator.cancel();
                if (null != rightCursorAnimator) rightCursorAnimator.cancel();
            }
        }

        //判断手指落点是否在允许向左滑动的区域内（宽为五边形的宽度再往左加大10px往右加大30px，高为五边形的顶部到小圆的底部的长度再向下加长50px）
        if (eventX > mCurrentRightX - cursorWidth / 2 - 10 && eventX < mCurrentRightX + cursorWidth / 2 + 30 && eventY < cursorHeight + marginBetweencursorCircle + rLittleCircle * 2 + 50) {
            enableSlideToLeft = true;
            if (enableAnimation) {
                if (null != leftCursorAnimator) leftCursorAnimator.cancel();
                if (null != rightCursorAnimator) rightCursorAnimator.cancel();
            }
        }

        //记录一次落点
        mLastActionDownX = eventX;
        mLastActionDownY = eventY;
    }

    private void onActionMove(float eventX) {
        //当允许向右滑动时
        if (enableSlideToRight) {

            if (isMoving) {
                if (eventX < arrayCircleCenter[0]) {
                    //当手指超过最左边的圆点
                    mCurrentLeftX = arrayCircleCenter[0];
                } else {
                    if (eventX < mCurrentRightX - lengthCircleToCircle) {
                        //当左指标还未临近右指标
                        mCurrentLeftX = eventX;
                    } else {
                        if (mCurrentRightX < arrayCircleCenter[arrayCircleCenter.length - 1]) {
                            //当左指标临近右指标，且右指标未到达最右边圆点，将右指标连带一起拖动
                            mCurrentLeftX = eventX;
                            mCurrentRightX = eventX + lengthCircleToCircle;
                        } else {
                            //当右指标到达最右边圆点，阻止左指标继续向右滑动
                            mCurrentLeftX = mCurrentRightX - lengthCircleToCircle;
                            mCurrentRightX = arrayCircleCenter[arrayCircleCenter.length - 1];
                        }
                    }
                }
            } else {
                //不能让它立即滑动，不然会出现闪动的效果(判断与手指落点的位置偏移量是否超过30px）
                if (Math.abs(eventX - mLastActionDownX) > 30) {
                    isMoving = true;
                }
            }
        }

        //参考上方注释
        if (enableSlideToLeft) {
            if (isMoving) {
                if (eventX > arrayCircleCenter[arrayCircleCenter.length - 1]) {
                    mCurrentRightX = arrayCircleCenter[arrayCircleCenter.length - 1];
                } else {
                    if (eventX > mCurrentLeftX + lengthCircleToCircle) {
                        mCurrentRightX = eventX;
                    } else {
                        if (mCurrentLeftX > arrayCircleCenter[0]) {
                            mCurrentRightX = eventX;
                            mCurrentLeftX = eventX - lengthCircleToCircle;
                        } else {
                            mCurrentRightX = mCurrentLeftX + lengthCircleToCircle;
                            mCurrentLeftX = arrayCircleCenter[0];
                        }
                    }
                }
            } else {
                if (Math.abs(eventX - mLastActionDownX) > 30) {
                    isMoving = true;
                }
            }
        }

        setCurrentArrayIndex();

        if (enableSlideToRight || enableSlideToLeft) {
            //判断是否滑动超过了当前展示当前文本区域的边界，当超过时触发一次事件
            checkListenerTriggered();
        }

        postInvalidate();

        //处理滑动冲突
        attemptClaimDrag();
    }

    private void onActionUp(float eventX, float eventY) {
        //滑动完成后让指标归位
        if (enableSlideToRight || enableSlideToLeft) {
            //算离左指标最近的圆点位置（下标）
            int indexLeft = turnToNearestCenter(arrayCircleCenter, mCurrentLeftX);
            //算离右指标最近的圆点位置（下标）
            int indexRight = turnToNearestCenter(arrayCircleCenter, mCurrentRightX);

            if (indexLeft == arrayCircleCenter.length - 1) {
                //当手指滑动到最右边的圆点附件，强行让左指标归位到倒数第二个圆点的位置
                mCurrentLeftX = arrayCircleCenter[indexLeft - 1];
            } else {
                //归位到手指附件圆点位置
                if (enableAnimation) {
                    //开启动画
                    leftCursorAnimator = ObjectAnimator.ofFloat(this, "mCurrentLeftX", mCurrentLeftX, arrayCircleCenter[indexLeft]);
                    leftCursorAnimator.setDuration(animationTime);
                    leftCursorAnimator.addUpdateListener(animation -> invalidate());
                    leftCursorAnimator.start();
                } else {
                    //不开启动画
                    mCurrentLeftX = arrayCircleCenter[indexLeft];
                }

            }
            if (indexRight == 0) {
                if (arrayCircleCenter.length >= 2) {
                    mCurrentRightX = arrayCircleCenter[1];
                    mCurrentLeftX = arrayCircleCenter[0];
                } else {
                    mCurrentRightX = arrayCircleCenter[0];
                }
            } else {
                if (enableAnimation) {
                    rightCursorAnimator = ObjectAnimator.ofFloat(this, "mCurrentRightX", mCurrentRightX, arrayCircleCenter[indexRight]);
                    rightCursorAnimator.setDuration(animationTime);
                    rightCursorAnimator.addUpdateListener(animation -> invalidate());
                    rightCursorAnimator.start();
                } else {
                    mCurrentRightX = arrayCircleCenter[indexRight];
                }
            }
        }

        //判断手指离开屏幕时的位置与手指落下时的位置距离偏差是否过大
        if (eventX > mLastActionDownX - 60 && eventX < mLastActionDownX + 60 && eventY > mLastActionDownY - 60 && eventY < mLastActionDownY + 60) {
            //判断手指落地是否在允许触发单击操作的区域内（宽为控件宽度，高为大阴影圆的直径再加上下各一倍的marginBetweencursorCircle）
            if (eventY > cursorHeight + marginBetweencursorCircle + rLittleCircle / 2 - rHugeCircleShadow / 2 - marginBetweencursorCircle && eventY < cursorHeight + marginBetweencursorCircle + rLittleCircle / 2 + rHugeCircleShadow / 2 + marginBetweencursorCircle) {
                int index = turnToNearestCenter(arrayCircleCenter, eventX);
                //判断离当前左边指标近还是离当前右边指标近
                if (Math.abs(mCurrentRightX - eventX) >= Math.abs(eventX - mCurrentLeftX)) {
                    //离左边近
                    if (enableAnimation) {
                        if (null != leftCursorAnimator) leftCursorAnimator = null;
                        leftCursorAnimator = ObjectAnimator.ofFloat(this, "mCurrentLeftX", mCurrentLeftX, arrayCircleCenter[index]);
                        leftCursorAnimator.setDuration(animationTime);
                        leftCursorAnimator.addUpdateListener(animation -> {
                            mCurrentLeftX = (float) animation.getAnimatedValue();
                            setCurrentArrayIndex();
                            checkListenerTriggered();

                            invalidate();
                        });
                        leftCursorAnimator.start();
                    } else {
                        mCurrentLeftX = arrayCircleCenter[index];
                    }
                    mCurrentLeftArrayIndex = index;
                } else {
                    //离右边近
                    if (enableAnimation) {
                        if (null != rightCursorAnimator) rightCursorAnimator = null;
                        rightCursorAnimator = ObjectAnimator.ofFloat(this, "mCurrentRightX", mCurrentRightX, arrayCircleCenter[index]);
                        rightCursorAnimator.setDuration(animationTime);
                        rightCursorAnimator.addUpdateListener(animation -> {
                            mCurrentRightX = (float) animation.getAnimatedValue();
                            setCurrentArrayIndex();
                            checkListenerTriggered();

                            invalidate();
                        });
                        rightCursorAnimator.start();
                    } else {
                        mCurrentRightX = arrayCircleCenter[index];
                    }
                    mCurrentRightArrayIndex = index;
                }

                //单击操作触发的回调
                checkListenerTriggered();

                //一般是在ACTION_UP中调用这个方法，但是目前我们暂时没有特定的作用
                performClick();
            }
        }

        postInvalidate();
        enableSlideToRight = false;
        enableSlideToLeft = false;
        isMoving = false;
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    //------------------------------------辅助方法区--------------------------------

    //找出离哪个圆心最近
    private int turnToNearestCenter(float[] centerArray, float currentX) {
        if (null != centerArray && centerArray.length != 0) {
            int minIndex = 0;
            float min = Math.abs(centerArray[0] - currentX);
            for (int i = 1; i < centerArray.length; i++) {
                if (Math.abs(centerArray[i] - currentX) < min) {
                    minIndex = i;
                    min = Math.abs(centerArray[i] - currentX);
                }
            }
            return minIndex;
        }
        return 0;
    }

    //设置蓝色细长条的渐变色
    private void setBarLineGradient() {
        if (enableGradient) {
            LinearGradient linearGradient = new LinearGradient(
                    (int) (cursorWidth / 2),
                    (int) (cursorHeight + marginBetweencursorCircle + rLittleCircle),
                    (int) (mMeasureWidth - cursorWidth / 2),
                    (int) (cursorHeight + marginBetweencursorCircle + rLittleCircle),
                    Color.parseColor("#506FFA"),
                    Color.parseColor("#6CAFFF"),
                    Shader.TileMode.CLAMP
            );
            gradientPaint.setShader(linearGradient);
        }
    }

    //设置多个静态小圆的渐变色
    private void setLittleCircleGradient(int i) {
        if (enableGradient) {
            LinearGradient linearGradient = new LinearGradient(
                    (int) (arrayCircleCenter[i] - rLittleCircle),
                    (int) (cursorHeight + marginBetweencursorCircle + rLittleCircle),
                    (int) (arrayCircleCenter[i] + rLittleCircle),
                    (int) (cursorHeight + marginBetweencursorCircle + rLittleCircle),
                    Color.parseColor("#506FFA"),
                    Color.parseColor("#6CAFFF"),
                    Shader.TileMode.CLAMP
            );
            gradientPaint.setShader(linearGradient);
        }
    }

    //可拖动的阴影
    private void drawHugeCircleShadow(Canvas canvas, float mCurrentLOrRX) {
        canvas.drawOval(
                mCurrentLOrRX - rHugeCircleShadow,
                cursorHeight + marginBetweencursorCircle - (rHugeCircleShadow - rLittleCircle),
                mCurrentLOrRX + rHugeCircleShadow,
                cursorHeight + marginBetweencursorCircle - (rHugeCircleShadow - rLittleCircle) + rHugeCircleShadow * 2,
                mainPaint
        );
    }

    //可拖动的大圆
    private void drawHugeCircle(Canvas canvas, float mCurrentLOrRX) {
        canvas.drawOval(
                mCurrentLOrRX - rHugeCircle,
                cursorHeight + marginBetweencursorCircle - (rHugeCircle - rLittleCircle),
                mCurrentLOrRX + rHugeCircle,
                cursorHeight + marginBetweencursorCircle - (rHugeCircle - rLittleCircle) + rHugeCircle * 2,
                mainPaint
        );
    }

    //设置可拖动的小圆的渐变色
    private void setMoveLittleCicleGradient(float mCurrentLOrRX) {
        if (enableGradient) {
            LinearGradient linearGradient = new LinearGradient(
                    (int) (mCurrentLOrRX - rLittleCircle),
                    (int) (cursorHeight + marginBetweencursorCircle + rLittleCircle),
                    (int) (mCurrentLOrRX + rLittleCircle),
                    (int) (cursorHeight + marginBetweencursorCircle + rLittleCircle),
                    Color.parseColor("#506FFA"),
                    Color.parseColor("#6CAFFF"),
                    Shader.TileMode.CLAMP
            );
            gradientPaint.setShader(linearGradient);
        }
    }

    //可拖动的小圆
    private void drawLittleCircle(Canvas canvas, float mCurrentLOrRX) {
        canvas.drawOval(
                mCurrentLOrRX - rLittleCircle,
                cursorHeight + marginBetweencursorCircle,
                mCurrentLOrRX + rLittleCircle,
                cursorHeight + marginBetweencursorCircle + rLittleCircle * 2,
                enableGradient ? gradientPaint : mainPaint
        );
    }

    private void drawcursor(Canvas canvas, float mCurrentLOrRX) {
        cursorShowRect.set((int) (mCurrentLOrRX - cursorWidth / 2), 0, (int) (mCurrentLOrRX + cursorWidth / 2), (int) cursorHeight);
        canvas.drawBitmap(cursorBitmap, cursorBitmapRect, cursorShowRect, mainPaint);
    }

    private void drawMaskText(Canvas canvas, float mCurrentLOrRX, String text) {
        //测量文字宽度并正确绘制
        canvas.drawText(text, mCurrentLOrRX - worldPaint.measureText(text) / 2, cursorTextBaseLineHeight, worldPaint);
    }

    //判断是否滑动超过了当前展示当前文本区域的边界，当超过时触发一次事件
    private void checkListenerTriggered() {
        for (int i = 0; i < arrayCircleCenter.length; i++) {
            if (mCurrentLeftX >= arrayCircleCenter[i] - lengthCircleToCircle / 2 && mCurrentLeftX < arrayCircleCenter[i] + lengthCircleToCircle / 2) {
                String currentMask = mData.get(i).getDescribe();
                if (!TextUtils.equals(mLeftLastMask, currentMask)) {
                    //回调
                    if (null != mListener) {
                        mListener.onLOrRMaskChange(mData.get(i).getValue(), true);
                    }
                    mLeftLastMask = currentMask;
                }
            }
        }
        for (int i = 0; i < arrayCircleCenter.length; i++) {
            if (mCurrentRightX >= arrayCircleCenter[i] - lengthCircleToCircle / 2 && mCurrentRightX < arrayCircleCenter[i] + lengthCircleToCircle / 2) {
                String currentMask = mData.get(i).getDescribe();
                if (!TextUtils.equals(mRightLastMask, currentMask)) {
                    //回调
                    if (null != mListener) {
                        mListener.onLOrRMaskChange(mData.get(i).getValue(), false);
                    }
                    mRightLastMask = currentMask;
                }
            }
        }
    }

    //实时变更临近左右指标的圆点的下标索引
    private void setCurrentArrayIndex() {
        for (int i = 0; i < arrayCircleCenter.length - 1; i++) {
            if (mCurrentLeftX == arrayCircleCenter[i]) {
                mCurrentLeftArrayIndex = i;
                break;
            } else if (mCurrentLeftX > arrayCircleCenter[i] && mCurrentLeftX <= arrayCircleCenter[i + 1]) {
                mCurrentLeftArrayIndex = i + 1;
                break;
            }
        }
        for (int i = 0; i < arrayCircleCenter.length - 1; i++) {
            if (mCurrentRightX == arrayCircleCenter[i + 1]) {
                mCurrentRightArrayIndex = i + 1;
                break;
            } else if (mCurrentRightX >= arrayCircleCenter[i] && mCurrentRightX < arrayCircleCenter[i + 1]) {
                mCurrentRightArrayIndex = i;
                break;
            }
        }
    }

    private void attemptClaimDrag() {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

    //------------------------------------设置本控件各个属性的方法（未添加完，可根据需要自行添加）--------------------------------

    public void setData(List<PointData> list) {
        mData = list;

        if (!list.isEmpty()) {
            mLeftLastMask = list.get(0).getDescribe();
            mRightLastMask = list.get(list.size() - 1).getDescribe();
            littleCircleNumber = mData.size();
        }
    }

    public void setListener(OnLOrRMaskChangeListener mListener) {
        this.mListener = mListener;
    }

    public void setLeftCursorIndex(int index) {
        if (null != mData) {
            if (index < mData.size()) {
                mCurrentLeftX = arrayCircleCenter[index];
                mCurrentLeftArrayIndex = index;
                mLeftLastMask = mData.get(index).getDescribe();
                postInvalidate();
            }
        }
    }

    public void setRightCursorIndex(int index) {
        if (null != mData) {
            if (index < mData.size()) {
                mCurrentRightX = arrayCircleCenter[index];
                mCurrentRightArrayIndex = index;
                mRightLastMask = mData.get(index).getDescribe();
                postInvalidate();
            }
        }
    }

    @Keep  //ObjectAnimator指定动画方法，对方法名称有要求，所以要加上改注解
    public void setMCurrentLeftX(float newX) {
        mCurrentLeftX = newX;
    }

    @Keep
    public void setMCurrentRightX(float newX) {
        mCurrentRightX = newX;
    }

    public void reset() {
        if (null != mData && !mData.isEmpty()) {
            setLeftCursorIndex(0);
            setRightCursorIndex(mData.size() - 1);
        }
    }

    public void setCursorTextColor(@ColorInt int cursorTextColor) {
        this.cursorTextColor = cursorTextColor;
    }

    public void setLittleCircleRadius(float rLittleCircle) {
        this.rLittleCircle = rLittleCircle;
    }

    public void setTextSize(float textSize) {
        this.cursorTextSize = textSize;
    }

    public void setCursorWidth(float cursorWidth) {
        this.cursorWidth = cursorWidth;
    }

    public void setCursorHeight(float cursorHeight) {
        this.cursorHeight = cursorHeight;
    }


    //------------------------------------数据类--------------------------------

    public static class PointData {
        //数据元素
        private String value;
        //数据描述
        private String describe;

        public PointData() {

        }

        public PointData(String value, String describe) {
            this.value = value;
            this.describe = describe;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getDescribe() {
            return describe;
        }

        public void setDescribe(String describe) {
            this.describe = describe;
        }
    }

    public interface OnLOrRMaskChangeListener {
        //lOrR为true时表示left， false表示right
        void onLOrRMaskChange(String valueAfterChange, boolean lOrR);
    }
}
