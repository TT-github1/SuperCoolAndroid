package com.tthappy.supercoolandroid.uidemo.ui.skills.measure

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import com.tthappy.supercoolandroid.log.HpLog

class FlowBoxLayout(context: Context, attrs: AttributeSet) : ViewGroup(context, attrs) {


    /**
     *  作为一个ViewGroup，需要负起测量每个子View的重任
     *  并且照顾好子View后，还需要对自己负责，测量自己并且设置测量宽高
     */
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        // 1.首先获取子View并测量
        measureChildren(widthMeasureSpec, heightMeasureSpec)
//        for (index in 0 until childCount) {
//            val child = getChildAt(index)
//            measureChild(child, widthMeasureSpec, heightMeasureSpec)
//        }

        setMeasuredDimension(layoutParams.width, layoutParams.height)
        HpLog.e("layoutParams.width = ${layoutParams.width}    layoutParams.height = ${layoutParams.height}")
    }

    // 在布局时需要注意考虑padding和margin的情况，如果是需要多行布局，还需要留言换行时的状态重置。
    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        HpLog.e("左 = $l   上 = $t    右 = $r     下 = $b")

        // 需要考虑父View的padding
        var currentStart = paddingStart
        var currentTop = paddingTop
        var currentLine = 1
        val validWidth = measuredWidth - paddingStart - paddingEnd
        val validHeight = measuredHeight - paddingTop - paddingBottom
        var currentLineHeight = 0

        for (index in 0 until childCount) {
            val child = getChildAt(index)
            val cWidth = child.measuredWidth
            val cHeight = child.measuredHeight
            val cMarginStart = (child.layoutParams as MarginLayoutParams).leftMargin
            val cMarginTop = (child.layoutParams as MarginLayoutParams).topMargin
            val cMarginEnd = (child.layoutParams as MarginLayoutParams).rightMargin
            val cMarginBottom = (child.layoutParams as MarginLayoutParams).bottomMargin

            if (currentStart + cMarginStart + child.measuredWidth + cMarginEnd > validWidth) {
                // 换行，累计值和复位状态
                currentLine++
                currentTop += currentLineHeight
                currentLineHeight = 0
                currentStart = paddingStart
            }

            child.layout(
                    currentStart + cMarginStart,
                    currentTop + cMarginTop,
                    currentStart + cMarginStart + child.measuredWidth,
                    currentTop + cMarginTop + child.measuredHeight
            )
            currentStart += cMarginStart + child.measuredWidth + cMarginEnd
            currentLineHeight = currentLineHeight.coerceAtLeast(cMarginTop + cHeight + cMarginBottom)
        }
    }

    /**
     *  在inflate方法解析xml布局文件的时候，会调用父View的generateLayoutParams来解析布局中设置的相关的属性值
     *  而ViewGroup的默认实现不是MarginLayoutParams，我们需要拿到margin相关属性，所以需要重写该方法
     */
    override fun generateLayoutParams(attrs: AttributeSet?): MarginLayoutParams {
        return MarginLayoutParams(context, attrs)
    }
}