package com.tthappy.supercoolandroid.aptprocessor;

import com.google.auto.service.AutoService;
import com.tthappy.supercoolandroid.annotation.HpInfo;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/6 14:22
 * Update Date:
 * Modified By:
 * Description:
 */

@AutoService(Processor.class)
public class HpInfoProcessor extends AbstractProcessor {

    private Messager mMessager;
//    private Elements mElementsUtils;
    private Filer mFiler;
    private Map<String, String> mProxyMap = new HashMap<>();

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        mMessager.printMessage(Diagnostic.Kind.NOTE, "start processing...");

        TypeElement kadkfjakd = null;

        mProxyMap.clear();
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(HpInfo.class);
        for (Element element : elements) {
            TypeElement classElement = (TypeElement) element;
            kadkfjakd = (TypeElement) element;
            String fullClassName = classElement.getQualifiedName().toString();

            String info = mProxyMap.computeIfAbsent(fullClassName, n -> n);
            mMessager.printMessage(Diagnostic.Kind.NOTE, "========>  info : " + info);
        }


        Writer writer = null;
        try {
            for (String s : mProxyMap.keySet()) {
                String omg = mProxyMap.get(s);
                JavaFileObject javaFileObject = mFiler.createSourceFile(kadkfjakd.getSimpleName().toString(), kadkfjakd);
                writer = javaFileObject.openWriter();
                writer.write(createJavaFile(kadkfjakd, omg));
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        mMessager.printMessage(Diagnostic.Kind.NOTE, "end processing...");
        return true;
    }

    private String createJavaFile(TypeElement kadkfjakd, String name) {
        String text = "package com.tthappy.supercoolandroid.java.test;\n" +
                "\n" +
                "public class " + kadkfjakd.getSimpleName().toString() + " {\n" +
                "    public static void main(String[] args) {\n" +
                "        System.out.println(\"" + name + "\");\n" +
                "    }\n" +
                "}\n";
        return text;
    }

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);

        mMessager = processingEnv.getMessager();
        mFiler = processingEnv.getFiler();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> supportTypes = new LinkedHashSet<>();
        supportTypes.add(HpInfo.class.getCanonicalName());
        return supportTypes;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
