package com.tthappy.supercoolandroid.uihome.main.category.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

//import com.tthappy.nativedemo.tree.AVLTree;
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils;
import com.tthappy.supercoolandroid.java.datastruct.tree.HuffmanTree;
import com.tthappy.supercoolandroid.java.datastruct.tree.PrintTreeUtils;
import com.tthappy.supercoolandroid.uihome.R;


/**
 * Author:      tfhe
 * Create Date: Created in 2020/10/10 18:21
 * Update Date:
 * Modified By:
 * Description:
 */
public class HomeFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewview = inflater.inflate(R.layout.fragment_main, container, false);
        initView(viewview);
        initData();
        initListener();

        return viewview;
    }

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    private void initView(View view) {
        RecyclerView mRecyclerView = view.findViewById(R.id.rv_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setOverScrollMode(View.OVER_SCROLL_NEVER);

        HomeFragmentAdapter adapter = new HomeFragmentAdapter(new HomeData().getData());

        adapter.setOnItemClickListener((adapterr, viewr, position) ->
                BridgeUtils.bridgeWithUrl(adapter.getItem(position).getUrl())
//                test()
        );

        mRecyclerView.setAdapter(adapter);
    }

    private void initData() { }

    private void test() {
//        RedBlackTree<Integer> avlTree = new RedBlackTree<>();
//        for (int i = 1; i < 11; i++) {
//            avlTree.add(i);
//        }
////        avlTree.deleteMin();
//
//        new RedBlackPrintTreeUtils<Integer>().show(avlTree.head);

        HuffmanTree<Integer> huffmanTree = new HuffmanTree<>();
        huffmanTree.add(2, 2);
        huffmanTree.add(4, 4);
        huffmanTree.add(5, 5);
        huffmanTree.add(7, 7);
        huffmanTree.add(1, 1);
        huffmanTree.add(14, 14);
        huffmanTree.add(22, 22);
        huffmanTree.add(9, 9);
        huffmanTree.head = huffmanTree.createTree();
        new PrintTreeUtils<Integer>().show(huffmanTree.head);
    }

    private void initListener() {

    }
}
