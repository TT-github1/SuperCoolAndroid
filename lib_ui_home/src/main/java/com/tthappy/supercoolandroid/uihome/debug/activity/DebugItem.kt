package com.tthappy.supercoolandroid.uihome.debug.activity

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/18 16:09
 * Update Date:
 * Modified By:
 * Description:
 */
data class DebugItem (
    val icon: Int?,
    val name: String,
    val desc: String
)