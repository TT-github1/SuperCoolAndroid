package com.tthappy.supercoolandroid.uidemo.ui.customcontrol.myviews.blingcircle;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.tthappy.supercoolandroid.uidemo.R;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/4/1 8:18
 * Update Date:
 * Modified By:
 * Description:
 */
public class BLingCirCleView extends View {

    //本控件宽高
    private float mViewWidth;
    private float mViewHeight;

    //本控件半径（感觉和上面重复了）
    private float rViewCircle;

    //外围隐形圆的半径
    private float rVirtualCircle;
    //核心圆的半径（主观一眼看过去就看到的那个圆的半径）
    private float rMainCircle;
    //将圆分成几等分（有多少根细线延出来）
    private int lineNumber;
    //所有细线的宽度
    private float lineWidth;

    //主画笔
    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public BLingCirCleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.BLingCirCleView);
        lineNumber = ta.getInteger(R.styleable.BLingCirCleView_lineNumber, 120);
//        lineWidth = ta.getFloat(R.styleable.BLingCirCleView_lineWidth, DimenUtils.dp2px(3));
        lineWidth = ta.getFloat(R.styleable.BLingCirCleView_lineWidth, 9);
        ta.recycle();

        mPaint.setColor(context.getColor(R.color.main_blue));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(lineWidth);
        mPaint.setStrokeCap(Paint.Cap.ROUND);

        Disposable disposable = Observable.interval(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> {
                    postInvalidate();
                });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);

        if (widthMode == MeasureSpec.AT_MOST) setMeasuredDimension(widthSize, widthSize);

        mViewWidth = getMeasuredWidth();
        rViewCircle = mViewWidth / 2;
        rVirtualCircle = rViewCircle * 4 / 5;
        rMainCircle = rViewCircle * 2 / 3;

        Random random = new Random();
        random.nextFloat();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawOval(
                rViewCircle - rMainCircle,
                rViewCircle - rMainCircle,
                rViewCircle + rMainCircle,
                rViewCircle + rMainCircle,
                mPaint
        );

        for (int i = 0; i < lineNumber; i++) {
            float random = (float) ((10 + Math.random() * 85) / 100);
//            LogUtils.e(random + "");

            canvas.drawLine(
                    (float) (rViewCircle + rVirtualCircle * Math.sin(Math.toRadians(360d / lineNumber) * i)),
                    (float) (rViewCircle + rVirtualCircle * Math.cos(Math.toRadians(360d / lineNumber) * i)),
                    (float) (rViewCircle + (rVirtualCircle + (rViewCircle - rVirtualCircle) * random) * Math.sin(Math.toRadians(360d / lineNumber) * i)),
                    (float) (rViewCircle + (rVirtualCircle + (rViewCircle - rVirtualCircle) * random) * Math.cos(Math.toRadians(360d / lineNumber) * i)),
                    mPaint
            );

        }
    }
}

