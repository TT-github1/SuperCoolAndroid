package com.tthappy.supercoolandroid.uidemo.data.entity.entity.base

data class BaseResult<T> (
        val data: T,
        val errorCode: Int,
        val errorMsg: String
)