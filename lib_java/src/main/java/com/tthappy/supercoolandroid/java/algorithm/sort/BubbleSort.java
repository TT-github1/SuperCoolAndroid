package com.tthappy.supercoolandroid.java.algorithm.sort;

import com.tthappy.supercoolandroid.java.datamock.HpDataLib;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/23 17:30
 * Update Date:
 * Modified By:
 * Description: 冒泡排序 （两两交换）
 */
public class BubbleSort {

    public static void main(String[] args) {
        int[] arr = HpDataLib.getArrI(16);
        int[] arr2 = new int[16];
        System.arraycopy(arr, 0, arr2, 0, arr.length);
        System.out.println(Arrays.toString(arr));
        sort(arr);
        System.out.println(Arrays.toString(arr));

        System.out.println(Arrays.toString(arr2));
        sorter(arr2);
        System.out.println(Arrays.toString(arr2));
    }
    
    public static void sort(int[] arr) {
        // 外层循环 控制冒几次泡
        for(int i = 0; i < arr.length - 1; i++) {
            // 内层循环 控制一次冒泡的有序执行
            for(int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j+1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = tmp;
                }
            }
        }
    }

    private static void sorter(int[] arr) {
        if (arr == null  || arr.length < 2) return;
        int end = arr.length - 1;
        while (end > 1) {
            for (int i = 0; i < end; i++) {
                if (arr[i] > arr[i + 1]) {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
            end--;
        }
    }
}
