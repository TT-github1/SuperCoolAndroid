package com.tthappy.supercoolandroid.uidemo.ui.socket

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import android.os.Message
import com.tthappy.supercoolandroid.log.HpLog
import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.ServerSocket

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/23 14:26
 * Update Date:
 * Modified By:
 * Description:
 */
object SocketServerUtils {

    lateinit var mHandler: Handler
    lateinit var outputStream: DataOutputStream

    init {
        Thread{
            Looper.prepare()
            mHandler = @SuppressLint("HandlerLeak")
            object : Handler() {
                override fun handleMessage(msg: Message) {
                    outputStream.writeByte(1)
                    outputStream.writeInt(4)
                    outputStream.writeInt(msg.arg1)
                    outputStream.writeInt(msg.arg2)
                    outputStream.flush()
                }
            }
            Looper.loop()
        }.start()
    }

    fun getHandler() = mHandler

    fun startServer(handler: Handler) {
        Thread {
            val server = ServerSocket(12345)
            HpLog.e("服务端开启..."::toString)
            val socket = server.accept()
            HpLog.e("服务端连接成功..."::toString)
            val inputStream = DataInputStream(socket.getInputStream())
            outputStream = DataOutputStream(socket.getOutputStream())

            while(true) {
                val b = inputStream.readByte()
                val len = inputStream.readInt()
                if (len == 4) {
                    val x = inputStream.readInt()
                    val y = inputStream.readInt()
                    HpLog.e("收到客户端发来的坐标=>  x: $x , y: $y"::toString)

                    val msg = Message.obtain()
                    msg.arg1 = x
                    msg.arg2 = y
                    handler.sendMessage(msg)
                }
            }

        }.start()
    }

}