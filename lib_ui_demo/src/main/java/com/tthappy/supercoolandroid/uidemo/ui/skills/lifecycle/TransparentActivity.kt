package com.tthappy.supercoolandroid.uidemo.ui.skills.lifecycle

import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityTransparentDemoBinding

@HpRouterData(name = "透明页面", icon = "R.drawable.icon_double_drag_bar_pentagon", desc = "透明页面，辅助展示生命周期", isNew = false, url = BridgeConstansUIDemo.TRANSPARENT)
@Route(path = BridgeConstansUIDemo.TRANSPARENT)
class TransparentActivity: BaseActivity<ActivityTransparentDemoBinding, BaseViewModel>() {
    override fun initView() {
        viewBinding.tvView.setOnClickListener {
            BridgeUtils.bridgeWithUrl(BridgeConstansUIDemo.LIFECYCLE_SHOW)
        }
    }
    override fun initData() {}
    override fun observeData() {}
}