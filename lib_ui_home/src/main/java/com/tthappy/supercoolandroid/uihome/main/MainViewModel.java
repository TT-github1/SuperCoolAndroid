package com.tthappy.supercoolandroid.uihome.main;

import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel;
import com.tthappy.supercoolandroid.uihome.data.UiHomeRepository;

import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/10 14:47
 * Update Date:
 * Modified By:
 * Description:
 */
public class MainViewModel extends BaseViewModel {
    public List<String> getMineFunctionsList() {
        return UiHomeRepository.INSTANCE.getMineFunctionsList();
    }
}
