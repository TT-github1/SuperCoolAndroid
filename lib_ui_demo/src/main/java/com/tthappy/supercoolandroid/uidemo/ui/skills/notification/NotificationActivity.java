package com.tthappy.supercoolandroid.uidemo.ui.skills.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;

@Route(path = BridgeConstansUIDemo.NOTIFICATION)
public class NotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_demo);

        //创建通知管理类
        NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        //创建一个渠道(渠道功能在Android8.0之前是没有的)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //第一个参数是渠道id，第二个参数是渠道名称，第三个参数是通知的重要等级
            NotificationChannel channel = new NotificationChannel("normal", "normal", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);

            NotificationChannel channel2 = new NotificationChannel("high", "high", NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel2);
        }

        TextView textView = findViewById(R.id.mdjfb);
        textView.setOnClickListener(v -> {
            //通知点击的逻辑
//            Intent intent = new Intent(this, MovieActivity.class);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
//
//            //设置通知各种属性
//            Notification notification = new NotificationCompat.Builder(this, "high")
//                    .setContentTitle("Title")
//                    .setContentText("This is content text")
//                    .setSmallIcon(R.drawable.icon_search)
//                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.bg_hills))
//                    .setContentIntent(pendingIntent)
//                    .setAutoCancel(true)
//                    .build();
//
//            //上面的id是渠道的id，这里第一个参数id是通知的id
//            manager.notify(1, notification);
        });
    }
}