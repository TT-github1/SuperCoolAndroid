#### 提起数字签名，要反应-私钥加密；公私钥能互解；数字证书中包含公钥；数字证书的数字签名能用数字证书上标注的签发机构的证书（即设备的根证书，携带了公钥）验证

1.Client首先发送本地的TLS/SSL的协议版本、支持的加密算法套件和一个客户端随机数R1
2.Sever确认TLS/SSL协议版本号。从Client端支持的加密算法套件中选取一个，并生成一个服务端随机数R2，一起返回给Client
3.Server向Client发送自己的CA证书（包含公钥）、和证书签名（使用某根证书的私钥进行加密）
4.Client判断证书签名与CA证书是否合法有效。首先将证书内容进行hash得到值A，再根据证书上的颁发机构证书信息取得颁发机构的证书公钥，对数字签名解密得到值B，将A和B比较，相等证明可信
5.Client生成随机数 Pre-Master并使用服务器的公钥对 Pre-Master 进行加密，然后发送给Server。自此Client和Server都拥有R1、R2和Pre-Master。
  两端便可以通过这三个随机数独立生成对称加密的会话密钥了，避免了对称加密的会话密钥的传输，同时可以通过会话密钥生成6个密钥（P1~P6）用作后续身份验证
6.Client 使用 P1 将之前的握手信息（？？？）的 hash 值加密并发送给 Server。Client 发送握手结束消息
7.Server 计算之前的握手信息的 hash 值，并与 P1 解密客户端发送的握手信息的 hash 对比校验
8.验证通过后，使用 P2 将之前的握手信息的 hash 值加密并发送给 Client
9.Client 计算之前的握手信息的 hash 值，并与 P2 解密 Server 发送的握手信息的 hash 对比校验
10.验证通过后，开始发起 HTTPS 请求。

一个正常的服务器证书都包含哪些内容？（仅列举方便理解https工作流程的内容）
1.签名算法（用 签发机构的证书的私钥 对 服务器的证书的哈希值 进行加密时用的算法，非对称加密）
2.签名哈希算法（算 服务器证书的哈希值 时用的算法）
3.颁发者（签发这个证书的机构）
4.使用者（网站本身）
5.公钥（服务器的公钥）
6.指纹（即证书签名，先对证书用 签名哈希算法 算得一个哈希值，再对这个哈希值 用签发机构的私钥和签名算法 进行加密，得到指纹）


如何验证服务器证书是可信的？为什么
1.首先证书中包含 证书的哈希算法、证书的数字签名、证书的签发机构
2.通过证书的签发机构信息去根证书中寻找签发机构的证书，拿上面的公钥去解密服务器证书的数字签名
3.使用服务器证书上的哈希算法对证书进行哈希，将得到的值与上一步的结果进行对比，一致则可信