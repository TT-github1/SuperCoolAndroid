package com.tthappy.supercoolandroid.java.reflect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/13 17:20
 * Update Date:
 * Modified By:
 * Description:
 */
public class Sockett {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("169.254.17.80", 8888);

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            writer.println("yes");
            writer.flush();

            while (true) {
                String readString = reader.readLine();
                System.out.println(readString);
                if (!"".equals(readString)) {
                    break;
                }
            }
            reader.close();
            writer.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
