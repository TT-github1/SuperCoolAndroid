package com.tthappy.supercoolandroid.java.algorithm.dynamicprogramming;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/27 10:57
 * Update Date:
 * Modified By:
 * Description: 求连续子数组最大值
 */
public class ContinuousSubArray {
    public static void main(String[] args) {
//        int[] arr = {-1, -2, 1, 2, -2, 2, -1, 2, -2, -2};
        int[] arr = {4, -6, 11, 2, -12, 2, 6, 2, -2, -62};
        System.out.println(find(arr, 0, arr.length - 1));
        System.out.println(anotherFind(arr, 0, arr.length - 1));
        System.out.println(findMaxSumOfArray(arr));
        System.out.println(findByDynamicProgramming(arr));
    }

    private static int find(int[] arr, int start, int end) {
        if (arr == null || arr.length == 0) return -1;
        if (start == end) return arr[start];

        int maxResult = arr[start];
        int count = arr[start];
        for(int i = start + 1; i < end + 1; i++) {
            count += arr[i];
            maxResult = Math.max(maxResult, count);
        }

        return Math.max(maxResult, find(arr, start + 1, end));
    }

    private static int anotherFind(int[] arr, int start, int end) {
        if (arr == null || arr.length == 0) return -1;
        if (start == end) return arr[start];

        int maxResult = arr[start];
        for (int i = start; i < end; i++) {
            int count = arr[i];
            for(int j = i + 1; j < end + 1; j++) {
                count += arr[j];
                maxResult = Math.max(maxResult, count);
            }
        }

        return maxResult;
    }

    /**
     * 动态规划，用res[i]表示以第i个元素结尾的最大和
     * res[i]中最大者即为最大连续子序列的和
     * res[i]=max(res[i-1] + data[i] , data[i])
     */
    public static int findMaxSumOfArray(int[] array) {
        if (array == null || array.length == 0) return Integer.MIN_VALUE;
        int endAsI = array[0];    //endASI就是res[i-1]
        int result = endAsI;      //result存取最大的结果，最后返回的就是这个
        for (int i = 1; i < array.length; i++) {
            endAsI = Math.max(endAsI + array[i], array[i]);
            if (endAsI > result) result = endAsI;
        }
        return result;
    }

    private static int findByDynamicProgramming(int[] array) {
        if (array == null || array.length == 0) return Integer.MIN_VALUE;

        int startAsI = array[array.length - 1];
        int result = startAsI;

        for (int i = array.length - 2; i >= 0; i--) {
            startAsI = Math.max(startAsI + array[i], array[i]);
            if (result < startAsI) result = startAsI;
        }

        return result;
    }

}
