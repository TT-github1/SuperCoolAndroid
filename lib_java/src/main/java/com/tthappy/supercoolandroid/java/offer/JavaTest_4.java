package com.tthappy.supercoolandroid.java.offer;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/23 16:14
 * Update Date:
 * Modified By:
 * Description: 将一个矩阵顺时针打印
 */
public class JavaTest_4 {
    public static void main(String[] args) {
        int[][] arr = {
                {1,  2,  3},
                {8,  9,  4},
                {7,  6,  5},
        };

        printArray(arr);
    }

    private static void printArray(int[][] arr) {
        if (arr == null || arr.length == 0) return;
        int left = 0;
        int top = 0;
        int right = arr[0].length - 1;
        int bottom = arr.length - 1;
        int index = 0;
        int[] newArr = new int[(right + 1) * (bottom + 1)];

        while (true) {
            for (int i = left; i <= right; i++) newArr[index++] = arr[top][i];
            if (++top > bottom) break; // 这个判断是精髓
            for(int i = top; i <= bottom; i++) newArr[index++] = arr[i][right];
            if (--right < left) break;
            for(int i = right; i >= left; i --) newArr[index++] = arr[bottom][i];
            if (--bottom < top) break;
            for(int i = bottom; i >= top; i--) newArr[index++] = arr[i][left];
            if (++left > right) break;
        }

        System.out.println(Arrays.toString(newArr));
    }
}
