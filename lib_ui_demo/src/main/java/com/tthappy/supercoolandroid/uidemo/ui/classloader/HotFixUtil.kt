package com.tthappy.supercoolandroid.uidemo.ui.classloader

import java.io.File
import java.io.IOException
import java.lang.reflect.Method

/**
 *  热修复的时机越早越好
 *  如果要修复的类已经被加载过了，热修复就会失效
 */

object HotFixUtil {

    /**
     *  我们的目的是拿到BaseDexClassLoaser的pathList的dexElements对象（一个dex包的数组），
     *  用我们的修复包来生成一个Element对象，然后插入到dexElements对象的最前面，
     *  这样类加载的时候会先从我们的修复包中去加载
     */
    fun fix(classLoader: ClassLoader) {
        //1.反射获取父类BaseDexClassLoader的pathList变量
        val pathListField = classLoader.javaClass.superclass.getDeclaredField("pathList")
        pathListField.isAccessible = true
        val pathList = pathListField.get(classLoader)

        //2.反射获取dexElements对象
        val dexElementsField = pathList.javaClass.getDeclaredField("dexElements")
        dexElementsField.isAccessible = true
        val dexElements = dexElementsField.get(pathList) as Array<*>

        //3.反射获取用于生成dexElements对象的方法
        val makeDexElementsMethod: Method = pathList.javaClass.getDeclaredMethod("makeDexElements", List::class.java, File::class.java, List::class.java, ClassLoader::class.java)
        makeDexElementsMethod.isAccessible = true
        val file = File("/sdcard/patch.jar")
        val exceptionList = mutableListOf<IOException>()
        val fixdex = makeDexElementsMethod.invoke(pathList, arrayListOf(file), null, exceptionList, classLoader) as Array<*>

        //4.生成一个新的dexElements数组
        val len = dexElements.size + fixdex.size
        val newDexElements = java.lang.reflect.Array.newInstance(dexElements[0]!!.javaClass, len)
        System.arraycopy(fixdex, 0, newDexElements, 0, fixdex.size)
        System.arraycopy(dexElements, 0, newDexElements, fixdex.size, dexElements.size)

        //5.用新的包替换旧的包
        dexElementsField.set(pathList, newDexElements)
    }

}