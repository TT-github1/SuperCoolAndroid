package com.tthappy.supercoolandroid.java.designpattern.singleton;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/4 8:45
 * Update Date:
 * Modified By:
 * Description: 懒汉式 - 单线程适用
 */
public class SecondNotSaveLazySingleton {

    private SecondNotSaveLazySingleton() {}

    private static volatile SecondNotSaveLazySingleton instance;

    public static SecondNotSaveLazySingleton getInstance() {
        if (instance == null) {
            instance = new SecondNotSaveLazySingleton();
        }
        return instance;
    }

}
