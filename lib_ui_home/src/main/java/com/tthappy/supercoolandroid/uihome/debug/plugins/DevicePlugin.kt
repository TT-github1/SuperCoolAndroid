package com.tthappy.supercoolandroid.uihome.debug.plugins

import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils
import com.tthappy.supercoolandroid.uihome.debug.DebugPlugin
import com.tthappy.supercoolandroid.uihome.debug.activity.DebugActivity

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/24 15:51
 * Update Date:
 * Modified By:
 * Description:
 */
class DevicePlugin(mActivity: DebugActivity): DebugPlugin(mActivity) {
    override fun icon(): Nothing? = null

    override fun name() = "设备属性和信息"

    override fun value() = "长、宽、ip、mac"

    override fun doWhat() {
        BridgeUtils.bridgeWithUrl(BridgeConstansUIDemo.EQUIPMENTATTRIBUTE)
    }
}