//package com.tthappy.supercoolandroid.java.javarouter;
//
//import com.google.auto.service.AutoService;
//import com.squareup.javapoet.JavaFile;
//import com.squareup.javapoet.MethodSpec;
//import com.squareup.javapoet.TypeSpec;
//import com.tthappy.annotation.JavaRouter;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//
//import javax.annotation.processing.AbstractProcessor;
//import javax.annotation.processing.Filer;
//import javax.annotation.processing.Messager;
//import javax.annotation.processing.ProcessingEnvironment;
//import javax.annotation.processing.Processor;
//import javax.annotation.processing.RoundEnvironment;
//import javax.annotation.processing.SupportedAnnotationTypes;
//import javax.annotation.processing.SupportedOptions;
//import javax.annotation.processing.SupportedSourceVersion;
//import javax.lang.model.SourceVersion;
//import javax.lang.model.element.Element;
//import javax.lang.model.element.Modifier;
//import javax.lang.model.element.TypeElement;
//import javax.lang.model.util.Elements;
//import javax.lang.model.util.Types;
//import javax.tools.Diagnostic;
//
///**
// * Author:      tfhe
// * Create Date: Created in 2021/11/23 8:54
// * Update Date:
// * Modified By:
// * Description:
// */
//@AutoService(Processor.class) //启用服务
//@SupportedAnnotationTypes("com.tthappy.annotation.JavaRouter")
//@SupportedSourceVersion(SourceVersion.RELEASE_7) //环境的版本
//@SupportedOptions("student") //来自APP的传参
//public class JavaRouterProcessor extends AbstractProcessor {
//
//    //操作类、函数、属性
//    private Elements elementTool;
//    //包含用于操作各种类的信息
//    private Types typeTool;
//    //日志
//    private Messager messager;
//    //文件生成器
//    private Filer filer;
//
//    @Override
//    public synchronized void init(ProcessingEnvironment processingEnv) {
//        super.init(processingEnv);
//
//        elementTool = processingEnv.getElementUtils();
//        messager = processingEnv.getMessager();
//        filer = processingEnv.getFiler();
//
//        String value = processingEnv.getOptions().get("student");
//        messager.printMessage(Diagnostic.Kind.NOTE, ">>>>>>>>>>>>>>>" + value);
//
//    }
//
//    //编译时调用
//    @Override
//    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
//
//        messager.printMessage(Diagnostic.Kind.NOTE, ">>>>>>>>>>>>>>>  process starting");
//
//        //获取被 JavaRouter注解的类节点信息
//        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(JavaRouter.class);
//        for (Element element : elements) {
//
//            //main方法
//            MethodSpec mainMethod = MethodSpec.methodBuilder("main")
//                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
//                    .returns(void.class)
//                    .addParameter(String[].class, "args")
//                    .addStatement("$T.out.println($S)", System.class, "Hello, JavaPoet!")
//                    .addStatement("$T.out.println($S)", System.class, "Hello|||| JavaPoet!")
//                    .addStatement("$T<String> list = new $T<>()", List.class, ArrayList.class)
//                    .addStatement("list.add(\"笑嘻嘻\")")
//                    .addStatement("list.add(\"蛤蟆皮\")")
//                    .addStatement("list.add(\"鬼脚七\")")
//                    .addStatement("$T.out.println(list.toString())", System.class)
//                    .build();
//
//            //类
//            TypeSpec targetClass = TypeSpec.classBuilder("TargetTest")
//                    .addMethod(mainMethod)
//                    .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
//                    .build();
//
//            //包
//            JavaFile packagef = JavaFile.builder("com.tthappy.autofile", targetClass).build();
//
//            try {
//                packagef.writeTo(filer);
//            } catch (IOException e) {
//                e.printStackTrace();
//                messager.printMessage(Diagnostic.Kind.NOTE, "自动生成注解对应的文件失败，异常为：" + e.getMessage());
//            }
//
//        }
//        return false;
//    }
//}




//plugins {
//        id 'java-library'
//        }
//
//        dependencies {
//        //背后的服务（编译时工作的基础）
//        compileOnly "com.google.auto.service:auto-service:1.0-rc4"
//        annotationProcessor "com.google.auto.service:auto-service:1.0-rc4"
//        //帮助外面通过类调用的形式来生成java代码
//        implementation "com.squareup:javapoet:1.9.0"
//        implementation project(":lib_annotation")
//        }
//
//        java {
//        sourceCompatibility = JavaVersion.VERSION_1_7
//        targetCompatibility = JavaVersion.VERSION_1_7
//        }
