package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/30 9:12
 * Update Date:
 * Modified By:
 * Description: 寻找 字符串数组 中元素的 最长 公共前缀
 */
public class JavaTest_19 {
    public static void main(String[] args) {
        String[] arr = {"Happy", "Harry", "Half"};
        System.out.println(find(arr));
    }

    private static String find(String[] arr) {
        if (arr == null || arr.length == 0) return "";
        if (arr.length == 1) return arr[0];

        String res = arr[0];
        for (int i = 1; i < arr.length; i++) {
            int j = 0;
            for (; j < res.length() && j < arr[i].length(); j++) {
                if (res.charAt(j) != arr[i].charAt(j)) break;
            }
            res = res.substring(0, j);
            if (res.equals("")) return res;
        }

        return res;
    }
}
