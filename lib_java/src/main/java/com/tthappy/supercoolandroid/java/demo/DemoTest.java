package com.tthappy.supercoolandroid.java.demo;

import javax.sql.rowset.WebRowSet;

public class DemoTest {

    static {
        System.out.println("ClassLoader success!");
    }

    public static void main(String[] args) {
//        ClassLoader loader1 = Object.class.getClassLoader();
        ClassLoader loader1 = String.class.getClassLoader();
        System.out.println(loader1);
        ClassLoader loader2 = WebRowSet.class.getClassLoader();
        System.out.println(loader2);
        ClassLoader loader3 = DemoTest.class.getClassLoader();
        System.out.println(loader3);
    }
}