package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/5 9:39
 * Update Date:
 * Modified By:
 * Description: 求整数数组中具有最大和的连续子数组
 */
public class JavaTest_26 {
    public static void main(String[] args) {
        int[] arr = {-2, 1, 1, 4, -5, 2, 2, -4, 9, 0, -7, 1, 5};
        System.out.println(find(arr));
        System.out.println(dynamicProgramming(arr));
    }

    // 穷举
    private static int find(int[] arr) {
        int result = 0;

        for (int i = 0; i < arr.length; i++) {
            int sum = 0;
            for (int j = i; j < arr.length; j++) {
                sum += arr[j];
                result = Math.max(result, sum);
            }
        }

        return result;
    }

    // 动态规划
    private static int dynamicProgramming(int[] array) {
        if (array == null || array.length == 0) return Integer.MIN_VALUE;
        int endAsI = array[0];
        int result = endAsI;
        for (int i = 1; i < array.length; i++) {
            endAsI = Math.max(endAsI + array[i], array[i]);
            if (endAsI > result) result = endAsI;
        }
        return result;
    }
}
