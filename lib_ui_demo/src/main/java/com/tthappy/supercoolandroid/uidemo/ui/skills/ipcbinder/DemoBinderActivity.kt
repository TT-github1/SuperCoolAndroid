package com.tthappy.supercoolandroid.uidemo.ui.skills.ipcbinder

import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityBinderDemoBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/1 9:05
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.BINDER)
class DemoBinderActivity: BaseTitleActivity<ActivityBinderDemoBinding, DemoBinderViewModel>() {
    override fun initView() {

    }

    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = "Binder"
}