package com.tthappy.supercoolandroid.java.serializable;

import java.io.Serializable;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/5 12:20
 * Update Date:
 * Modified By:
 * Description:
 */
public class Person implements Serializable {
    private String name = "Jack";
    private int age = 18;
    private static final long serialVersionUID = 233;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
