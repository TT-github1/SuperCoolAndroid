package com.tthappy.supercoolandroid.uidemo.network.retrofit

import com.tthappy.supercoolandroid.network.RetrofitUtils
import com.tthappy.supercoolandroid.uidemo.data.api.UiDemoApi

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/11 17:16
 * Update Date:
 * Modified By:
 * Description:
 */
object UiDemoRetrofit {

    fun create(): UiDemoApi = RetrofitUtils.get("https://wanandroid.com").create(UiDemoApi::class.java) //name : 犬夜叉  password : quanyecha

}