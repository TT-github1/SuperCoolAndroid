package com.tthappy.supercoolandroid.nativedemo.tree;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/1/27 15:10
 * Update Date:
 * Modified By:
 * Description:
 */
public class AVLTree {
    static {
        System.loadLibrary("nativedemo");
    }

    public native void add(int data);
    public native void remove(int data);
    public native void showTree();
}
