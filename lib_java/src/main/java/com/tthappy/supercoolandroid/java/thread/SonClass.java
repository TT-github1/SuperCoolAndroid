package com.tthappy.supercoolandroid.java.thread;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/16 17:35
 * Update Date:
 * Modified By:
 * Description:
 */
public class SonClass extends AbstractClass{

    static {
        System.out.println("SonClass static first");
    }

    @Override
    public void openBook() {
        System.out.println("openBook");
    }

    @Override
    public void read() {
        System.out.println("read");
    }

    @Override
    public void closeBook() {
        System.out.println("closeBook");
    }
}
