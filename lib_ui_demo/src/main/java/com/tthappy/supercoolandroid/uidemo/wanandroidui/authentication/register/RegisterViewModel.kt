package com.tthappy.supercoolandroid.uidemo.wanandroidui.authentication.register

import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.wanandroid.RegisterRequest
import com.tthappy.supercoolandroid.uidemo.network.retrofit.UiDemoRetrofit
import com.uber.autodispose.autoDispose
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Author:      tfhe
 * Create Date: Created in 2022/3/31 11:47
 * Update Date:
 * Modified By:
 * Description:
 */
class RegisterViewModel: BaseViewModel() {
    fun register(userName: String, password: String, rePassword: String) {
        UiDemoRetrofit.create().register(RegisterRequest(userName, password, rePassword))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .bindLoadingCount()
            .autoDispose(this)
            .subscribe({

            }, {

            })
    }
}