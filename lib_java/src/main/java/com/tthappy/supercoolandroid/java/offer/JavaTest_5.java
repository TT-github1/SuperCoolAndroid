package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/23 17:33
 * Update Date:
 * Modified By:
 * Description: 给一个任意数组，请问数组中是否有一个元素出现的次数超过数组长度的一半
 *
 *              解： 给数组先排序，排序后，若数组中有这样一个元素，那么此时中位数上必然是
 *                  该元素，否则没有。
 */
public class JavaTest_5 {
    public static void main(String[] args) {
        int[] arr = {3, 1, 2, 3, 3, 3, 3, 3, 4, 5};
        sort(arr); // 排序可以选快排也可以选归并
        int mid = arr[(arr.length - 1) / 2];
        int count = 0;
        for (int item : arr) {
            if (item == mid) count++;
        }
        System.out.println(count > arr.length / 2);
//        System.out.println(MoreThanHalfNum_Solution(arr));
    }

    private static void sort(int[] arr) {
        if (arr == null || arr.length < 2) return;
        qS(arr, 0, arr.length - 1);
    }

    private static void qS(int[] arr, int start, int end) {
        // 递归头是当分组元素数量为1或空时
        if (end <= start) return; // 只剩一个元素时刚好end = start，没有元素时，end < start

        int left = start;
        int right = end;
        int pivot = arr[start];

        while (right > left) {
            while (right > left && arr[right] >= pivot) {
                right--;
            }
            if (right > left) {
                arr[left] = arr[right];
                left++;
            }
            while (right > left && arr[left] <= pivot) {
                left++;
            }
            if (right > left) {
                arr[right] = arr[left];
                right--;
            }
        }
        arr[left] = pivot;

        qS(arr, start, left - 1);
        qS(arr, left  + 1, end);
    }

    // 最优解 不是自己想的 不太理解
    public static int MoreThanHalfNum_Solution(int[] array) {
        if (array == null || array.length == 0)
            return 0;
        int len = array.length;
        int result = array[0];
        int times = 1;
        for (int i = 1; i < len; i++) {
            if (times == 0) {
                result = array[i];
                times = 1;
                continue;        //continue的意思是结束本次循环，也就是本次循环的下面代码不执行了
            }

            if (array[i] == result)
                times++;
            else
                times--;
        }
        //检查是否符合
        times = 0;
        for (int i = 0; i < len; i++) {
            if (array[i] == result)
                times++;
            if (times > len / 2)
                return result;
        }
        return 0;
    }
}
