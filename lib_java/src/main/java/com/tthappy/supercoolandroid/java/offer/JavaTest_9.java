package com.tthappy.supercoolandroid.java.offer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/27 17:54
 * Update Date:
 * Modified By:
 * Description: 求数组中和为S的两个数字
 */
public class JavaTest_9 {
     public static void main(String[] args) {
         int[] arr = {3, 3, 1, 7, -3, 6, 2};
         System.out.println(Arrays.toString(findTwoNum(arr, 0)));
         System.out.println(Arrays.toString(find(arr, 0)));
    }

    // O(n^2) 
    private static int[] findTwoNum(int[] arr, int target) {
        if(arr.length == 0 || arr.length == 1) return new int[0];
        int right = arr.length;
        for (int i = 0; i < arr.length; i++) {
            int cursor = arr[i];
            int left = i + 1;
            while (left < right) {
                if (cursor + arr[left] == target) return new int[] {cursor, arr[left]};
                else left++;
            }
        }
        return new int[0];
    }

    // O(n)
    private static int[] find(int[] arr, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            if (map.containsKey(target - arr[i])) return new int[] {i, map.get(target - arr[i])};
            map.putIfAbsent(arr[i], i);
        }
        return new int[0];
    }
}
