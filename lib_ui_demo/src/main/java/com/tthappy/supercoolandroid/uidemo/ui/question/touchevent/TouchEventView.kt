package com.tthappy.supercoolandroid.uidemo.ui.question.touchevent

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.tthappy.supercoolandroid.log.HpLog

/**
 * Author:      tfhe
 * Create Date: Created in 2022/8/17 10:40
 * Update Date:
 * Modified By:
 * Description: 多指触控中：
 *                  - 每根手指有一个index，会变，有一个id，不变，可以用这两个属性其中之一，获取另一个属性
 *                  - 一个事件中包含了多根手指的信息，例如可以在ACTION_POINTER_DOWN事件中获取第一根手指的坐标
 *                    也可以在ACTION_MOVE事件中打印每根手指的坐标。
 *                  - 获取手指坐标需要手指index，手指index可以从手指id获取，手指id可以在DOWN事件和POINTER_DOWN事件
 *                    时记录下来
 */
class TouchEventView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private val pointers = intArrayOf(-1, -1, -1, -1)

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                val actionIndex = event.actionIndex
                val pointerId = event.getPointerId(actionIndex)
                pointers[pointerId] = pointerId
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                val actionIndex = event.actionIndex
                val pointerId = event.getPointerId(actionIndex)
                pointers[pointerId] = pointerId

                val x = event.getX(0)
                val y = event.getY(0)
                HpLog.e("这是第${0}根（id）手指， X坐标为 $x, Y坐标为 $y")

                val x1 = event.getX(1)
                val y1 = event.getY(1)
                HpLog.e("这是第${1}根（id）手指， X坐标为 $x1, Y坐标为 $y1")
            }
            MotionEvent.ACTION_MOVE -> {
                for (pointer in pointers) {
                    if (pointer != -1) {
                        val pointerIndex = event.findPointerIndex(pointer)
                        val x = event.getX(pointerIndex)
                        val y = event.getY(pointerIndex)
                        HpLog.e("这是第${pointer}根（id）手指， X坐标为 $x, Y坐标为 $y")
                    }
                }
            }
            else -> {}
        }

        return true
    }
}