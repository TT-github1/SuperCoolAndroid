是一种线程间进行通信的机制。
通过在主线程中使用匿名内部类的方式创建一个Handler的实例，实现handleMessage方法。
然后在子线程中完成某些操作后，将想要发送的数据，用一个Message包装起来，通过这个Handler的实例，调用sendMessage等方法发送出去。
这个Message实际上最终都会调用Handler的enqueueMessage方法。
这个方法又会调用Handler的成员变量的enqueueMessage方法，这个成员变量是一个MessageQueue，到这里Message就加入到了MessageQueue的消息队列中。
另一方面，主线程的Looper对象会不断轮询消息队列，当取到消息时，会将消息分发给对应的Handler。
最终到达主线程的handleMessage方法中执行。



#### 如何为子线程创建Looper？一个线程可以有几个Looper？为什么？
- 调用Looper.prepare()创建Looper，调用Looper.loop()让Looper开始工作
- 一个线程最多只能有一个Looper
- 与Looper的创建方式有关，Looper只有一个私有的构造函数，且Looper类中拥有一个ThreadLocal的静态成员变量，当调用Looper.prepare()时，先从ThreadLocal中取值，如果为空，会调用Looper的私有构造函数创建Looper实例，并把这个实例set进ThreadLocal中，否则会抛异常，因此至多一个


#### Looper会处于一直不停地轮询MessageQueue的状态吗？
- 不会
- 当消息队列为空时，在MessageQueue的next()方法中，局部变量nextPollTimeoutMillis最终会变成-1，然后当成参数被nativePollOnce方法调用，之后会开始处于睡眠状态
- 当消息队列中有消息，但所有的消息的执行时间都比当前时间要大时（即还没到消息要执行的时间），局部变量nextPollTimeoutMillis会变成最近的消息和当前消息的执行时间差，然后被当成参数传进nativePollOnce方法中，睡眠这个时间
- 即Looper的工作状态主要依赖于MessageQueue中的消息队列，队列中没有消息或者有消息但都没有到执行时间，Looper都会休眠（调用nativePollOnce）直到被唤醒（调用nativeWake)


#### 既然一个线程可以有多个Handler实例，那多个Handler实例同时往消息队列放消息怎么保证线程安全？
- 调用MessageQueue的enqueueMessage方法时，方法内部代码块被sychronized关键字锁起来了，保证线程安全
- 除了添加消息的方法加了锁，其他类似移除消息、判断当前队列是否还有消息等方法也都加了锁，且都是锁的同一个对象，即当前消息队列的实例（this）


#### 怎么判断一个Handler属于哪个线程（语义上的属于，而不是指仅有哪个线程能访问该Handler），判断Handler属于哪个线程有什么作用？
- 判断Handler属于哪个线程主要看Handler的创建方式，如果是通过传入Looper的方式创建，Handler属于传入的Looper所在的线程；否则，Handler在哪个线程创建属于哪个线程
- 判断Handler属于哪个线程可以知道通过这个Handler发送的消息最终会在哪个线程得到执行（handleMessage）


#### 同步屏障（拦截同步消息的屏障，把同步消息拦下来，让异步消息先执行）
- 异步消息： Message.setAsynchronous(true);  Android系统中跟UI更新相关的消息即为异步消息，需要优先处理
- 当将消息设置为异步消息并且调用MessageQueue#postSyncBarrier方法开启同步屏障，在MessageQueue的next方法中，当队列头部是同步屏障的Message，会调用一个do-while循环，轮询队列去寻找有没有异步消息，有就先取异步消息，这样就使得异步消息可以得到优先处理
- 同步屏障由系统设置，由系统移除。当系统调用viewRootImpl.requestLayout()后设置同步屏障， 并设置ASYNC信号监听，等待，直到ASYNC信号到来，会发送一个异步消息，让系统去执行绘制任务（onMeasure、onLayout、onDraw），之后移除同步屏障。


#### 消息最终在哪个线程被执行跟Handler实例在哪个线程创建有关吗
- 无关
- 消息的执行是在Looper实例中loop方法里，当轮询得到消息后，调用msg.target.dispatchMessage()方法
- 即应该是Looper实例在哪个线程，消息就在哪个线程执行。因为Looper实例是通过ThreadLocal进行线程隔离的，每个线程只能访问到自己的Looper，但每个线程都可以访问到Handler实例。
- 所以即使Handler的多个实例在一个线程内创建，只要能在实例化时传给他们不同的Looper对象，最终他们就会在不同的线程处理消息


#### Message的sentMessageDelayed是延时发送吗
- 不是
- 这个delay的时间其实只是为了在消息队列入队消息的时候用来排队用的，为了确认消息的执行顺序
- 还有就是当消息判断消息是否到了该执行的时间，没到时间就睡眠，睡眠多长时间也是由这个delay的时间来决定，但如果消息执行很耗时，导致后面的消息到了该执行的时间而没有执行，这个它是不管的
- 在调用sentMessageDelayed方法的时候，消息就已经入队了


#### 当前一个消息执行耗时比较长，导致后一个消息到了理论上应该执行的时间了怎么办
- 凉拌
- 消息仍会按先后顺序依次执行，只是没法按指定时间进行执行
- Looper的loop方法只管调用MessageQueue的next方法取Message，然后执行，MessageQueue的next方法只管判断队头的消息是否还没到该执行的时间，没到时间就睡眠，然后卡在这，直到到时间。但是假如是不仅到时间了，还时间超了，没关系，只管按照队列顺序返回头节点，然后处理


#### Handler的应用
- HandlerThread： 
	- 本质是一个线程
	- 在run方法中创建Looper实例，并且调用实例的loop方法，即将这个Looper绑定到了这个线程上
	- 提供了获取这个线程对应的Looper实例的方法getLooper()，并且这个方法和创建Looper实例的方法都加上了synchronized关键字，并使用wait/notifyAll方法，确保获取的Looper实例不为空
	- 现在你可以用HandlerThread的Looper实例来在你需要的位置构造一个Handler的实例，然后使用这个Handler发送的消息就都可以跑在handlerThread这个线程中
	- 这个类主要是为了解决主线程向非主线程通信的问题。一般情况下，主线程不方便拿到子线程的Looper，拿不到子线程的Looper也就没办法向子线程发消息。另一方面，拿子线程的Looper也存在安全问题，因为无法确保拿子线程Looper的时候，子线程的Looper已经初始化完毕了
	- 使用HandlerThread创建的线程是一个死循环，所以可以一直在执行。但是当没有消息或消息没到执行时间时，线程会休眠，所以又不会占用CPU资源消耗CPU性能。当有消息来时，能自动唤醒，并处理消息，有一种很强的协作感，nice。


- IntentService： 
	- 本质是一个Service（抽象类）
	- 使用简单，只需要搞一个子类继承这个类，然后实现onHandleIntent方法，在这个方法里面执行耗时任务，然后startService这个service就可以了
	- 这个类在onCreate的时候，就创建了一个HandlerThread并start，开启了一个异步线程，并且实例化了一个使用这个异步线程的Looper来构建的ServiceHandler，这个Hanlder是这个类的内部类，handlerMessage方法中就执行一个onHandleIntent方法，然后就stopSelf，干掉自己。最后在onDestroy方法中调用异步线程的Looper的quit方法，停掉Looper。
	- 简单来说，这是一个一眼看上去比较让人省心的类，有高度的自我管理意识，并且使用简单，使用目的明确


#### 网上经常说的安卓线程通信的四种方式
- runOnUiThread()、View.post()、Handler、AsyncTask
- 前三种其实都是Handler（搞懂Handler还是要紧的啊）


#### Handler中涉及到的知识点应用
- ThreadLocal： 在Looper的prepare方法中使用ThreadLoacl存储自身实例，使得每个线程仅有一个Looper
- wait/notifyAll : 在HandlerThread中，



#### 介绍一下Handler机制
1.是什么
是安卓中的线程通信机制，通常用于在子线程中去通知主线程做对应的操作，例如更新UI
2.从哪里开始
Handler机制由一系列类相互协作共同构成。
在ActivityThread类的main方法中，会去调用Looper类的prepareMainLooper方法，里面又会调用Looper的prepare方法，
去执行Looper的私有构造方法，创建Looper的实例，同时将这个实例设置给Looper的一个ThreadLocal静态变量中。
之后会调用Looper的loop方法，开启死循环。
3.有哪些对象
当我们使用Handler不断发送消息时，其实都是在往消息队列中存消息，最终都会走到MessageQueue的enqueueMessage方法，
消息入队时，根据消息预计生效的时间来进行排序。
4.整个运转流程
这边我们不断的用Handler发送消息，然后我们之前提到的loop死循环中则不断的从MessageQueue中取出消息，然后调用消息的
target属性即Handler的dispatchMessage方法，就回到了我们自定义的Handler处理消息的方法中，完成线程通信。
5.整体概况
Handler机制整体上来说是一种生产者消费者模式，我们在子线程中不断生产消息，然后在loop循环中不断消费消息，如果消息队列为空
或者消息队列里的消息都还没到执行时间，就会在取消息的next方法中休眠，直到有消息入队或者有消息到了执行时间，才会去唤醒next
方法进行执行。

#### 说说Handler中的同步屏障
1.是什么
是安卓用来保证优先执行异步消息的一种机制
2.从哪里触发
在View更新时会触发，例如requestLayout、invalidate等方法，最终会走到ViewRootImpl的scheduleTraversals方法，
在这里会调用MessageQueue的postSyncBarrier方法，设置同步屏障，即往消息队列中插入一个target为空的Message，
当同步屏障处于队列头部时，排在这个同步屏障消息后面的异步消息会在同步屏障存在期间被优先取出来执行，
这样可以保证和UI显示相关的消息能得到尽快的处理。

#### 说说Handler中的消息缓存池机制
获取消息对象用obtain方法，得到缓存池中的对象，没有则new一个
回收消息对象用recycle方法，会调用到recycleUnchecked方法，将消息各个属性清空（inUsed标记除外），然后头插法加入缓冲池
缓冲池有大小限制，最大50

#### 说说Handler中的epoll //todo
poll 是一直忙轮询 不断在轮询每个IO是否有数据
select 是休眠和唤醒 没数据时休眠 有数据唤醒 然后轮询
epoll 也是休眠和唤醒 没数据时休眠 有数据唤醒 然后直接取那些有数据的IO

#### 为什么View的post方法能让Runnable顺利执行
View的post方法是调用了View的mAttachInfo的mHandler的post方法，即Handler的post方法，把Runnable包装成一个Message，
并赋值给Message的callback属性，当事件分发给Handler处理的时候，会先判断消息的callback是否为空，不为空则直接调用callback
的run方法，不走Handler的handleMessage了
至于为什么Message要有callback这条路，emm... todo吧
然后，另外的，假如View调用post方法的时候，mAttachInfo为空，即View没有附加到屏幕上，此时会将Runnable通过HandlerActionQueue
的post方法存起来，封装在一个HandlerAction对象中，等View附加到屏幕后，再去调用executeActions方法，用传进来的handler去post
这个Runnable，例如在View的dispatchAttachedToWindow方法中和ViewRootImpl的performTraversals方法中

#### 为什么loop是死循环却不会卡死界面
