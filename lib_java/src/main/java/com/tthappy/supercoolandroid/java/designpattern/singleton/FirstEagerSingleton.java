package com.tthappy.supercoolandroid.java.designpattern.singleton;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/3 19:37
 * Update Date:
 * Modified By:
 * Description: 饿汉式
 */
public class FirstEagerSingleton {
    //构造器私有化（防止new）
    private FirstEagerSingleton() {}

    //类的内部创建对象
    //这里还可以使用静态代码块的方式来实例化instance变量
    private static final FirstEagerSingleton instance = new FirstEagerSingleton();

    //向外暴露一个公共静态方法
    public static FirstEagerSingleton getInstance() {
        return instance;
    }
}
