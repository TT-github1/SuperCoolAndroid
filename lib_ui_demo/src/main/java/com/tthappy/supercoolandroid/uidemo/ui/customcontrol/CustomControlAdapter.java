package com.tthappy.supercoolandroid.uidemo.ui.customcontrol;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2020/10/12 13:32
 * Update Date:
 * Modified By:
 * Description:
 */
public class CustomControlAdapter extends RecyclerView.Adapter<CustomControlAdapter.ViewHolder> {

    private ArrayList<CustomControlEntity> list = new ArrayList<>();

    public CustomControlAdapter() {
        if (list.isEmpty()) {
//            for (int i = 0; i < 20; i++) {
                list.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "gogo", BridgeConstansUIDemo.CC_GUGD));
                list.add(new CustomControlEntity(R.drawable.bg_tab_fourth_unselected, "xxx", BridgeConstansUIDemo.CC_PANEL));
                list.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "ggg", BridgeConstansUIDemo.CC_CHART));
//                list.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "gogo", BridgeConstants.SCROLLVIEW_AND_VIEWPAGER));
//                list.add(new CustomControlEntity(R.drawable.bg_tab_fourth_unselected, "xxx", BridgeConstants.EQUIPMENTATTRIBUTE));
//                list.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "ggg", BridgeConstants.JETPACK_SCORE));
//                list.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "gogo", BridgeConstants.UI_TEST));
//            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom_control_demo, parent, false);
            return new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom_control_two, parent, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        final CustomControlEntity item = list.get(position);
        holder.mIcon.setImageResource(item.getmIcon());
        holder.mNameTextView.setText(item.getmName());
        holder.mItem.setOnClickListener(v -> ARouter.getInstance().build(item.getmUrl()).navigation());
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position);
        } else {
            Bundle payload = (Bundle) payloads.get(0);
            for (String s : payload.keySet()) {
                switch (s) {
                    case "icon" :
                        holder.mIcon.setImageResource(payload.getInt("icon"));
                        break;
                    case "name" :
                        holder.mNameTextView.setText(payload.getString("name"));
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void removeItem() {
        list.remove(2);
        list.add(2, new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "gogo", BridgeConstansUIDemo.UITEST));
//        notifyItemRemoved(3);
//        notifyItemInserted(3);
        notifyItemChanged(2);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void diffChangeListData() {
        ArrayList<CustomControlEntity> newList = new ArrayList<>();
//        for (int i = 0; i < 20; i++) {
            newList.add(new CustomControlEntity(R.drawable.bg_hills, "gogo", BridgeConstansUIDemo.CC_GUGD));
            newList.add(new CustomControlEntity(R.drawable.bg_tab_fourth_unselected, "玛玛哈哈", BridgeConstansUIDemo.CC_PANEL));
            newList.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "ggg", BridgeConstansUIDemo.CC_CHART));
//            newList.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "gogo", BridgeConstants.SCROLLVIEW_AND_VIEWPAGER));
//            newList.add(new CustomControlEntity(R.drawable.bg_tab_fourth_unselected, "xxx", BridgeConstants.EQUIPMENTATTRIBUTE));
//            newList.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "ggg", BridgeConstants.JETPACK_SCORE));
//            newList.add(new CustomControlEntity(R.drawable.bg_tab_fourth_selected, "gogo", BridgeConstants.UI_TEST));
//        }

        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new CustomDiffUtilCallback(list, newList), false);
        list = newList;
        diffResult.dispatchUpdatesTo(this);
    }

    public void normalChangeListData() {
        ArrayList<CustomControlEntity> newList = new ArrayList<>();
//        for (int i = 0; i < 70; i++) {
//            newList.add(new CustomControlEntity(R.drawable.bg_hills, "常山赵子龙", BridgeConstants.CC_GUGD));
//            newList.add(new CustomControlEntity(R.drawable.bg_home, "李志超他奶奶", BridgeConstants.CC_PANEL));
//        }
        newList.add(new CustomControlEntity(R.drawable.bg_hills, "常山赵子龙", BridgeConstansUIDemo.CC_GUGD));
        newList.add(new CustomControlEntity(R.drawable.bg_home, "梁志超他奶奶", BridgeConstansUIDemo.CC_PANEL));
        newList.add(new CustomControlEntity(R.drawable.bg_home, "梁志超他奶奶", BridgeConstansUIDemo.CC_PANEL));
        newList.add(new CustomControlEntity(R.drawable.bg_home, "梁志超他奶奶", BridgeConstansUIDemo.CC_PANEL));
        list = newList;
        notifyDataSetChanged();
    }


    
    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mIcon;
        TextView mNameTextView;
        ConstraintLayout mItem;

        public ViewHolder(@NonNull View view) {
            super(view);
            mIcon = view.findViewById(R.id.iv_icon);
            mNameTextView = view.findViewById(R.id.tv_name);
            mItem = view.findViewById(R.id.cl_hole_item);
        }
    }

    @Override
    public int getItemViewType(int position) {
        System.out.println("getItemViewType==" + position % 2);
        return position % 2;
    }



    public static class CustomDiffUtilCallback extends DiffUtil.Callback {

        private List<CustomControlEntity> oldList;
        private List<CustomControlEntity> newList;

        public CustomDiffUtilCallback(@NonNull List<CustomControlEntity> oldList, @NonNull List<CustomControlEntity> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return TextUtils.equals(oldList.get(oldItemPosition).getmName(), newList.get(newItemPosition).getmName());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            CustomControlEntity oldEntity = oldList.get(oldItemPosition);
            CustomControlEntity newEntity = newList.get(newItemPosition);
            return oldEntity.getmIcon() == newEntity.getmIcon();
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            CustomControlEntity oldEntity = oldList.get(oldItemPosition);
            CustomControlEntity newEntity = newList.get(newItemPosition);
            Bundle payload = new Bundle();
            if (oldEntity.getmIcon() != newEntity.getmIcon()) {
                payload.putInt("icon", newEntity.getmIcon());
            }
            if (TextUtils.equals(oldEntity.getmName(),newEntity.getmName())) {
                payload.putString("name", newEntity.getmName());
            }
            if (TextUtils.equals(oldEntity.getmUrl(),newEntity.getmUrl())) {
                payload.putString("url", newEntity.getmUrl());
            }
            if (payload.size() == 0) return null;
            return payload;
        }
    }
}
