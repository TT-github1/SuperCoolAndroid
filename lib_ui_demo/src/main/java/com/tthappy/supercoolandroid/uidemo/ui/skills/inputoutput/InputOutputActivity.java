package com.tthappy.supercoolandroid.uidemo.ui.skills.inputoutput;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

@Route(path = BridgeConstansUIDemo.INTPUTOUTPUT)
public class InputOutputActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_output_demo);
//        DisplayUtils.setTransparentStatusBar(this);

        EditText editText = findViewById(R.id.et_content);
        TextView textDesc = findViewById(R.id.tv_desc);
        TextView textView = findViewById(R.id.tv_save);

//        textView.setOnClickListener(v -> {
//            writeStringForStream(editText.getText().toString());
//            Toast.makeText(this, "保存成功！", Toast.LENGTH_SHORT).show();
//            textDesc.setText("请到/data/data/com.tthappy.supercoolandroid/files/目录下查看");
//            textView.setText(readStringForStream());
//        });

        textView.setOnClickListener(v -> {
            writeStringForBuffer(editText.getText().toString());
            Toast.makeText(this, "保存成功！", Toast.LENGTH_SHORT).show();
            textDesc.setText("请到" + getFilesDir() +"目录下查看");
            textView.setText(readStringForBuffer());
        });
    }

    private void writeStringForStream(String content) {
        try {
            FileOutputStream outputStream = openFileOutput("SuperCool.txt", MODE_PRIVATE);
            byte[] bytes = content.getBytes();
            outputStream.write(bytes);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readStringForStream() {
        String result = "";
        try {
            FileInputStream inputStream = openFileInput("SuperCool.txt");
            int length = inputStream.available();
            byte[] bytes = new byte[length];
            inputStream.read(bytes);
            result = new String(bytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void writeStringForBuffer(String content) {
        try {
            FileOutputStream outputStream = openFileOutput("SuperCool.txt", MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
            BufferedWriter writer = new BufferedWriter(outputStreamWriter);
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readStringForBuffer() {
        String result = "";
        try {
            FileInputStream inputStream = openFileInput("SuperCool.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(inputStreamReader);
            result = reader.readLine();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}