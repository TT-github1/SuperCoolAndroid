package com.tthappy.supercoolandroid.java.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/14 10:02
 * Update Date:
 * Modified By:
 * Description:
 */
public class ServerSocket {
    public static void main(String[] args) {
        try {
            ServerSocketChannel serverSocket = ServerSocketChannel.open();
            serverSocket.configureBlocking(false);
            serverSocket.bind(new InetSocketAddress("169.254.17.80", 8888));
            Selector selector = Selector.open();
            serverSocket.register(selector, SelectionKey.OP_ACCEPT);
            Calendar ca = Calendar.getInstance();
            System.out.println("Sever started...");
            System.out.println("================================");
            while (true) {
                selector.select();
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    if (key.isAcceptable()) {
                        SocketChannel socket = serverSocket.accept();
                        socket.configureBlocking(false);
                        socket.register(selector, SelectionKey.OP_READ);
                        String msg = "Connect is success, you are the " + (selector.keys().size() - 1) + " user...";
                        socket.write(ByteBuffer.wrap(msg.getBytes()));
                        InetSocketAddress address = (InetSocketAddress) socket.getRemoteAddress();
                        System.out.println(ca.getTime() + "\t" + address.getHostString() +
                                ":" + address.getPort() + "\t");
                        System.out.println("Client is connecting...");
                        System.out.println("=========================================================");
                    }

                    if (key.isReadable()) {
                        SocketChannel socket = (SocketChannel) key.channel();
                        InetSocketAddress address = (InetSocketAddress) socket.getRemoteAddress();
                        System.out.println(ca.getTime() + "\t" + address.getHostString() + ":" + address.getPort() + "\t");
                        ByteBuffer buffer = ByteBuffer.allocate(1024 * 4);
                        int len;
                        byte[] res = new byte[1024 * 4];
                        try {
                            while ((len = socket.read(buffer)) != 0) {
                                buffer.flip();
                                buffer.get(res, 0, len);
                                System.out.println(new String(res, 0, len));
                                buffer.clear();
                            }
                            System.out.println("============================================");
                        } catch (IOException e) {
                            key.cancel();
                            socket.close();
                            System.out.println("Client is disconnected...");
                            System.out.println("=================================================");
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Server error, Server is closing...");
            System.out.println("================================================");
        }

    }
}
