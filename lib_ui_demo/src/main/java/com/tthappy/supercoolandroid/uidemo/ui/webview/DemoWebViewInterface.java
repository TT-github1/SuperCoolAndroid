package com.tthappy.supercoolandroid.uidemo.ui.webview;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/9 17:51
 * Update Date:
 * Modified By:
 * Description:
 */

public class DemoWebViewInterface {

    private Context mContext;

    public DemoWebViewInterface(Context mContext) {
        this.mContext = mContext;
    }

    @JavascriptInterface
    public void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
}
