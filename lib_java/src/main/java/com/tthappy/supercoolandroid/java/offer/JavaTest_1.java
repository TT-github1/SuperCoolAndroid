package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/23 9:13
 * Update Date:
 * Modified By:
 * Description: 给定一个二维数组，每行递增，每列递增，给一个数，问在不在二维数组中
 *
 *              解： 取右上（也可以左下）值，和目标比较。(取值重点在与找到横向和竖向方向向量相交的端点）
 *              若目标小于右上值，则说明最右一列均大于目标值，可删去；
 *              若目标大于右上值，则说明最上一行均小于目标值，可删去
 *              每比较一次，可删去一行或一列，时间复杂度为 O(n)
 */
public class JavaTest_1 {

    public static void main(String[] args) {
        int[][] arr = {
                {1,  2,  3,  5,  6},
                {4,  8,  10, 11, 13},
                {7,  20, 25, 33, 39},
                {22, 44, 54, 66, 89},
                {24, 46, 56, 68, 96},
        };

        System.out.println(mainSearch(arr, 1));
        System.out.println(mainSearch(arr, 1));
        System.out.println(mainSearch2(arr, 1));
        System.out.println(mainSearch2(arr, 1));
    }

    private static boolean mainSearch(int[][] arr, int target) {
        if (arr == null || arr.length == 0) return false;
        return search(arr, 0, arr.length - 1, 0, arr[0].length - 1, target);
    }

    private static boolean mainSearch2(int[][] arr, int target) {
        if (arr == null || arr.length == 0) return false;
        return searchNoRecursion(arr, arr.length - 1, arr[0].length - 1, target);
    }

    // 递归解法
    private static boolean search(int[][] arr, int vStart, int vEnd, int hStart, int hEnd, int target) {
        int topRight = arr[vStart][hEnd];
        if (target == topRight) return true;
        if (target < topRight) {
            if (hEnd - hStart == 0) return false;
            return search(arr, vStart, vEnd, hStart, hEnd - 1, target);
        }
        else {
            if (vEnd - vStart == 0) return false;
            return search(arr, vStart + 1, vEnd, hStart, hEnd, target);
        }
    }

    // 非递归解法
    private static boolean searchNoRecursion(int[][] arr, int vEnd, int hEnd, int target) {
        int h = hEnd;
        int v = 0;

        while (h >= 0 && v <= vEnd) {
            int topRight = arr[h][v];
            if (target == topRight) return true;
            if (target < topRight) {
                if (h == 0) return false;
                h--;
            } else {
                if (vEnd - v == 0) return false;
                v++;
            }
        }

        return false;
    }
}
