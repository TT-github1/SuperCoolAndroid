package com.tthappy.supercoolandroid.uihome.debug.plugins

import com.alibaba.android.arouter.launcher.ARouter
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.uihome.debug.DebugPlugin
import com.tthappy.supercoolandroid.uihome.debug.activity.DebugActivity

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/9 16:49
 * Update Date:
 * Modified By:
 * Description:
 */
class WebViewTestPlugin(mActivity: DebugActivity): DebugPlugin(mActivity) {
    override fun icon(): Nothing? = null

    override fun name() = "WebView测试"

    override fun value() = "这是一场试炼"

    override fun doWhat() {
        ARouter.getInstance().build(BridgeConstansUIDemo.WEB).withString("url", "file:///android_asset/demo.html").navigation()
    }
}