package com.tthappy.supercoolandroid.uidemo.ui.skills.permission;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity;
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityRequestPermissionDemoBinding;

import java.util.Objects;

@Route(path = BridgeConstansUIDemo.REQUESTPERMISSION)
public class RequestPermissionActivity extends BaseActivity<ActivityRequestPermissionDemoBinding, BaseViewModel> {

    /**
     * 检查打电话权限是否已经开通，
     * 若没开通则弹出系统弹框动态申请，
     * 否则直接打电话（改权限要之前就在Manifest里声明，
     * 否则这里连框都不弹，
     * 直接走onRequestPermissionsResult回调方法的else方法体
     */
    @Override
    public void initView() {
        viewBinding.text.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_CONTACTS}, 1);
            } else {
                call();
            }
        });
    }

    //系统弹框的回调方法(主要是什么的ActivityCompat.requestPermissions方法中内部调用了这个回调方法）
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //这个requestCode就是上面请求动态权限我们传进来的第三个参数
        if (1 == requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                call();
            } else {
//                ToastUtils.showShort("拒绝开权限你他喵的还想打电话？");
            }
        }
    }

    //打电话
    private void call() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:10086"));
        startActivity(intent);

        Cursor cursor = Objects.requireNonNull(getMActivity()).getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            Log.e("hhh", "name = " + name);

            String phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.e("hhh", "phone = " + phone);
        }
        cursor.close();
    }

    @Override
    public void initData() {

    }

    @Override
    public void observeData() {

    }
}