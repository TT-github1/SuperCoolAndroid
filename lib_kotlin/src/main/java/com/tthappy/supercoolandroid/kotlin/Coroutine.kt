package com.tthappy.supercoolandroid.kotlin

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import java.util.*


object Coroutine {
    @JvmStatic
    fun main(args: Array<String>) {
        test()
    }
    val scope = CoroutineScope(Dispatchers.IO)
    val channel = Channel<Int>(3)

    private fun test() = runBlocking {
        val product = scope.launch {
            var count = 0
            while (true) {
                delay(1000)
                channel.send(++count)
                println("channel send $count")
            }
        }

        val consumer = scope.launch {
//            while (true) {
                val element = channel.map<Int, String> { index ->
                    "Object()"
                }
                println("channel receive $element")


//            }
        }

        joinAll(product, consumer)
    }

    private suspend fun <T, K> Channel<T>.map(func: (T) -> K) {
        val t = receive()
        func.invoke(t)
    }
}