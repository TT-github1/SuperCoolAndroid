package com.tthappy.supercoolandroid.uihome.debug.plugins

import android.annotation.SuppressLint
import com.tthappy.supercoolandroid.java.classloader.ClassLoaderUtil
import com.tthappy.supercoolandroid.uihome.debug.DebugPlugin
import com.tthappy.supercoolandroid.uihome.debug.activity.DebugActivity
import com.tthappy.supercoolandroid.utils.toast.ToastUtils

class HotFixPlugin(mActivity: DebugActivity): DebugPlugin(mActivity) {
    override fun icon(): Nothing? = null

    override fun name() = "简易热修复"

    @SuppressLint("SdCardPath")
    override fun value() = "点击查看\n补丁包的目录目前写死在\"/sdcard/patch.jar\""

    override fun doWhat() {
        try {
            if (ClassLoaderUtil.getInstance().init()) {
                ToastUtils.showShort(value())
            }
        } catch (e: Exception) {
            ToastUtils.showShort(e.cause.toString())
        }
    }
}