//
// Created by ad on 2022/1/26.
//

#include "../common_dependencies.h"
#include "../tree/linked_storage_binary_search_tree.h"

static LSBSTree * lsbsTree = NULL;

void visitTree(int data) {
    LOG("**  This node is %d", data);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_tthappy_nativedemo_tree_LinkedStorageBinarySearchTree_add(
    JNIEnv *env,
    jobject thiz,
    jint data
) {
    linkedStorageBinarySearchTree_add(&lsbsTree, data);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_tthappy_nativedemo_tree_LinkedStorageBinarySearchTree_showTree(
    JNIEnv *env,
    jobject thiz
) {
    linkedStorageBinarySearchTree_preVisitTree(lsbsTree, visitTree);
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_tthappy_nativedemo_tree_LinkedStorageBinarySearchTree_contain(
    JNIEnv *env,
    jobject thiz,
    jint data
) {
    return linkedStorageBinarySearchTree_contain(lsbsTree, data);
}