#### 是什么
订阅了LiveData的组件，LiveData数据发生改变时，会通知订阅组件，自动更新数据
并且LiveData拥有对Activity的生命周期感知能力，仅在Activity处于可见状态时，才会通知数据发生改变，并且当Activity生命周期结束时，会自动移除订阅，以及解绑生命周期

#### 原理是什么
使用了两层观察者模式
订阅LiveData时，会将observer和lifecycleOwner一起传入一个LifecycleBoundObserver类，然后以observer为键，LifecycleBoundObserver为值，保存到一个Map里
当LiveData的值发生改变，会去轮询Map，挨个调用observer的onChange方法，通知更新，此时LiveData是被观察者的角色
另外，在保存LifecycleBoundObserver时，还会调用owner.getLifecycle().addObserver(wrapper)，把它传进去，当Activity生命周期发生改变，会去判断是否是destroy，
如果是，就会去把两层的观察都移除掉，如果不是，则去判断是否需要修改当前mActive的状态，LiveData通过mActive的状态，来去决定消息通知是否会生效。
第一层观察其实是比较简单的观察者，第二层是结合了Lifecycle的观察者，走的是那一套流程，同时第二层的观察结果会影响第一层观察是否会派发






