package com.tthappy.supercoolandroid.network

import com.tthappy.supercoolandroid.network.interceptor.LogInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/9 19:58
 * Update Date:
 * Modified By:
 * Description:
 */
class RetrofitCreator(networkStrategy: NetworkStrategy) {

    val retrofit: Retrofit

    init {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.retryOnConnectionFailure(networkStrategy.retryOnFailure)
                .connectTimeout(networkStrategy.connectTimeOutSeconds, TimeUnit.SECONDS)
                .readTimeout(networkStrategy.connectTimeOutSeconds, TimeUnit.SECONDS)
                .writeTimeout(networkStrategy.connectTimeOutSeconds, TimeUnit.SECONDS)
                .addInterceptor(LogInterceptor())

        if(!networkStrategy.interceptors.isNullOrEmpty()) {
            networkStrategy.interceptors.forEach{
                clientBuilder.addInterceptor(it)
            }
        }

        retrofit = Retrofit.Builder()
                .baseUrl(networkStrategy.baseUrl)
                .client(clientBuilder.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

}