package com.tthappy.supercoolandroid.utils.reflect;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/14 15:56
 * Update Date:
 * Modified By:
 * Description:
 */
public class ReflectUtils {

    public static <T extends AccessibleObject> T publicObject(T object) {
        object.setAccessible(true);
        return object;
    }

    public static <T extends AccessibleObject> Object showAllMembers(Object object) {

        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
        }

        return object;
    }

}
