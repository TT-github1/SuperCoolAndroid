package com.tthappy.supercoolandroid.java.datastruct.tree;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/17 11:33
 * Update Date:
 * Modified By:
 * Description:
 */
public class BinarySearchTree<T extends Comparable<T>> {

    Node head;

    public void add(T data) {
        head = add(head, data);
    }

    // 递归方法实现
    private Node add(Node tree, T data) {
        if (tree == null) return new Node(data);

        int cmp = data.compareTo(tree.data);
        if (cmp < 0) tree.lChild = add(tree.lChild, data);
        else if (cmp > 0) tree.rChild = add(tree.rChild, data);

        return tree;
    }

    public void insert(T data) {
        insert(head, data);
    }

    // 非递归方法实现
    private void insert(Node tree, T data) {
        Node cursor = tree; // 循环指针
        Node target = null; // 目标叶子节点
        int cmp = 0;

        // 循环直到叶子节点
        while (cursor != null) {
            cmp = data.compareTo(cursor.data);
            if (cmp == 0) {
                System.out.println("不可以插入已经存在的数据哦");
                return;
            }

            target = cursor;

            if (cmp < 0) {
                cursor = cursor.lChild;
            } else {
                cursor = cursor.rChild;
            }
        }

        if (cmp < 0) target.lChild = new Node(data);
        else if (cmp > 0) target.rChild = new Node(data);
        else head = new Node(data); // 根节点为空的情况
    }

    public Node getHead() {
        return head;
    }

    class Node implements PrintTree.PrintNode<T> {
        T data;
        Node lChild, rChild;

        Node(T data) {
            this.data = data;
        }

        @Override
        public T getData() {
            return data;
        }

        @Override
        public PrintTree.PrintNode<T> getLChild() {
            return lChild;
        }

        @Override
        public PrintTree.PrintNode<T> getRChild() {
            return rChild;
        }
    }
}
