package com.tthappy.supercoolandroid.uidemo.ui.webview;

import android.text.TextUtils;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.log.HpLog;
import com.tthappy.supercoolandroid.mvvm.view.BaseFragment;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.databinding.FragmentWebViewDemoBinding;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/9 10:17
 * Update Date:
 * Modified By:
 * Description: 后续可结合loadsir lottie
 */
@Route(path = BridgeConstansUIDemo.WEBVIEWFRAGMENT)
public class DemoWebViewFragment extends BaseFragment<FragmentWebViewDemoBinding, DemoWebViewViewModel> {

    @Autowired
    String url = "";

    private FragmentWebViewDemoBinding viewBinding;

    @Override
    public void initView() {
        viewBinding = getViewBinding();
        if (viewBinding == null) return;

        if (url == null || TextUtils.isEmpty(url)) {
            url = getArguments() != null ? getArguments().getString("url") : "";
        }
        String finalUrl = (url == null || TextUtils.isEmpty(url)) ? "http://www.wengetech.com/" : url;
        HpLog.INSTANCE.e(("Current url : " + finalUrl)::toString);
        viewBinding.webView.loadUrl(finalUrl);
    }

    public void reload() {
        viewBinding.webView.reload();
    }

    public boolean canGoBack() {
        if (viewBinding.webView.canGoBack()) {
            viewBinding.webView.goBack();
            return true;
        }
        return false;
    }

    @Override
    public void initData() { }
    @Override
    public void observeData() { }
}
