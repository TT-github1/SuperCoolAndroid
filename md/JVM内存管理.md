#### JVM的运行过程
- 将.java文件通过javac命令编译成.class字节码文件
- JVM想要使用.class字节码文件，首先需要使用类加载器ClassLoader，把.class字节码文件加载到JVM的一个运行时数据区的地方
（所以ClassLoader的作用是以文件IO的形式读取.class文件，并解析里面的类信息，然后保存到内存中？）
- 但想要正在执行程序，还需要将通过执行引擎将.class字节码转换为计算机可识别的二进制机器码，有两种方式
  - 解释执行。对.class字节码逐行进行解释并执行
  - JIT技术。对热点代码（频繁使用的代码，包括方法和循环代码）进行整体编译成机器码，并保存在本地，可直接执行

因此JVM里主要包含三个部分
- Java类加载器ClassLoader
- 运行时数据区
- 执行引擎

#### 运行时数据区
Java虚拟机在执行Java程序的过程中会把它所管理的内存划分为若干个不同的数据区域
- 虚拟机栈（线程私有）
  存储当前线程运行方法所需的数据、指令、返回地址
  基本组成单元是栈帧，一个栈帧中包含
  - 局部变量表
    保存方法中的局部变量
    局部变量表中的变量是重要的垃圾回收节点，只要被局部变量表中直接或间接引用的变量都不会被回收
    局部变量表的大小在编译时就确定
  - 操作数栈
    Java的解释执行是基于栈（操作数栈）的
    操作数栈的大小在编译时就确定
  - 动态链接
    多态：静态分派和动态分派
    当一个方法要调用其他方法时，需要将这些方法的符号引用转化为其在内存地址中的直接引用。由于多态的存在，在编译期间，可能存在多个相同的符号引用，
    但它们表示不同的逻辑行为（例如Cat#voice（）和Dog#voice（）），所以就需要在运行时才将符号引用转换为直接引用。而有些方法可以在编译时就直接
    确定直接引用，这也就对应上面的动态分派和静态分派。
  - 完成出口（返回地址）
    方法完成之后，随着栈帧出栈，程序需要知道接下来应该执行到哪里，所以栈帧中会有这样一个地方保存着返回的地址
    返回地址分为正常返回和异常返回。正常返回可以视为用程序计数器中的引用作为返回地址。异常返回是通过异常处理器表确定的，栈帧中一般不会保存此部分信息。
  虚拟机栈在HotSpot版本的大小是受到限制的，通常是1M
- 本地方法栈（线程私有）
  在HotSpot版本Java虚拟机中，本地方法栈和虚拟机栈合二为一
  
- 程序计数器（线程私有）
  指向当前线程正在执行的字节码指令的地址（注意是字节码指令的地址，不是某行代码的地址）
  作用是在时间片轮转机制中，记录代码执行的位置，编译重新获取时间片后，能正确地恢复运行
  是JVM内存区域中唯一不会OOM的内存区域（这片内存区域很小，仅需要记录一个地址信息）
- 方法区 （JDK版本在1.8之前也叫永久代，之后叫元空间，这两个都是方法区的实现方式，而用方法区，无论在哪个版本都是可以指代这块内存区域）
  永久代时还是会和堆有一定的关联，发现垃圾回收的效率会受到影响，所以到元空间之后，就单独隔离出来，放在机器内存而不是堆内存，这样的好处是元空间便于扩展，
  不会OOM，但是坏处是会挤压堆内存，例如内存总共20G，元空间就占用了15G，然后堆内存本来设置最大可以10G，但是现在堆内存最大只能使用5G了
  - 类信息
  - 常量
  - 静态变量
  - 即时编译器（JIT）编译后的代码
- 堆