package com.tthappy.supercoolandroid.uidemo.ui.okhttp

import com.tthappy.supercoolandroid.log.HpLog
import com.tthappy.supercoolandroid.mvvm.event.SingleLiveData
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.network.NetworkStrategy
import com.tthappy.supercoolandroid.network.RetrofitCreator
import com.tthappy.supercoolandroid.uidemo.data.api.UiDemoApi
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal.WAZOfficialAccountsEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class OkhttpViewModel: BaseViewModel() {

    val result = SingleLiveData<List<WAZOfficialAccountsEntity>>()
    fun sbtest() {
        val d = RetrofitCreator(NetworkStrategy("https://wanandroid.com")).retrofit.create(UiDemoApi::class.java).sbtest()
                .concatMap { ResultHandler.handleCommonResult(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            result.postValue(it)
                        },
                        {
                            HpLog.e("emmm...."::toString)
                        }
                )
    }
}