package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/28 9:43
 * Update Date:
 * Modified By:
 * Description: 一个长度为 n-1 的递增排序数组中的所以数字都是唯一的，并且每个数字都在0~n-1的范围之内。
 *              在范围0~n-1的范围之内的n个数字中有且只有一个不在该数组中，请找出这个数字
 */
public class JavaTest_11 {
    public static void main(String[] args) {
        int[] arr = {0, 1, 2, 3, 4, 6, 7, 8};
//        int[] arr = {1, 2, 3, 4};
        System.out.println(findNum(arr));
    }

    private static int findNum(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != i) return i;
        }
        return -1;
    }
}
