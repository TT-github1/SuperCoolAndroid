package com.tthappy.supercoolandroid.uidemo.ui.skills.handler

import android.app.IntentService
import android.content.Intent
import android.os.*
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.log.HpLog
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityDispatchDemoBinding
import java.lang.ref.WeakReference

/**
 * Author:      tfhe
 * Create Date: Created in 2021/8/16 14:08
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.DISPATCH)
class DemoHandlerActivity : BaseTitleActivity<ActivityDispatchDemoBinding, BaseViewModel>() {

    private var isHandlersInit = false
    private lateinit var uiHandler: UiHandler
    private lateinit var handlerThread: HandlerThread
    private lateinit var childHandler: Handler

    private var index = 0

    override fun initView() {
        uiHandler = UiHandler(Looper.myLooper()!!, viewBinding)

        viewBinding.tvHandlerThread.setOnClickListener{ handlerThreadTest() }
        viewBinding.tvIntentService.setOnClickListener{ intentServiceTest() }
        viewBinding.tvAsyncTask.setOnClickListener{ asyncTaskTest() }
    }

    override fun onDestroy() {
        super.onDestroy()
        uiHandler.removeViewBinding()
    }

    private fun handlerThreadTest() {
        if (!isHandlersInit) {
            isHandlersInit = true
            handlerThread = HandlerThread("handlerThread")
            handlerThread.start()
            childHandler = Handler(handlerThread.looper, ChildCallback(uiHandler))
        }

        val msg = Message.obtain()
        msg.arg1 = ++index
        childHandler.sendMessageDelayed(msg, 1000)
    }

    private fun intentServiceTest() {
        startService(Intent(this, DemoIntentService::class.java))
    }

    private fun asyncTaskTest() {
        DemoAsyncTask(viewBinding).execute()
    }

    class DemoAsyncTask(vb: ActivityDispatchDemoBinding) : AsyncTask<String, Int, String>() {

        private val viewbinding = WeakReference(vb)

        override fun onPreExecute() {
            val text = "AsyncTask start头！！！"
            viewbinding.get()!!.tvShow.text = text
        }

        override fun onProgressUpdate(vararg values: Int?) {
            val text = "任务已经执行 -> ${values[0]}0% <-"
            viewbinding.get()!!.tvShow.text = text
        }

        override fun doInBackground(vararg params: String?): String? {
            for(index in 1 until 11) {
                Thread.sleep(1000)
                HpLog.e("DemoAsyncTask任务执行进度  ${index}0%"::toString)
                publishProgress(index)
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            val text = "DONE！！！"
            viewbinding.get()!!.tvShow.text = text
        }

    }

    class DemoIntentService : IntentService("DemoIntentService") {
        override fun onHandleIntent(intent: Intent?) {
            for (index in 1..10) {
                Thread.sleep(1000)
                HpLog.e("模拟耗时任务,running...￥${index}"::toString)
            }
        }

        override fun onCreate() {
            HpLog.e("DemoIntentService____onCreate"::toString)
            super.onCreate()
        }

        override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
            //当上一条消息没有执行完，就重新startService，不会重新走onCreate和onDestroy方法
            // ，但会重新走这个方法，立即发送一条新消息（这个消息就会排队排到之前的消息后面）
            HpLog.e("DemoIntentService____onStartCommand"::toString)
            return super.onStartCommand(intent, flags, startId)
        }

        override fun onDestroy() {
            HpLog.e("DemoIntentService____onDestroy"::toString)
            super.onDestroy()
        }
    }

    class UiHandler(looper: Looper, private var viewBinding: ActivityDispatchDemoBinding?) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            viewBinding?.tvShow?.text = msg.arg1.toString()
        }

        fun removeViewBinding() {
            viewBinding = null
        }
    }

    class ChildCallback(private val uiHandler: UiHandler): Handler.Callback {
        override fun handleMessage(msg: Message): Boolean {
            if (msg.arg1 != 0) {
                val message = Message.obtain()
                message.arg1 = msg.arg1
                uiHandler.sendMessage(message)
                return true
            }
            return false
        }
    }

    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = "HandlerThread && IntentService"
}