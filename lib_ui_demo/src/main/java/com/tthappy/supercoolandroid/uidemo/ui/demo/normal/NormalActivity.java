package com.tthappy.supercoolandroid.uidemo.ui.demo.normal;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity;
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityNormalDemoBinding;

/**
 * Author:      tfhe
 * Create Date: Created in 2020/11/10 14:43
 * Update Date:
 * Modified By:
 * Description: 朴实无华的普通页面
 */
@Route(path = BridgeConstansUIDemo.NORMAL)
public class NormalActivity extends BaseActivity<ActivityNormalDemoBinding, BaseViewModel> {
    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void observeData() {

    }

//    @Override
//    protected int getLayoutId() {
//        return R.layout.activity_normal;
//    }


//    @Override
//    protected void initView() {
//        ToolbarHelper helper = getToolbarHelper();
//        helper.enableBack(context);
//        helper.setTitle("朴实无华的普通页面");
//    }
}
