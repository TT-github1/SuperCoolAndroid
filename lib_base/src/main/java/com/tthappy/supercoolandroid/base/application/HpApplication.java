package com.tthappy.supercoolandroid.base.application;

import android.app.Application;

import com.alibaba.android.arouter.launcher.ARouter;
import com.tthappy.supercoolandroid.base.BuildConfig;
import com.tthappy.supercoolandroid.base.router.RouterService;


/**
 * Author:      tfhe
 * Create Date: Created in 2020/10/12 14:29
 * Update Date:
 * Modified By:
 * Description:
 */
public class HpApplication extends Application {

    protected static Application mApplication;

    public static Application getInstance() {
        return mApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mApplication = this;

        if (BuildConfig.DEBUG) {
            //以下两种属性必须在初始化之前开启
            //打印日志
            ARouter.openLog();
            //开启调试模式（如果在InstantRun模式下进行，必须开启调试模式）
            // 线上版本需要关闭，否则会有安全风险
            ARouter.openDebug();
        }

        // Android工程中打开协程debug模式，无法直接在工程中进行设置，而是需要在Android代码中设置相应属性。设置代码如下：
        // 开启用以打印协程名称（好像不开也行？）
//        System.setProperty("kotlinx.coroutines.debug", "on" );

        ARouter.init(this);

        //屏幕适配
        ARouter.getInstance().build(RouterService.EQUIPMENTUTILS).navigation();
        //Log日志
        ARouter.getInstance().build(RouterService.HPLOG).navigation();
        //Debug调试功能
        ARouter.getInstance().build(RouterService.DEBUG).navigation();
        //热修复功能
        ARouter.getInstance().build(RouterService.HOTFIX).navigation();
    }
}
