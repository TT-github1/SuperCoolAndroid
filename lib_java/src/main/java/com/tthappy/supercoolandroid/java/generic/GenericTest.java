package com.tthappy.supercoolandroid.java.generic;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/16 14:27
 * Update Date:
 * Modified By:
 * Description:
 */
public class GenericTest<T> {

    public static void main(String[] args) {
        new Node();
    }

    static class Node {

        Tree tree = new Tree();

        Node() {
            test(tree);
            System.out.println(tree.data);
        }

        public void test(Tree tree) {
            Tree newTree = new Tree();
            newTree.data = 999;
            tree = newTree;
        }
    }

    static class Tree {
        int data;
    }
}
