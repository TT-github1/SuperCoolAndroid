package com.tthappy.supercoolandroid.java.javarouter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/23 9:01
 * Update Date:
 * Modified By:
 * Description:
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface JavaRouter {
    String path();
    String group() default "";
}