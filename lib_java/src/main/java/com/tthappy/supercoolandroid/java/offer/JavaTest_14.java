package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/28 11:32
 * Update Date:
 * Modified By:
 * Description: 字符串旋转
 */
public class JavaTest_14 {
    public static void main(String[] args) {
        String msg = "abcdef";
        System.out.println(rotate(msg, 1));
    }

    private static String rotate(String msg, int n) {
        if (n > msg.length()) return msg;
        char[] oldString = msg.toCharArray();
        char[] temp = new char[n];
        System.arraycopy(oldString, 0, temp, 0, n);
        System.arraycopy(oldString, n, oldString, 0, oldString.length - n);
        System.arraycopy(temp, 0, oldString, oldString.length - n, n);
        return String.valueOf(oldString);
    }
}
