package com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal

data class DemoListEntity(
        val list: List<String>
)