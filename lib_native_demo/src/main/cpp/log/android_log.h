//
// Created by ad on 2022/1/26.
//

#ifndef SUPERCOOLANDROID_ANDROID_LOG_H
#define SUPERCOOLANDROID_ANDROID_LOG_H

#include <android/log.h>

#define TAG "tthappy"
#define LOG(...)                                                    \
__android_log_print(ANDROID_LOG_ERROR, TAG, "******************");  \
__android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__);           \
__android_log_print(ANDROID_LOG_ERROR, TAG, "******************");  \
__android_log_print(ANDROID_LOG_ERROR, TAG, " ")

#endif //SUPERCOOLANDROID_ANDROID_LOG_H
