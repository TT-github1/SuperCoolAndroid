package com.tthappy.supercoolandroid.views.view

import android.content.Context
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import com.tthappy.supercoolandroid.views.R

/**
 * Author:      tfhe
 * Create Date: Created in 2022/3/31 15:46
 * Update Date:
 * Modified By:
 * Description:
 */
class HpPasswordEditText(context: Context, attrs: AttributeSet): AbstractHpEditText(context, attrs)  {

    var mCipherIconDrawableId = R.drawable.icon_hide_password
    var mPlainIconDrawableId = R.drawable.icon_show_password

    init {
        setCipherState()
    }

    private fun setCipherState() {
        transformationMethod = PasswordTransformationMethod.getInstance()
        setIcon(mCipherIconDrawableId)
        showIcon()
    }

    private fun setPlainState() {
        transformationMethod = HideReturnsTransformationMethod.getInstance()
        setIcon(mPlainIconDrawableId)
        showIcon()
    }

    override fun notifyEvent() {
        val type = transformationMethod
        if (type === HideReturnsTransformationMethod.getInstance()) {
            setCipherState()
        }
        if (type === PasswordTransformationMethod.getInstance()) {
            setPlainState()
        }
        val selection: Int = getPreSelectionEnd()
        if (selection <= text!!.length) {
            setSelection(selection)
        }
    }

    override fun precondition() = true
}