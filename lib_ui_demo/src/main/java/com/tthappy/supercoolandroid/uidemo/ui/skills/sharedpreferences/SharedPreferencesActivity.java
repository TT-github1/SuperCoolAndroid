package com.tthappy.supercoolandroid.uidemo.ui.skills.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;

@Route(path = BridgeConstansUIDemo.SHAREDPREFERENCES)
public class SharedPreferencesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharedpreferences_demo);
//        DisplayUtils.setTransparentStatusBar(this);

        EditText editText = findViewById(R.id.et_content);
        TextView textDesc = findViewById(R.id.tv_desc);
        TextView textView = findViewById(R.id.tv_save);

        textView.setOnClickListener(v -> {
            SharedPreferences sharedPreferences = getSharedPreferences("test", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("editText", editText.getText().toString());
            boolean status = editor.commit();
            if (status) Toast.makeText(this, "保存成功！", Toast.LENGTH_SHORT).show();
            textDesc.setText("请到/data/data/com.tthappy.supercoolandroid/shared_prefs/目录下查看");
            textView.setText(sharedPreferences.getString("editText", "啥也不是"));
        });
    }
}