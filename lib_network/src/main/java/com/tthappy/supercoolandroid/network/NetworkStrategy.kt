package com.tthappy.supercoolandroid.network

import okhttp3.Interceptor

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/9 20:02
 * Update Date:
 * Modified By:
 * Description:
 */
data class NetworkStrategy(
        val baseUrl: String = "",
        val enableLogger: Boolean = true,
        val retryOnFailure: Boolean = true,
        val connectTimeOutSeconds: Long = 30L,
        val interceptors: List<Interceptor> = ArrayList(),
        val trustAllCerts: Boolean = false
)