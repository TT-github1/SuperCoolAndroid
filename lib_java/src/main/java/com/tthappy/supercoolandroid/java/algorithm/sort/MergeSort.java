package com.tthappy.supercoolandroid.java.algorithm.sort;

import com.tthappy.supercoolandroid.java.datamock.HpDataLib;
import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/23 14:03
 * Update Date:
 * Modified By:
 * Description: 归并排序 （拆分 - 合并）
 *
 *              归并排序能成功排序，依赖于在将两个子数组进行合并操作时，这两个子数组分别已经是有序的了，
 *              于是合并时，可以少考虑很多事情，仅仅将两个数组的前端（下标靠前）元素不断进行比较，并将
 *              较小的元素放到临时数组中，然后指针后移，最终就可以得到一个新的有序数组
 */
public class  MergeSort {
    public static void main(String[] args) {
        int[] arr = HpDataLib.getArrI(12);
        System.out.println(Arrays.toString(arr));
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static void sort(int[] arr) {
        int[] tmp = new int[arr.length];
        sort(arr, 0, arr.length - 1, tmp);
    }

    public static void sort(int[] arr, int left, int right, int[] tmp) {
        // 判断递归结束条件（结果就是当分到子数组长度为1时结束）
        if (left < right) { // else => left >= right
            // 二分数组，继续递归
            int mid = (left + right) / 2;
            sort(arr, left, mid, tmp);
            sort(arr, mid + 1, right, tmp);

            //合并上面被拆分的数组
            merge(arr, left, mid, right, tmp);
        }
    }

    public static void merge(int[] arr, int left, int mid, int right, int[] tmp) {
        // 左子序列指针
        int leftIndex = left;
        // 右子序列指针
        int rightIndex = mid + 1;
        // 临时指针
        int t = 0;

        // 如果左右子序列都还没遍历完成
        while (leftIndex <= mid && rightIndex <= right) {
            tmp[t++] = arr[leftIndex] <= arr[rightIndex] ? arr[leftIndex++] : arr[rightIndex++];
        }

        // 如果左子序列还没遍历完成，将剩余的元素全部塞进临时数组
        while (leftIndex <= mid) {
            tmp[t++] = arr[leftIndex++];
        }
        while (rightIndex <= right) {
            tmp[t++] = arr[rightIndex++];
        }

        // 即将拷贝临时数组，先将指针归位
        t = 0;

        while (left <= right) {
            arr[left++] = tmp[t++];
        }
    }
}
