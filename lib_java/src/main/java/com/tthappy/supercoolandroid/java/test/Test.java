package com.tthappy.supercoolandroid.java.test;

public class Test {
    public static void main(String[] args) {
        new Test().print(new AAA() {
            @Override
            public void onA() {
                System.out.println("hhh");
            }
        });
    }

    public void print(AAA a) {
        a.onA();
    }

    interface AAA {
        void onA();
    }
}
