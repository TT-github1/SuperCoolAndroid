package com.tthappy.supercoolandroid.views.pop

import android.animation.ValueAnimator
import android.app.Activity
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.AccelerateInterpolator
import android.widget.PopupWindow
import androidx.viewbinding.ViewBinding
import com.tthappy.supercoolandroid.views.inflateBindingWithGeneric

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/28 17:27
 * Update Date:
 * Modified By:
 * Description:
 */
abstract class BasePopupWindow<VB: ViewBinding>(val mActivity: Activity) {

    private var systemUiVisibility = 0
    protected val popupWindow by lazy { PopupWindow(
            viewBinding.root,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            true
    ) }

    protected val viewBinding: VB = inflateBindingWithGeneric(mActivity.layoutInflater)

    init {
        initView()
        initListener()
    }

    private fun initView() {
//        popupWindow.animationStyle = R.style.am_pop_top_anim_style
        // 这个属性能让软键盘弹起时露出底下activity的布局内容，需要设为false
        popupWindow.isClippingEnabled = false
        popupWindow.inputMethodMode = PopupWindow.INPUT_METHOD_NEEDED
        systemUiVisibility = mActivity.window.decorView.systemUiVisibility
//        StatusBarUtil.setStatusBarFontColor(mActivity.window, true)
    }

    var onDismissListener: (() -> Unit)? = null
    private fun initListener() {
        popupWindow.setOnDismissListener {
            mActivity.window.decorView.systemUiVisibility = systemUiVisibility
            animWindow(0.6f, 1f)
            onDismissListener?.invoke()
        }
    }

    private fun animWindow(from: Float, to: Float) {
        val valueAnimator = ValueAnimator.ofFloat(from, to)
        valueAnimator.duration = 300
        valueAnimator.interpolator = AccelerateInterpolator()
        valueAnimator.addUpdateListener {
            setBackground(it.animatedValue as Float)
        }
        valueAnimator.start()
    }

    /**
     *  popup想要拥有灰色背景其实需要改变它所依附的activity的window的alpha值
     *  来达到目的，所以响应的在popupWindow关闭时要记得把activity的window的
     *  alpha值改回去
     */
    private fun setBackground(value: Float) {
        val layoutParams = mActivity.window.attributes
        layoutParams.alpha = value
        mActivity.window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        mActivity.window.attributes = layoutParams
    }

    fun show() {
        val view = mActivity.window.decorView
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, 0, 0)
        animWindow(1.0f, 0.6f)
    }

    open fun show(anchor: View) {
        show(anchor, 0, 0)
    }

    fun show(anchor: View, x: Int, y: Int) {
        show(anchor, x, y, Gravity.TOP or Gravity.START)
    }

    fun show(anchor: View, x: Int, y: Int, gravity: Int) {
        popupWindow.showAsDropDown(anchor, x, y, gravity)
        animWindow(1.0f, 0.6f)
    }

    fun isShowing(): Boolean = popupWindow.isShowing

    fun dismiss() {
        if (isShowing()) popupWindow.dismiss()
    }
}