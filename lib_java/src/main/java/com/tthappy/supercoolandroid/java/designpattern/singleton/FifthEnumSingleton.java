package com.tthappy.supercoolandroid.java.designpattern.singleton;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/4 9:13
 * Update Date:
 * Modified By:
 * Description: 枚举 (避免多线程同步问题，防止反序列化重新创建对象)
 */
public enum FifthEnumSingleton {
    INSTANCE
}
