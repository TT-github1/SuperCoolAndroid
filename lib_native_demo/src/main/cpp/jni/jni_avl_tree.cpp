//
// Created by ad on 2022/1/26.
//

#include "../common_dependencies.h"
#include "../tree/avl_tree.h"

static AVLTree * avlTree = nullptr;

static void visitTree(int data) {
    LOG("**  This node is %d", data);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_tthappy_nativedemo_tree_AVLTree_add(
    JNIEnv *,
    jobject ,
    jint data
) {
    avlTree = avlTree_add(avlTree, data);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_tthappy_nativedemo_tree_AVLTree_remove(
        JNIEnv *,
        jobject ,
        jint data
) {
    avlTree = avlTree_remove(avlTree, data);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_tthappy_nativedemo_tree_AVLTree_showTree(
    JNIEnv *,
    jobject
) {
    avlTree_preOrder(avlTree, visitTree);
}