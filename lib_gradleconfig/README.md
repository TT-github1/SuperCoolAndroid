### 升级步骤

- 修改ConfigTask类中的代码（或者其他代码）
- 修改本模块下的build.gradle文件中的pom.version，例如：pom.version = '1.0.2'  -->  pom.version = '1.0.3'
- 打开右上角的Gradle面板，找到本模块下的Tasks下的upload下的uploadArchives，执行（执行完后应该可以看到本地configplugin模块下多了一个版本）
- 上一步成功后，修改本项目下的build.gradle文件中的插件版本号classpath 'com.tthappy.plugin:gradle-config:1.0.3'
- 搞定