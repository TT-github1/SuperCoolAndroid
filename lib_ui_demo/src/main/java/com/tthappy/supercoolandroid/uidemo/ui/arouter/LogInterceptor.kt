package com.tthappy.supercoolandroid.uidemo.ui.arouter

import android.content.Context
import com.alibaba.android.arouter.facade.Postcard
import com.alibaba.android.arouter.facade.annotation.Interceptor
import com.alibaba.android.arouter.facade.callback.InterceptorCallback
import com.alibaba.android.arouter.facade.template.IInterceptor
import com.tthappy.supercoolandroid.log.HpLog

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/22 16:10
 * Update Date:
 * Modified By:
 * Description:
 */
@Interceptor(priority = 4, name = "log")
class LogInterceptor : IInterceptor {
    override fun process(postcard: Postcard, callback: InterceptorCallback) {
        HpLog.e(postcard::toString)
        callback.onContinue(postcard)
    }

    override fun init(context: Context?) {}
}