package com.tthappy.supercoolandroid.uidemo.ui.question.nestedscrollandfling

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.NestedScrollingParent2
import androidx.core.view.ViewCompat
import com.tthappy.supercoolandroid.log.HpLog
import com.tthappy.supercoolandroid.utils.measure.DimenUtils

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/27 9:58
 * Update Date:
 * Modified By:
 * Description: 对于嵌套的Parent来说， 要做的事情比较简单，就是在接口提供的5个回调事件里正确的处理一些信息和进行一些操作就行
 *              例如：在onNestedPreScroll方法中，只需要判断自身现在的状态需不需要滑动，需要滑动的话就调用scrollBy方法滑动，
 *              然后把消耗了的距离如实地传给consumed数组，告诉Child我们使用了多少距离，一切都是比较按部就班，核心功能其实在Child身上
 */
class HpNestedScrollParentView(context: Context, attrs: AttributeSet): LinearLayout(context, attrs), NestedScrollingParent2 {

    private val searchBarHeight by lazy { DimenUtils.dp2px(200f) }

    override fun onStartNestedScroll(child: View, target: View, axes: Int, type: Int): Boolean {
        return axes and ViewCompat.SCROLL_AXIS_VERTICAL != 0
    }
    override fun onNestedScrollAccepted(child: View, target: View, axes: Int, type: Int) {}
    override fun onStopNestedScroll(target: View, type: Int) {}

    override fun onNestedScroll(
        target: View,
        dxConsumed: Int,
        dyConsumed: Int,
        dxUnconsumed: Int,
        dyUnconsumed: Int,
        type: Int,
    ) {}

    override fun onNestedPreScroll(target: View, dx: Int, dy: Int, consumed: IntArray, type: Int) {
        val hiddenTop = dy > 0 && scrollY < searchBarHeight
        val showTop = dy < 0 && scrollY > 0
        if (hiddenTop||showTop){
            HpLog.e("output $dy"::toString)

            scrollBy(0, dy)
            consumed[1] = dy  //消耗此次滚动值,这样子View就不会滑动,如果去掉这句父View和子View同时滚动

//            changeRecyclerViewHeight()
        }
    }
}