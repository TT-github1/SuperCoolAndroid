package com.tthappy.supercoolandroid.java.thread;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/16 17:33
 * Update Date:
 * Modified By:
 * Description:
 */
public abstract class AbstractClass {
    public abstract void openBook();
    public abstract void read();
    public abstract void closeBook();

    public AbstractClass() {
        openBook();
        read();
        closeBook();
    }
}
