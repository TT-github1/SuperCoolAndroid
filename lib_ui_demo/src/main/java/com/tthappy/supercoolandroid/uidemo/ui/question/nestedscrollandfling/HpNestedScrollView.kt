package com.tthappy.supercoolandroid.uidemo.ui.question.nestedscrollandfling

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.ViewConfiguration
import android.widget.LinearLayout
import android.widget.Scroller
import androidx.core.view.NestedScrollingChild2
import androidx.core.view.NestedScrollingChildHelper
import androidx.core.view.ViewCompat

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/27 9:58
 * Update Date:
 * Modified By:
 * Description: 对于嵌套的 Child 来说， 它是嵌套滑动效果逻辑的核心，嵌套滑动由它发起，由它控制，由它结束
 *
 *              例如：dispatchNestedPreScroll方法需要在此View的scrollTo方法之前调用，让Parent有优先决定是否要嵌套滑动的权力，
 *              在Parent进行了嵌套滑动后，要对Parent消耗的距离修正到对lastY属性上，
 */

class HpNestedScrollView(context: Context, attrs: AttributeSet): LinearLayout(context, attrs),
    NestedScrollingChild2 {

    private val consumed = intArrayOf(0, 0)
    private val offsetInWindow = intArrayOf(0, 0)
    // 上一次触摸事件的y坐标
    private var lastY = 0f
    // 用来算速度的工具（通过在每次的触摸事件中打点）
    private lateinit var mVelocityTracker: VelocityTracker
    // 用来根据传入的速度算当前应该滚动到的位置的工具
    private val scroller by lazy { Scroller(context) }
    private val mScrollingChildHelper by lazy { NestedScrollingChildHelper(this) }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!super.onTouchEvent(event)) {
            if (!this::mVelocityTracker.isInitialized) mVelocityTracker = VelocityTracker.obtain()
            var isActionUp = false
            // 拷贝一份触摸事件，我猜是为了防止污染原事件
            val eventCopy = MotionEvent.obtain(event)

            val eventY = event.y

            when(event.actionMasked) {
                MotionEvent.ACTION_DOWN -> lastY = eventY
                MotionEvent.ACTION_MOVE -> {
                    val dy = lastY - eventY

                    dispatchNestedPreScroll(0, (dy + 0.5f).toInt(), consumed, offsetInWindow, ViewCompat.TYPE_TOUCH)

                    if (consumed[1] == 0) scrollTo(0, (scrollY + dy).toInt())
                    // 当发生嵌套滑动后，我们手指在此View中的位置会被父View（NestedScrollingParent）带偏
                    // 此时就不能再用eventY当lastY了，需要把dy偏移的距离补回来
                    lastY = if (consumed[1] == 0) eventY else eventY + dy
                }
                MotionEvent.ACTION_UP -> {
                    // 最后一次打点
                    mVelocityTracker.addMovement(eventCopy)
                    isActionUp = true
                    // 设定一个最大速度，速度太快体验也不好
                    val maxV = ViewConfiguration.get(context).scaledMaximumFlingVelocity.toFloat()
                    // 这里的 1000 是你想要的速度单位。值1提供像素/毫秒，1000提供像素/秒
                    mVelocityTracker.computeCurrentVelocity(1000, maxV)
                    val yVelocity = -mVelocityTracker.getYVelocity(event.getPointerId(0))

                    startFling(yVelocity.toInt())

                    mVelocityTracker.clear()
                }
                else -> {}
            }

            if (!isActionUp) {
                // 每次触摸事件打点
                mVelocityTracker.addMovement(eventCopy)
            }
            eventCopy.recycle()

            return true
        }

        return false
    }

    private val refreshRunnable = Runnable {
        if (scroller.computeScrollOffset()) {
            scrollTo(0, scroller.currY)
            postOnAnimationFun()
        }
    }

    private fun postOnAnimationFun() {
        // 使Runnable在下一个动画时间步长上执行
        postOnAnimation (refreshRunnable)
    }

    private fun startFling(velocity: Int) {
        // 通知scroller开始计算应该活动到的位置
        scroller.fling(0, scrollY, 0, velocity, Int.MIN_VALUE, Int.MAX_VALUE, Int.MIN_VALUE, Int.MAX_VALUE)

        postOnAnimationFun()
    }

    override fun scrollTo(x: Int, y: Int) {
        startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL, ViewCompat.TYPE_TOUCH)
        val realHeight = getRealHeight()
        val newY = if (y < 0) 0
        else if (y > realHeight) realHeight
        else y
        super.scrollTo(x, newY)
    }

    private fun getRealHeight(): Int {
        var height = 0
        for(index in 0 until childCount) {
            height += getChildAt(index).height
        }
        return height - this.height
    }

    override fun dispatchNestedScroll(
        dxConsumed: Int,
        dyConsumed: Int,
        dxUnconsumed: Int,
        dyUnconsumed: Int,
        offsetInWindow: IntArray?,
        type: Int,
    ) = mScrollingChildHelper.dispatchNestedScroll(
        dxConsumed,
        dyConsumed,
        dxUnconsumed,
        dyUnconsumed,
        offsetInWindow,
        type
    )

    override fun dispatchNestedPreScroll(
        dx: Int,
        dy: Int,
        consumed: IntArray?,
        offsetInWindow: IntArray?,
        type: Int,
    ): Boolean {
        return mScrollingChildHelper.dispatchNestedPreScroll(dx, dy, consumed, offsetInWindow, type)
    }

    init {
        mScrollingChildHelper.isNestedScrollingEnabled = true
    }

    override fun startNestedScroll(axes: Int, type: Int): Boolean {
        return mScrollingChildHelper.startNestedScroll(axes, type)
    }
    override fun stopNestedScroll(type: Int) = mScrollingChildHelper.stopNestedScroll(type)
    override fun hasNestedScrollingParent(type: Int) = mScrollingChildHelper.hasNestedScrollingParent(type)
}