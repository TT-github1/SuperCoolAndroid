package com.tthappy.supercoolandroid.uidemo.service

import android.content.Context
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.application.service.IHotFixUtilService
import com.tthappy.supercoolandroid.base.router.RouterService
import com.tthappy.supercoolandroid.uidemo.ui.classloader.HotFixUtil

@Route(path = RouterService.HOTFIX)
class HotFixUtilService: IHotFixUtilService {
    override fun init(context: Context?) {
        HotFixUtil.fix(context!!.classLoader)
    }
}