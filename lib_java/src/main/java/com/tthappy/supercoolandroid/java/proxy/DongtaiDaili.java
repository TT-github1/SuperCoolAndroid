package com.tthappy.supercoolandroid.java.proxy;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/1/3 12:32
 * Update Date:
 * Modified By:
 * Description:
 */
interface IMessage {

    String eho(String str);

    String mmp(String str);
}

interface ISay {
    String say(String str);
}

class MessageImpl implements IMessage, ISay {

    @Override
    public String eho(String str) {
        return "[ECHO:]" + str;
    }

    @Override
    public String mmp(String str) {
        return "[MMP:]" + str;
    }

    @Override
    public String say(String str) {
        return "[SAY:]" + str;
    }
}

class ServerProxy implements InvocationHandler {
    private Object target;

    public Object bind(Object target) {
        this.target = target;
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        this.connect();
        Object resultValue = method.invoke(this.target, args);

        Annotation[] annotations = method.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation.toString());
        }
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for (Annotation[] parameterAnnotation : parameterAnnotations) {
            for (Annotation annotation : parameterAnnotation) {
                System.out.println(annotation.toString());
            }
        }
        this.close();
        return resultValue;
    }

    public void connect() {
//        Log.e("hhh", "[代理业务]:connect success");
        System.out.println("[代理业务]:connect success");
    }

    public void close() {
//        Log.e("hhh", "[代理业务]:close success");
        System.out.println("[代理业务]:close success");
    }
}

public class DongtaiDaili {
    public DongtaiDaili() {
        IMessage messageObject = (IMessage) new ServerProxy().bind(new MessageImpl());
//        Log.e("hhh", "[真实业务]");
//        Log.e("hhh", messageObject.eho("[真实业务]"));
//        System.out.println(messageObject.say("[真实业务]"));
        System.out.println(messageObject.eho("[真实业务]"));
    }

    public static void main(String[] args) {
        new DongtaiDaili();
    }
}
