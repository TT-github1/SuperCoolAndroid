package com.tthappy.supercoolandroid.java.classloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class FindClassClassLoader extends ClassLoader {
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        try {
            String myPath = "E:\\APP\\git_Repository\\SuperCoolAndroid\\lib_java\\build\\classes\\java\\main\\com\\tthappy\\supercoolandroid\\java\\demo\\DemoTest.class";
            // E:\APP\git_Repository\SuperCoolAndroid\lib_java\build\classes\java\main\com\tthappy\supercoolandroid\java\demo
            File file = new File(myPath);
            InputStream inputStream = new FileInputStream(file);
            int len = inputStream.available();
            byte[] classBytes = new byte[len];
            inputStream.read(classBytes);
            inputStream.close();
            return defineClass(name, classBytes, 0, classBytes.length);

        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new ClassNotFoundException("欧欧~木有找到类");
    }

    public static void main(String[] args) {
        try {
            System.out.println(new FindClassClassLoader().loadClass("com.tthappy.supercoolandroid.java.demo.DemoTest"));
            System.out.println(new FindClassClassLoader().findLoadedClass("com.tthappy.supercoolandroid.java.demo.DemoTest"));
            System.out.println("ggg");
        } catch (Exception e) { // ClassNotFound
            System.out.println("hhh");
            System.out.println(e);
            e.printStackTrace();
        }
    }
}
