//
// Created by ad on 2022/1/26.
//

#ifndef SUPERCOOLANDROID_SEQUENTIAL_STORAGE_BINARY_TREE_H
#define SUPERCOOLANDROID_SEQUENTIAL_STORAGE_BINARY_TREE_H

#include "../common_dependencies.h"

struct SequentialStorageBinaryTree {
    int size;
    void * nodes;
};

typedef struct SequentialStorageBinaryTree SSBTree;

extern "C" JNIEXPORT
void JNICALL Java_com_tthappy_nativedemo_tree_SequentialStorageBinaryTree_createTree(JNIEnv * env, jobject thiz);

extern "C" JNIEXPORT
void JNICALL Java_com_tthappy_nativedemo_tree_SequentialStorageBinaryTree_add(JNIEnv * env, jobject thiz, jint data);

extern "C" JNIEXPORT
void JNICALL Java_com_tthappy_nativedemo_tree_SequentialStorageBinaryTree_showTree(JNIEnv * env, jobject thiz);

#endif //SUPERCOOLANDROID_SEQUENTIAL_STORAGE_BINARY_TREE_H
