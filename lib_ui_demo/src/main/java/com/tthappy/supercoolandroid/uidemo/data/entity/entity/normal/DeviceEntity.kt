package com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/24 18:48
 * Update Date:
 * Modified By:
 * Description:
 */
data class DeviceEntity (
    val name: String,
    val text: String,
    val button: String
)