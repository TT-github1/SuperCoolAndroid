package com.tthappy.supercoolandroid.utils.file

import java.io.File
import java.io.FileWriter

object LogToFileUtils {

    lateinit var writer: FileWriter

    fun init(logFileName: String = "hpLog.txt", path: String = "/storage/emulated/0") {
        // 确保有文件读写权限
        // 创建log文件
        val file = File("$path/$logFileName")
        file.createNewFile()

        // 创建文件输出流
        writer = FileWriter(file, true)

    }

    fun log(msg: String) {
        writer.write(msg)
    }

    fun release() {
        writer.close()
    }

}