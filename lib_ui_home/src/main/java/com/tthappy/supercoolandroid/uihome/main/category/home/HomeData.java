package com.tthappy.supercoolandroid.uihome.main.category.home;

import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.base.router.entity.HomeEntity;
import com.tthappy.supercoolandroid.uihome.R;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
//import com.tthappy.supercoolandroid.java.test.HomeDataUtil;

/**
 * Author:      tfhe
 * Create Date: Created in 2020/10/30 14:25
 * Update Date:
 * Modified By:
 * Description:
 */
public class HomeData {
    private final List<HomeEntity> data = new ArrayList<>();

    public List<HomeEntity> getData() {
//        return new HomeDataUtil().getList();
        try {
            Class<?> clz = Class.forName("com.tthappy.supercoolandroid.java.test.HomeDataUtil");
            Constructor<?> constructor = clz.getConstructor();
            Object o = constructor.newInstance();
            Method getList = clz.getMethod("getList");
            List<HomeEntity> invoke = (List<HomeEntity>) getList.invoke(o);
            return add(invoke, data);
        } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        return data;
    }

    public HomeData() {
        data.add(new HomeEntity("UI测试专用入口", R.drawable.icon_home_header, "闲杂人等禁止入内！！！", BridgeConstansUIDemo.UITEST));
        data.add(new HomeEntity("Socket按钮客户端", R.drawable.icon_home_header, "客户端", BridgeConstansUIDemo.SOCKET_CLIENT));
        data.add(new HomeEntity("Socket按钮服务端", R.drawable.icon_home_header, "服务端", BridgeConstansUIDemo.SOCKET_SERVER));
        data.add(new HomeEntity("玩安卓文章列表", R.drawable.icon_home_header, "就是", BridgeConstansUIDemo.WAN_ARTICLES));
        data.add(new HomeEntity("BridgeConstansUIDemo.SCROLLVIEW_AND_VIEWPAGER", R.drawable.icon_home_header, "BridgeConstansUIDemo.SCROLLVIEW_AND_VIEWPAGER", BridgeConstansUIDemo.WAN_REGISTER));
        data.add(new HomeEntity("BridgeConstansUIDemo.SCROLLVIEW_AND_VIEWPAGER", R.drawable.icon_home_header, "BridgeConstansUIDemo.SCROLLVIEW_AND_VIEWPAGER", BridgeConstansUIDemo.SCROLLVIEW_AND_VIEWPAGER));
        data.add(new HomeEntity("FlexboxLayout", R.drawable.icon_home_header, "FlexboxLayoutFlexboxLayoutFlexboxLayoutFlexboxLayout", BridgeConstansUIDemo.FLEXBOXLAYOUT));
        data.add(new HomeEntity("ListView试炼", R.drawable.icon_home_header, "ListView试炼", BridgeConstansUIDemo.LISTVIEW));
        data.add(new HomeEntity("RecyclerView试炼", R.drawable.icon_home_header, "RecyclerView试炼", BridgeConstansUIDemo.RECYCLERVIEW));
        data.add(new HomeEntity("超级无敌炫酷的自定义控件", R.drawable.icon_home_header, "这是得有多炫酷。。。", BridgeConstansUIDemo.SHOW_CUSTOM_CONTROL));
        data.add(new HomeEntity("拉稀的dispatch", R.drawable.icon_home_header, "暂无描述", BridgeConstansUIDemo.DISPATCH));
        data.add(new HomeEntity("朴实无华的普通页面", R.drawable.icon_home_header, "普通页面", BridgeConstansUIDemo.NORMAL));
        data.add(new HomeEntity("咩野都哞", R.drawable.icon_home_header, "emmm...", BridgeConstansUIDemo.SHOW_CUSTOM_CONTROL));
        data.add(new HomeEntity("okhttp", R.drawable.icon_home_header, "okhttp", BridgeConstansUIDemo.OKHTTP));
        data.add(new HomeEntity("阿巴阿巴阿巴", R.drawable.icon_home_header, "。。。", BridgeConstansUIDemo.SHOW_CUSTOM_CONTROL));
        data.add(new HomeEntity("viewmodel", R.drawable.icon_home_header, "viewmodel  viewmodel  viewmodel", BridgeConstansUIDemo.VIEWMODEL));
        data.add(new HomeEntity("Binder", R.drawable.icon_home_header, "Binder", BridgeConstansUIDemo.BINDER));
        data.add(new HomeEntity("WebView", R.drawable.icon_home_header, "WebView", BridgeConstansUIDemo.WEB));
    }

    private List<HomeEntity> add(List<HomeEntity> list1, List<HomeEntity> list2) {
        List<HomeEntity> resultList = new ArrayList<>();
        resultList.addAll(list1);
        resultList.addAll(list2);
        return resultList;
    }
}
