package com.tthappy.supercoolandroid.plugin.gradleconfig

import org.gradle.api.Plugin
import org.gradle.api.Project

class GradleConfigPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.tasks.create("HpConfig", ConfigTask.class)
    }
}