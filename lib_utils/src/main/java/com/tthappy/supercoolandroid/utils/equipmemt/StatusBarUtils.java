package com.tthappy.supercoolandroid.utils.equipmemt;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.tthappy.supercoolandroid.utils.measure.DimenUtils;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/10 16:20
 * Update Date:
 * Modified By:
 * Description: 斯dei特斯
 */
public class StatusBarUtils {

    /**
     *  获取状态栏高度
     */
    public static int getStatusBarHeight(Context context){
        int result = 0;
        int resultId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if(resultId > 0){
            result = context.getResources().getDimensionPixelSize(resultId);
        }
        return result;
    }

    /**
     *  设置View在StatusBar下方
     */
    public static void underStatusBar(Context context, View view) {
        underStatusBar(context, view, 0);
    }

    /**
     *  设置View的marginTop为StatusBar的高度和一段距离的和
     */
    public static void underStatusBar(Context context, View view, int distance) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.topMargin = getStatusBarHeight(context) + DimenUtils.dp2px(distance);
        view.setLayoutParams(layoutParams);
    }

    /**
     *  隐藏状态栏（全屏）（但从有状态栏的状态切换到无状态栏状态会有一个状态栏退出屏幕的动画）
     */
    public static void hideStatusBar(Window window) {
        // 第一步
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // 第二步
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
            window.setAttributes(lp);

            // 第三步
            final View decorView = window.getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     *  显示状态栏
     */
    public static void showStatusBar(Window window) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     *  设置状态栏文字和图标颜色白色
     */
    public static void setStatusBarFontDarkMode(Window window) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.TRANSPARENT);
        int systemUiVisibility = window.getDecorView().getSystemUiVisibility();
        systemUiVisibility |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
        window.getDecorView().setSystemUiVisibility(systemUiVisibility);
    }

    /**
     *  设置状态栏文字和图标颜色黑色
     */
    public static void setStatusBarFontLightMode(Window window) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.TRANSPARENT);
        int systemUiVisibility = window.getDecorView().getSystemUiVisibility();
        systemUiVisibility &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
        window.getDecorView().setSystemUiVisibility(systemUiVisibility);
    }

    /**
     *  切换状态栏文字颜色(黑色或者白色）
     */
    public static void setStatusBarFontDarkMode(Window window, boolean dark) {
        if (dark) setStatusBarFontDarkMode(window);
        else setStatusBarFontLightMode(window);
    }
}
