package com.tthappy.supercoolandroid.uidemo.ui.socket

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/23 8:51
 * Update Date:
 * Modified By:
 * Description:
 */
class SocketTextView(context: Context, attrs: AttributeSet): androidx.appcompat.widget.AppCompatTextView(context, attrs) {

    var listener: ((Int, Int) -> Unit)? = null

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {

        when(event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {

            }
            MotionEvent.ACTION_MOVE -> {
                x = event.rawX
                y = event.rawY
                listener?.invoke(x.toInt(), y.toInt())
            }
            else -> {}
        }

        return true
    }

}