//
// Created by ad on 2022/1/27.
//

#ifndef SUPERCOOLANDROID_AVL_TREE_H
#define SUPERCOOLANDROID_AVL_TREE_H

#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#include <stdlib.h>

struct AVLTree;
typedef struct AVLTree AVLTree, * Node;
struct AVLTree {
    int height;
    Node lChild;
    int data;
    Node rChild;
};

Node avlTree_add(Node node, int data);
Node avlTree_node(int data);
int avlTree_compare(int nodeValue, int data);
int avlTree_getHeight(Node node);
Node avlTree_LL(Node node);
Node avlTree_LR(Node node);
Node avlTree_RR(Node node);
Node avlTree_RL(Node node);
void avlTree_preOrder(AVLTree * tree, void (* visit)(int));
Node avlTree_remove(Node node, int data);
Node avlTree_getMax(Node tree);
Node avlTree_getMin(Node tree);

#endif //SUPERCOOLANDROID_AVL_TREE_H
