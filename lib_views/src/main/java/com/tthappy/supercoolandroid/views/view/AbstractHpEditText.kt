package com.tthappy.supercoolandroid.views.view

import android.content.Context
import android.graphics.Point
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat

/**
 * Author:      tfhe
 * Create Date: Created in 2022/3/31 13:52
 * Update Date:
 * Modified By:
 * Description:
 */
abstract class AbstractHpEditText(context: Context, attrs: AttributeSet): AppCompatEditText(context, attrs) {
    companion object {
        private const val CLICK_PADDING = 20
    }

    private var mPreSelectionEnd = 0
    private var isClickInIcon = false
    private var mPreSelectionStart = 0
    private var isCustomizeSize = false
    private lateinit var mIconRect: Rect
    private lateinit var mClickPoint: Point
    private var mIconDrawable: Drawable? = null
    private var mExpandAreaPadding = CLICK_PADDING
    private var mIconClickListener: ((View) -> Unit)? = null

    override fun performClick(): Boolean {
        if (isClickInIcon) {
            notifyEvent()
            if (mIconClickListener != null) {
                mIconClickListener!!.invoke(this)
            }
            doAfterClick()
        }
        return super.performClick()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        mPreSelectionStart = selectionStart
        mPreSelectionEnd = selectionEnd

        when(event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                isClickInIcon = precondition() && isClickInIcon((event.x + scrollX).toInt(), (event.y + scrollY).toInt())
                if (isClickInIcon) {
                    mClickPoint = Point(event.x.toInt(), (event.y + scrollY).toInt())
                }
            }
            else -> {}
        }
        return super.onTouchEvent(event)
    }

    private fun isClickInIcon(x: Int, y: Int): Boolean {
        if (mIconDrawable == null) return false
        updateRect()
        return mIconRect.contains(x, y)
    }

    private fun updateRect() {
        if (!this::mIconRect.isInitialized) mIconRect = Rect()

        // 可用高度？
        val vSpace = bottom - top - compoundPaddingBottom - compoundPaddingTop
        val compoundRect = Rect()
        mIconDrawable!!.copyBounds(compoundRect)

        val leftBound = scrollX + right - left - paddingRight - compoundRect.width()
        val topBound = scrollY + compoundPaddingTop + (vSpace - compoundRect.height()) / 2

        mIconRect.left = leftBound - mExpandAreaPadding
        mIconRect.right = leftBound + mIconDrawable!!.intrinsicWidth + mExpandAreaPadding
        mIconRect.top = topBound - mExpandAreaPadding
        mIconRect.bottom = topBound + mIconDrawable!!.intrinsicHeight + mExpandAreaPadding
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused && isClickInIcon) performClick()
    }

    fun setIcon(@DrawableRes resId: Int) {
        setIcon(ContextCompat.getDrawable(context, resId))
    }

//    fun setIcon(@DrawableRes resId: Int, size: Int) {
//        setIcon(ContextCompat.getDrawable(context, resId), size)
//    }

    private fun setIcon(drawable: Drawable?) {
        mIconDrawable = drawable
        isCustomizeSize = false
        mIconDrawable!!.setBounds(0, 0, mIconDrawable!!.intrinsicWidth, mIconDrawable!!.intrinsicHeight)
    }

//    fun setIcon(drawable: Drawable?, size: Int) {
//        mIconDrawable = drawable
//        isCustomizeSize = true
//        mIconDrawable!!.setBounds(0, 0, size, size)
//    }

    fun showIcon() {
        val drawables = compoundDrawables
        drawables[2] = mIconDrawable
        if (isCustomizeSize) setCompoundDrawables(drawables[0], drawables[1], drawables[2], drawables[3])
        else setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawables[2], drawables[3])
    }

    fun hideIcon() {
        val drawables = compoundDrawables
        drawables[2] = null
        if (isCustomizeSize) setCompoundDrawables(drawables[0], drawables[1], drawables[2], drawables[3])
        else setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawables[2], drawables[3])
    }

//    fun setIconClickListener(listener: ((View) -> Unit)?) {
//        mIconClickListener = listener
//    }

//    fun setIsClickInIcon(isClickInIcon: Boolean) {
//        this.isClickInIcon = isClickInIcon
//    }

//    fun setExpandAreaPadding(expandAreaPadding: Int) {
//        mExpandAreaPadding = expandAreaPadding
//    }

    private fun doAfterClick() {}
//    fun getIcon() = mIconDrawable
//    fun isClickInIcon() = isClickInIcon
    protected abstract fun notifyEvent()
    fun getPreSelectionEnd() = mPreSelectionEnd
    protected abstract fun precondition(): Boolean
//    fun getPreSelectionStart() = mPreSelectionStart
}