package com.tthappy.supercoolandroid.log

/**
 * Author:      tfhe
 * Create Date: Created in 2021/8/23 11:32
 * Update Date:
 * Modified By:
 * Description:
 */
internal val LINE_SEP = System.getProperty("line.separator")
internal const val TOP_CORNER = "┌"
internal const val MIDDLE_CORNER = "├"
internal const val LEFT_BORDER = "│ "
internal const val BOTTOM_CORNER = "└"
internal const val SIDE_DIVIDER = "────────────────────────────────────────────────────────────────────────────"
internal const val MIDDLE_DIVIDER = "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄"

internal const val MIN_STACK_OFFSET = 3

internal const val TOP_BORDER = TOP_CORNER + SIDE_DIVIDER
internal const val MIDDLE_BORDER = MIDDLE_CORNER + MIDDLE_DIVIDER
internal const val BOTTOM_BORDER = BOTTOM_CORNER + SIDE_DIVIDER

internal const val NOTHING = "log nothing"
internal const val NULL = "null"

var sGlobalTag = "TTHappy"
