package com.tthappy.supercoolandroid.java.datastruct.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/22 16:24
 * Update Date:
 * Modified By:
 * Description: 哈夫曼树（赫夫曼树）
 */
public class HuffmanTree<E extends Comparable<E>> {

    public Node head;
    List<Node> nodes = new ArrayList<>();

    public void add(E data, int weight) {
        nodes.add(new Node(data, weight));
    }

    public Node createTree() {
        while (nodes.size() > 1) {
            quickSort();

            Node left = nodes.get(0);
            Node right = nodes.get(1);

            Node parent;
            if (left.data instanceof Integer) {
                Integer p = (Integer)left.weight + (Integer)right.weight;
                parent = new Node((E)p, left.weight + right.weight);
            } else {
                parent = new Node(left.data, left.weight + right.weight);
            }
            parent.lChild = left;
            parent.rChild = right;

            nodes.remove(0);
            nodes.remove(0);

            nodes.add(parent);
        }

        return nodes.get(0);
    }

    private void quickSort() {
        quickSort(0, nodes.size() - 1);
    }

    private void quickSort(int start, int end) {
        if (start < end) {
            Node base = nodes.get(start);

            int cursorStart = start;
            int cursorEnd = end;

            while (cursorStart < cursorEnd) {
                while (cursorStart < cursorEnd && nodes.get(cursorEnd).weight >= base.weight) {
                    cursorEnd--;
                }
                swapNodes(cursorStart, cursorEnd);

                while (cursorStart < cursorEnd && nodes.get(cursorStart).weight <= base.weight) {
                    cursorStart++;
                }
                swapNodes(cursorStart, cursorEnd);
            }

            quickSort(start, cursorStart - 1);
            quickSort(cursorStart + 1, end);
        }
    }

    private void swapNodes(int i, int j) {
        Node tmp = nodes.get(i);
        nodes.set(i, nodes.get(j));
        nodes.set(j, tmp);
    }

    public class Node implements PrintTree.PrintNode<E> {
        E data;
        int weight;
        Node lChild, rChild;

        public Node(E data, int weight) {
            this.data = data;
            this.weight = weight;
        }

        @Override
        public E getData() {
            return data;
        }

        @Override
        public PrintTree.PrintNode<E> getLChild() {
            return lChild;
        }

        @Override
        public PrintTree.PrintNode<E> getRChild() {
            return rChild;
        }
    }
}
