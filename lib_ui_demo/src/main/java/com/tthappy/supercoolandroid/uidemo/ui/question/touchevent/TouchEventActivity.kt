package com.tthappy.supercoolandroid.uidemo.ui.question.touchevent

import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityNestedScrollAndFlingBinding
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityTouchEventBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2022/8/17 10:37
 * Update Date:
 * Modified By:
 * Description:
 */
@HpRouterData(name = "触摸事件的一些验证",
    icon = "R.drawable.icon_double_drag_bar_pentagon",
    desc = "触摸事件的一些验证",
    isNew = false,
    url = BridgeConstansUIDemo.TOUCH_EVENT)
@Route(path = BridgeConstansUIDemo.TOUCH_EVENT)
class TouchEventActivity : BaseTitleActivity<ActivityTouchEventBinding, BaseViewModel>() {
    override fun providerTitle() = ""

    override fun initView() {

    }

    override fun initData() {

    }

    override fun observeData() {

    }
}