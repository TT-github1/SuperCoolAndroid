package com.tthappy.supercoolandroid.uihome.debug.activity

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tthappy.supercoolandroid.uihome.R

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/18 15:53
 * Update Date:
 * Modified By:
 * Description:
 */
class DebugAdapter : BaseQuickAdapter<DebugItem, BaseViewHolder>(R.layout.item_debug) {
    override fun convert(holder: BaseViewHolder, item: DebugItem) {
        holder.setImageResource(R.id.iv_icon, item.icon ?: 0)
        holder.setText(R.id.tv_name, item.name)
        holder.setText(R.id.tv_desc, item.desc)
    }
}