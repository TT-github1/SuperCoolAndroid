package com.tthappy.supercoolandroid.java.algorithm.sort;

import java.util.Arrays;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/14 9:36
 * Update Date:
 * Modified By:
 * Description: 快速排序（基准元素归位， 划分左右为两部分，依法递归） O(nlogn)
 *
 *              快速排序每一次排序（递归），就把数组分为两部分（理想情况下），
 *              左部分数组的元素在下一次排序时就不需要再和右部分元素进行比较，
 *              直接节约了大量的冗余比较，右部分数组同理，我想这就是快排快的原因
 *              这是分而治之的思想，将大问题分解为小问题
 */

public class QuickSort {
    public static void main(String[] args) {
        int[] arr = {0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        int[] arr1 = {0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        quickSort(arr, 0, arr.length - 1);
        quickSort_direct(arr1, 0, arr1.length - 1);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr1));
    }

    // 不是自己写的算法，勉强能理解但不知道怎么写出来，以后再看看
    private static void quickSort(int[] arr, int start, int end) {
        // 当区间长度为1时结束排序
        if (start >= end) return;

        // 定义两端的指针下标为i、j
        int i = start;
        int j = end;

        // 外层循环为一个区间内的整个排序
        // 用右端指针 大于 左端指针为条件
        while (j > i) {
            // 内层循环为从左向右或从右向左一个方向上的排序操作
            // 用右端指针 大于 左端指针为条件 的同时
            // 用左端端点值为 基准数 进行判断，从右向左直到找到比基准数小的数
            while (j > i && arr[j] > arr[start]) {
                j--;
            }
            // 这里的“=”，是为了第一轮比较的时候，能让左指针能直接前进一位
            // 其他轮开始的时候，因为上一轮左右指针交互过值，所以arr[i] < arr[start]肯定成立，所以左指针也能顺利前进一位
            while (j > i && arr[i] <= arr[start]) {
                i++;
            }

            if (i == j) {
                int tmp = arr[i];
                arr[i] = arr[start];
                arr[start] = tmp;
            } else {
                int tmp = arr[j];
                arr[j] = arr[i];
                arr[i] = tmp;
            }
        }

        quickSort(arr, start, i - 1);
        quickSort(arr, i + 1, end);

    }

    // 比较直观的算法，符合常规思维习惯
    public static void quickSort_direct(int[] arr, int start, int end) {
        // 递归头
        if (end <= start) return;

        int left = start;
        int right = end;
        // 基准数（英文意思：支点）
        int pivot = arr[start];

        // 右指针应当一直大于左指针
        while (right > left) {
            // 从右向左一直找小于基准数的值
            while (right > left && arr[right] > pivot) {
                // 没找到就将右指针左移一位
                right--;
            }
            // 可能一直找到和左指针重合都没找到，和左指针重合就不需要执行下面的代码
            if (right > left) {
                // 找到小于基准数的值，将该值拷贝到左指针此时的位置上，开始从左往右找
                arr[left] = arr[right];
                // 每次换查找方向，指针都要往该方向前进一位
                left++;
            }

            while (right > left && arr[left] <= pivot) {
                left++;
            }
            if (right > left) {
                arr[right] = arr[left];
                right--;
            }
        }

        // 结束一轮查找，将基准数落到左右指针重合的位置，这个位置即是基准数在这个数组按序排序的正确位置
        arr[left] = pivot;

        quickSort_direct(arr, start, left - 1);
        quickSort_direct(arr, left + 1, end);

    }
}
