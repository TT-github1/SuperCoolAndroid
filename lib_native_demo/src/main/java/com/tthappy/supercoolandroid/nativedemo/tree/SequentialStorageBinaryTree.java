package com.tthappy.supercoolandroid.nativedemo.tree;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/1/26 9:31
 * Update Date:
 * Modified By:
 * Description:
 */
public class SequentialStorageBinaryTree {
    static {
        System.loadLibrary("nativedemo");
    }

    public native void createTree();
    public native void add(int data);
    public native void showTree();
}
