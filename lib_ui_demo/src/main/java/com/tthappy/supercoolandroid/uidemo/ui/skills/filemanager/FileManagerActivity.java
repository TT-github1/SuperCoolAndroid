package com.tthappy.supercoolandroid.uidemo.ui.skills.filemanager;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity;
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityFileManagerDemoBinding;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

@SuppressWarnings("all")
@RequiresApi(api = Build.VERSION_CODES.O)
@Route(path = BridgeConstansUIDemo.FILEMANAGER)
public class FileManagerActivity extends BaseTitleActivity<ActivityFileManagerDemoBinding, BaseViewModel> {

    @Override
    public void initView() {
        TextView textView = findViewById(R.id.tv_create_file);
        TextView textView2 = findViewById(R.id.tv_create_file2);
        TextView textView3 = findViewById(R.id.tv_create_file3);
        TextView textView4 = findViewById(R.id.tv_create_file4);
        TextView textView5 = findViewById(R.id.tv_create_file5);

        textView.setOnClickListener(v -> text1Method());
        textView2.setOnClickListener(v -> text2Method());
        textView3.setOnClickListener(v -> text3Method());
        textView4.setOnClickListener(v -> text4Method());
        textView5.setOnClickListener(v -> text5Method());
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //这个requestCode就是上面请求动态权限我们传进来的第三个参数
        if (1 == requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                call();
            } else {
//                ToastUtils.showShort("拒绝开权限你他喵的还想打电话？");
            }
        }
    }

    private void call() {
//        e("开call");
        Uri uri;

        //读外部文件
        ContentResolver resolver = getContentResolver();
        String[] downloadColumns = new String[]{
                MediaStore.Downloads._ID,
                MediaStore.Downloads.DISPLAY_NAME
        };
        Cursor cursor = resolver.query(MediaStore.Downloads.EXTERNAL_CONTENT_URI, downloadColumns, null, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                String cursorId = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Downloads._ID));
                String cursorName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Downloads.DISPLAY_NAME));
                if ("test5.jpg".equals(cursorName)) {
                    uri = Uri.withAppendedPath(MediaStore.Downloads.EXTERNAL_CONTENT_URI, cursorId);
                    break;
                }
            }
            cursor.close();
        }

        ContentResolver resolver1 = getContentResolver();
        ContentValues values = new ContentValues();
        values.put(MediaStore.Downloads.DISPLAY_NAME, "test5.jpg");
        values.put(MediaStore.Downloads.MIME_TYPE, "image/jpeg");
        Uri uri1 = MediaStore.Downloads.EXTERNAL_CONTENT_URI;
        uri = resolver1.insert(uri1, values);

//        e("测试：\nuri1:"+uri1.toString()+"\nvalues:"+values.toString()+"\nresolver1:"+resolver1.toString()+"\nuri:");

//        e("读完,uri:" + uri.toString());

        //写入外部文件
        try {
            OutputStream outputStream = getContentResolver().openOutputStream(uri);
            if (outputStream != null) {
                BitmapFactory.decodeResource(getResources(), R.drawable.abc_vector_test).compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                outputStream.flush();
                outputStream.close();
//                e("写完");


            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void text1Method() {
        File dir = getFilesDir();

//        e("File dir = getFilesDir();\ndir:"+dir.toString());

        File dir2 = new File(dir.getAbsolutePath(), "tmp2");
        dir2.mkdirs();

//        e("File dir2 = new File(dir.getAbsolutePath(), \"tmp2\");\ndir2:"+dir2.toString());

        File file = new File(dir2.getAbsolutePath(), "test2.txt");
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

//            e("File file = new File(dir2.getAbsolutePath(), \"test2.txt\");\nfile:"+file.toString());

            FileOutputStream outputStream = new FileOutputStream(file);
            OutputStreamWriter writer = new OutputStreamWriter(outputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            byte[] bytes = "天门中断楚江开".getBytes();
            outputStream.write(bytes);
            outputStream.close();

//            ToastUtils.showShort("看不见，摸不着，除非Root先");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void text2Method() {
        File dir = getExternalFilesDir(null);

//        e("File dir = getExternalFilesDir(null);\ndir:"+dir.toString());

        File dir2 = new File(dir.getAbsolutePath(), "tmp2");

//        e("File dir2 = new File(dir.getAbsolutePath(), \"tmp\");\ndir2:"+dir2.toString());

        dir2.mkdirs();
        File file = new File(dir2.getAbsolutePath(), "test2.txt");
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

//            e("File file = new File(dir2.getAbsolutePath(), \"test.txt\");\nfile:"+file.toString());

            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] bytes = "奔流到海不复回".getBytes();
            outputStream.write(bytes);
            outputStream.close();

//            ToastUtils.showShort("可以在“/内部存储/Android/data/com.tthappy.supercoolandroid/files/tmp3/”目录下查看");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void text3Method() {
        File dir = getExternalFilesDir(null);

        File dir2 = new File(dir, "aaa");
        dir2.mkdir();
//        dir2.delete();
//        ToastUtils.showShort("dir2.mkdir()");
        //            e(dir.toString()+"\n"+dir.getAbsolutePath()+"\n"+dir.getCanonicalPath()+"\n");
    }

    private void text4Method() {
        String dirPath = Environment.getExternalStorageDirectory() + "/DCIM/Camera/";
        String filePath = dirPath + "coolandroid.jpeg";

//        e(filePath);

        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        File file = new File(filePath);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileOutputStream os = new FileOutputStream(filePath);
            BitmapFactory.decodeResource(getResources(), R.drawable.brvah_sample_footer_loading).compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri uri = Uri.parse("file://" + filePath);
//        e(uri.toString());
//        e(uri.getPath());
        getMActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
    }

    private void text5Method() {
//        e("单击事件开始");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {
//            e("call了");
            call();
        }
    }

    @Override
    public void initData() {

    }

    @Override
    public void observeData() {

    }

    @Nullable
    @Override
    public String providerTitle() {
        return "文件管理";
    }
}