package com.tthappy.supercoolandroid.java.reflect;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/30 10:42
 * Update Date:
 * Modified By:
 * Description:
 */
public class MySingleton {
    private static volatile MySingleton instance;
    private MySingleton() { }
    public static MySingleton getInstance() {
        if (instance == null) {
            synchronized (MySingleton.class) {
                if (instance == null) {
                    instance = new MySingleton();
                }
            }
        }
        return instance;
    }

    @Override
    public String toString() {
        return "饿汉式\n懒汉式\n静态内部类\n枚举 -- 线程安全与否";
    }
}
