package com.tthappy.supercoolandroid.uihome.debug

import com.tthappy.supercoolandroid.uihome.debug.activity.DebugActivity

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/18 15:56
 * Update Date:
 * Modified By:
 * Description:
 */
abstract class DebugPlugin(val mActivity: DebugActivity) {
    abstract fun icon(): Int?
    abstract fun name(): String
    abstract fun value(): String
    abstract fun doWhat()
}