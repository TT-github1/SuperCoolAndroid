package com.tthappy.supercoolandroid.java.datastruct.tree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/14 9:36
 * Update Date:
 * Modified By:
 * Description: 重点在于计算每个节点距离屏幕左边的位置（利用递归方法返回的数值作为当前节点距离屏幕左边的位置的值）
 */
public class PrintTreeUtils<T extends Comparable<T>> {
    PrintTree<T> printTree = new PrintTree<>();

    public void show(PrintTree.PrintNode<T> tree) {
        showAVLTree(tree);
    }

    private void showAVLTree(PrintTree.PrintNode<T> tree) {
        scanAVLTree(tree);
        calculate(printTree.head, 0);

        levelTraversal(printTree.head);
    }

    private void scanAVLTree(PrintTree.PrintNode<T> tree) {
        printTree.head = copyAVLTree(tree);
    }

    // 将传进来的树改造成适合打印的树
    private  PrintTree<T>.Node copyAVLTree(PrintTree.PrintNode<T> node) {
        if (node == null) return null;

        PrintTree<T>.Node pNode = printTree.new Node(node.getData());
        pNode.lChild = copyAVLTree(node.getLChild());
        pNode.rChild = copyAVLTree(node.getRChild());

        return pNode;
    }

    private void levelTraversal(PrintTree<T>.Node node) {
        if (node == null) return;

        Queue<PrintTree<T>.Node> queue = new LinkedList<>();
        Queue<PrintTree<T>.Node> guide1Queue = new LinkedList<>();
        Queue<PrintTree<T>.Node> guide2Queue = new LinkedList<>();
        queue.offer(node);
        guide1Queue.offer(node);
        guide2Queue.offer(node);

        while (!queue.isEmpty()) {
            // 打印数字
            printNumber(queue);
            // 打印辅助线（横向虚线）
            printGuide_1(guide1Queue);
            // 打印辅助性（纵向虚线）
            printGuide_2(guide2Queue);
        }
    }

    private void printNumber(Queue<PrintTree<T>.Node> queue) {
        // 每层节点个数
        int size = queue.size();
        // 上一个节点的index
        int lastWidth = 0;
        // 遍历此层节点
        for (int i = 0; i < size; i++) {
            // 弹出队列头部节点
            PrintTree<T>.Node cursor  = queue.poll();
            if (cursor == null) return;
            // 打印该节点
            printNum(cursor, lastWidth);

            lastWidth = cursor.width + getDataWidth(cursor);

            // 将弹出的节点的左右孩子入队（如果有的话）
            if (cursor.lChild != null) {
                queue.offer(cursor.lChild);
            }
            if (cursor.rChild != null) {
                queue.offer(cursor.rChild);
            }
        }

        // 换行
        System.out.println();
    }

    private void printGuide_1(Queue<PrintTree<T>.Node> queue) {
        int size = queue.size();
        int lastWidth = 0;
        for (int i = 0; i < size; i++) {
            PrintTree<T>.Node cursor  = queue.poll();
            if (cursor == null || (cursor.lChild == null && cursor.rChild == null)) continue;

            if (cursor.lChild == null) {
                printGui_1(cursor.width - lastWidth, cursor.rChild.width + getDataWidth(cursor.rChild) - cursor.width);
                lastWidth = cursor.rChild.width + getDataWidth(cursor.rChild);
            } else if (cursor.rChild == null) {
                printGui_1(cursor.lChild.width - lastWidth, cursor.width - cursor.lChild.width);
                lastWidth = cursor.width;
            } else {
                printGui_1(cursor.lChild.width - lastWidth, cursor.rChild.width + getDataWidth(cursor.rChild) - cursor.lChild.width);
                lastWidth = cursor.rChild.width + getDataWidth(cursor.rChild);
            }

            if (cursor.lChild != null) queue.offer(cursor.lChild);
            if (cursor.rChild != null) queue.offer(cursor.rChild);
        }

        System.out.println();
    }

    private void printGuide_2(Queue<PrintTree<T>.Node> queue) {
        int size = queue.size();
        int lastWidth = 0;
        for (int i = 0; i < size; i++) {
            PrintTree<T>.Node cursor  = queue.poll();
            if (cursor == null || (cursor.lChild == null && cursor.rChild == null)) continue;

            if (cursor.lChild == null) {
                printGui_2(cursor.width - lastWidth, cursor.rChild.width - cursor.width + getDataWidth(cursor.rChild), true, false);
                lastWidth = cursor.rChild.width + getDataWidth(cursor.rChild);
            } else if (cursor.rChild == null) {
                printGui_2(cursor.lChild.width - lastWidth, cursor.width - cursor.lChild.width, true, true);
                lastWidth = cursor.width;
            } else {
                printGui_2(cursor.lChild.width - lastWidth, cursor.rChild.width + getDataWidth(cursor.rChild) - cursor.lChild.width, false, true);
                lastWidth = cursor.rChild.width + getDataWidth(cursor.rChild);
            }

            if (cursor.lChild != null) queue.offer(cursor.lChild);
            if (cursor.rChild != null) queue.offer(cursor.rChild);
        }

        System.out.println();
    }

    private void printNum(PrintTree<T>.Node node, int lastWidth) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < node.width - lastWidth; i++) {
            builder.append(" ");
        }
        builder.append(node.data);
        System.out.print(builder.toString());
    }

    private void printGui_1(int spaceNum, int lineNum) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < spaceNum; i++) {
            builder.append(" ");
        }
        for (int i = 0; i < lineNum; i++) {
            builder.append("-");
        }
        System.out.print(builder.toString());
    }

    private void printGui_2(int spaceNum, int lineNum, boolean isSingle, boolean isLeft) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < spaceNum; i++) {
            builder.append(" ");
        }
        if (!isSingle || isLeft) builder.append("|");
        for (int i = 0; i < (isSingle ? lineNum - 1 : lineNum - 2); i++) {
            builder.append(" ");
        }
        if (!isSingle || !isLeft) builder.append("|");
        System.out.print(builder.toString());
    }

    private int calculate(PrintTree<T>.Node node, int width) {
        if (node.lChild != null) {
            width = calculate(node.lChild, width);
        }

        node.width = width;
        width = width + getDataWidth(node);

        if (node.rChild != null) {
            width = calculate(node.rChild, width);
        }

        return width;
    }

    private int getDataWidth(PrintTree<T>.Node node) {
        return node.data.toString().length();
    }
}
