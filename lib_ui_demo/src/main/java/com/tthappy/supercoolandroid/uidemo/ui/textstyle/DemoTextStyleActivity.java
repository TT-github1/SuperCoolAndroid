package com.tthappy.supercoolandroid.uidemo.ui.textstyle;

import androidx.annotation.Nullable;

import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityTextStyleDemoBinding;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/19 14:50
 * Update Date:
 * Modified By:
 * Description:
 */
public class DemoTextStyleActivity extends BaseTitleActivity<ActivityTextStyleDemoBinding, DemoTextStyleViewModel> {
    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void observeData() {

    }

    @Nullable
    @Override
    public String providerTitle() {
        return null;
    }
}
