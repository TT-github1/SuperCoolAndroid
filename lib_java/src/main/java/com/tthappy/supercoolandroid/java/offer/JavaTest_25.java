package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/1 16:20
 * Update Date:
 * Modified By:
 * Description: 二分查找，找到则返回位置，找不到返回应该插入的位置
 */
public class JavaTest_25 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 5, 7, 9};
        System.out.println(base(arr, 10));
        System.out.println(base(arr, 7));
    }

    private static int base(int[] arr, int target) {
        if (arr == null) return -1;
        if (arr.length == 0) return 0;
        return search(arr, 0, arr.length - 1, target);
    }

    private static int search(int[] arr, int start, int end, int target) {
        if (start > end) return start; // 应该插入的位置

        int mid = (start + end) / 2;
        if (target == arr[mid]) return mid;
        if (target < arr[mid]) return search(arr, start, mid - 1, target);
        else return search(arr, mid + 1, end, target);
    }
}
