想在Android Studio的terminal中使用gradlew命令，需要先在系统变量中配置gradlew命令所在路径，例如我的一次配置是：
新建变量：
	GRADLEW_6.1.1_HOME  :  C:\Users\ad\.gradle\wrapper\dists\gradle-6.1.1-all\wrapper\dists\gradle-6.1.1-all\cfmwm155h49vnt3hynmlrsdst\gradle-6.1.1\bin
增加变量：
	path : %GRADLEW_6.1.1_HOME%\bin

另外，配置完系统变量后，需要在terminal中cd到项目根目录，否则仍无法正常使用gradlew命令