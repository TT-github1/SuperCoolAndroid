package com.tthappy.supercoolandroid.base.router

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/13 13:50
 * Update Date:
 * Modified By:
 * Description:
 */
object BridgeConstansUIDemo {

    private const val DEMO = "/uidemo/"
    const val OKHTTP = "${DEMO}okhttp"
    const val UITEST = "${DEMO}uiTest"
    const val DISPATCH = "${DEMO}dispatch"
    const val FILEMANAGER = "${DEMO}fileManager"
    const val INTPUTOUTPUT = "${DEMO}intputoutput"
    const val NOTIFICATION = "${DEMO}notification"
    const val REQUESTPERMISSION = "${DEMO}requestPermission"
    const val TAKEPHOTO = "${DEMO}takePhoto"
    const val SHAREDPREFERENCES = "${DEMO}sharedpreferences"
    const val NORMAL = "${DEMO}normal"
    const val EQUIPMENTATTRIBUTE = "${DEMO}equipmentAttribute"
    const val STATUSBARANDDIALOGANDPOPUPWINDOW = "${DEMO}statusBarAndDialogAndPopupWindow"
    const val NESTEDSCROLLANDFLING = "${DEMO}nestedScrollAndFling"
    const val TOUCH_EVENT = "${DEMO}touchEvent"
    const val SCROLLVIEW_AND_VIEWPAGER = "${DEMO}scrollViewAndViewPager"
    const val CC_CHART = "${DEMO}chart"
    const val CC_GUGD = "${DEMO}gugd"
    const val CC_PANEL = "${DEMO}panel"
    const val SHOW_CUSTOM_CONTROL = "${DEMO}showCustomControl"
    const val VIEWMODEL = "${DEMO}viewmodel"
    const val FRAGMENT = "${DEMO}fragment"
    const val BINDER = "${DEMO}binder"
    const val WEB = "${DEMO}web"
    const val LISTVIEW = "${DEMO}listView"
    const val RECYCLERVIEW = "${DEMO}recyclerView"
    const val FLEXBOXLAYOUT = "${DEMO}flexboxLayout"
    const val SOCKET_CLIENT = "${DEMO}client"
    const val SOCKET_SERVER = "${DEMO}server"
    const val LIFECYCLE_SHOW = "${DEMO}lifecycleShow"
    const val TRANSPARENT = "${DEMO}transparent"
    const val MEASURE_CHILDREN = "${DEMO}measureChildren"
    const val GAME_TCS = "${DEMO}gameTanChiShe"


    const val WAN_REGISTER = "${DEMO}wan_android_register"
    const val WAN_ARTICLES = "${DEMO}wan_android_articles"


    private const val DEMOFRAGMENT = "/uidemoFragment/"
    const val WEBVIEWFRAGMENT = "${DEMOFRAGMENT}webViewFragment"
}