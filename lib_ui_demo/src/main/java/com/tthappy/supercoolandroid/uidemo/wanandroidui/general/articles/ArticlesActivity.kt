package com.tthappy.supercoolandroid.uidemo.wanandroidui.general.articles

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityArticlesBinding
import com.tthappy.supercoolandroid.utils.measure.DimenUtils

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/13 14:11
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.WAN_ARTICLES)
class ArticlesActivity: BaseTitleActivity<ActivityArticlesBinding, ArticlesViewModel>() {

    private val mAdapter = ArticlesAdapter()

    override fun providerTitle() = "文章列表"

    override fun initView() {
        initRecyclerView()
        initListener()
    }

    private fun initRecyclerView() {
        // 建立TCP链接
        // 数据传送
        // 释放链接

        // 确认双方
        // 协商一些参数
        // 对运输实体资源：缓存大小、链接表中的项目等进行分配

        // TCP连接表
        // 指向发送和接收缓存的指针
        // 指向重传队列的指针
        // 当前的发送和接收序号

        // 标志位大写
        // 序列号小写

        // SYN = 1 ，seq = x
        // SYN = 1 ACK = 1, seq = y, ack = x+1
        // ACK = 1, seq = x+1, ack=y+1

        // SYN_SEND
        //              SYN_RCVD
        // ESTABLISHED
        //              ESTABLISHED

        // 三次握手 从逻辑上来说 是为了双方互相确认（输入输出）
        // 从丢包角度看是为了避免服务端浪费不必要的资源
        // 假设客户端先发起了一次连接请求，但因为网络延迟，在网络上兜兜转转，然后客户端超时重传，发起第二次连接请求
        // 然后正常的两次握手，交互，四次挥手
        // 断开连接之后，第一次的连接请求到达服务端，服务端响应这个连接，连接建立，结果就是服务端白白消耗了资源

        // SSL/TLS Secure Sockets Layer / Transport Layer Security


        viewBinding.rvContainer.adapter = mAdapter
        viewBinding.rvContainer.layoutManager = LinearLayoutManager(this)

        viewBinding.rvContainer.addItemDecoration(object : RecyclerView.ItemDecoration(){
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                super.getItemOffsets(outRect, view, parent, state)
                val dp10 = DimenUtils.dp2px(10f)
                val bottom = if (parent.getChildAdapterPosition(view) != mAdapter.data.size - 1) 0 else dp10
                outRect.set(0, dp10, 0, bottom)
            }
        })
    }

    private fun initListener() {
        mAdapter.setOnItemClickListener { _, _, position ->
            val item = mAdapter.getItem(position)
            BridgeUtils.bridgeWebView(item.link)
        }
    }

    override fun initData() {
        viewModel.getArticles()
    }

    override fun observeData() {
        viewModel.liveData.observe(this) {
            mAdapter.setNewInstance(it.toMutableList())
        }
    }
}