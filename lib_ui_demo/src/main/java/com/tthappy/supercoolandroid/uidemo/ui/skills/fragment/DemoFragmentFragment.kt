package com.tthappy.supercoolandroid.uidemo.ui.skills.fragment

import androidx.core.content.ContextCompat
import com.tthappy.supercoolandroid.mvvm.view.BaseFragment
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.FragmentFragmentDemoBinding
import com.tthappy.supercoolandroid.uidemo.databinding.FragmentVmTestDemoLeftBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/22 16:29
 * Update Date:
 * Modified By:
 * Description:
 */
class DemoFragmentFragment(val color: Int): BaseFragment<FragmentFragmentDemoBinding, BaseViewModel>() {
    override fun initView() {
        viewBinding.viewColor.setBackgroundColor(ContextCompat.getColor(requireContext(), color))
    }
    override fun initData() {}
    override fun observeData() {}
}