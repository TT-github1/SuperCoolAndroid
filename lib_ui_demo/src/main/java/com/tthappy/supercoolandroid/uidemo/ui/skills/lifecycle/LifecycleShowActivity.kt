package com.tthappy.supercoolandroid.uidemo.ui.skills.lifecycle

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils
import com.tthappy.supercoolandroid.log.HpLog
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityLifecycleShowDemoBinding
import kotlinx.coroutines.*

@HpRouterData(name = "LifecycleShow", icon = "R.drawable.icon_double_drag_bar_pentagon", desc = "展示各个生命周期", isNew = false, url = BridgeConstansUIDemo.LIFECYCLE_SHOW)
@Route(path = BridgeConstansUIDemo.LIFECYCLE_SHOW)
@SuppressLint("NotifyDataSetChanged")
class LifecycleShowActivity: BaseTitleActivity<ActivityLifecycleShowDemoBinding, BaseViewModel>() {

    private val mAdapter = LifecycleShowAdapter()
    private val mLifecycles = ArrayList<String>()

    companion object {
        const val LIFECYCLE_ON_CREATE = "onCreate"
        const val LIFECYCLE_ON_RESTART = "onRestart"
        const val LIFECYCLE_ON_START = "onStart"
        const val LIFECYCLE_ON_RESUME = "onResume"
        const val LIFECYCLE_ON_PAUSE = "onPause"
        const val LIFECYCLE_ON_STOP = "onStop"
        const val LIFECYCLE_ON_DESTROY = "onDestroy"

        const val LIFECYCLE_ON_NEW_INTENT = "onNewIntent"
        const val LIFECYCLE_ON_SAVE_INSTANCE_STATE = "onSaveInstanceState"
        const val LIFECYCLE_ON_RESTORE_INSTANCE_STATE = "onRestoreInstanceState"
    }


    override fun initView() {
        getTitleHelper().enableRightBtn("清屏") {
            mLifecycles.clear()
            mAdapter.notifyDataSetChanged()

            BridgeUtils.bridgeWithUrl(BridgeConstansUIDemo.UITEST)
        }

        viewBinding.rvContainer.adapter = mAdapter
        viewBinding.rvContainer.layoutManager = LinearLayoutManager(this)
        mAdapter.setNewInstance(mLifecycles)



        val scope = MainScope()
        scope.launch {
//            try {
                withContext(Dispatchers.IO + CoroutineName("main_1")) {
                    delay(10 * 1000)
                    HpLog.e("delay\nCoroutine Name: ${coroutineContext[CoroutineName]}")
                }
//            } catch (e: Exception) {
//                HpLog.e("$e \nCoroutine Name: ${coroutineContext[CoroutineName]}")
//            }
        }

        val main = MainScope()
        main.launch {
            withContext(Dispatchers.IO + CoroutineName("main_2")) {
                delay(10 * 1000)
                HpLog.e("delay\nCoroutine Name: ${coroutineContext[CoroutineName]}")
            }
        }

        viewBinding.rvContainer.post {
            main.cancel()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        log(LIFECYCLE_ON_CREATE)
        mLifecycles.add(LIFECYCLE_ON_CREATE)
        mAdapter.notifyDataSetChanged()
    }

    override fun onRestart() {
        super.onRestart()
        log(LIFECYCLE_ON_RESTART)
        mLifecycles.add(LIFECYCLE_ON_RESTART)
        mAdapter.notifyDataSetChanged()
    }

    override fun onStart() {
        super.onStart()
        log(LIFECYCLE_ON_START)
        mLifecycles.add(LIFECYCLE_ON_START)
        mAdapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        log(LIFECYCLE_ON_RESUME)
        mLifecycles.add(LIFECYCLE_ON_RESUME)
        mAdapter.notifyDataSetChanged()
    }

    override fun onPause() {
        super.onPause()
        log(LIFECYCLE_ON_PAUSE)
        mLifecycles.add(LIFECYCLE_ON_PAUSE)
        mAdapter.notifyDataSetChanged()
    }

    override fun onStop() {
        super.onStop()
        log(LIFECYCLE_ON_STOP)
        mLifecycles.add(LIFECYCLE_ON_STOP)
        mAdapter.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        log(LIFECYCLE_ON_DESTROY)
        mLifecycles.add(LIFECYCLE_ON_DESTROY)
        mAdapter.notifyDataSetChanged()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        log(LIFECYCLE_ON_NEW_INTENT)
        mLifecycles.add(LIFECYCLE_ON_NEW_INTENT)
        mAdapter.notifyDataSetChanged()
    }

    /**
     *  除了主动退出当前页面（按返回键等），其他会导致页面不可见的行为，都会触发这个函数，例如
     *  a. 当用户按下HOME键
     *  b. 长按HOME键，选择运行其他的程序
     *  c. 按下电源按键（关闭屏幕显示）
     *  d. 从activity A中启动一个新的activity
     *  e. 屏幕方向切换时，例如从竖屏切换到横屏
     *  此函数的触发时机在onPause和onStop之间
     */
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putStringArrayList("mLifecycles", mLifecycles)
        super.onSaveInstanceState(outState)
        log(LIFECYCLE_ON_SAVE_INSTANCE_STATE)
        mLifecycles.add(LIFECYCLE_ON_SAVE_INSTANCE_STATE)
        mAdapter.notifyDataSetChanged()
    }

    /**
     *  这个方法并不是和onSaveInstanceState成对出现的，需要Activity因为系统配置等原因销毁页面并重建
     *  才会被调用（像ViewModel）
     */
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        //将重建前保存的生命周期插入到合适的位置
        val lifecycles = savedInstanceState.getStringArrayList("mLifecycles")
        if (lifecycles != null && lifecycles.isNotEmpty()) {
            for (index in lifecycles.size - 1 downTo 0) {
                mLifecycles.add(0, lifecycles[index])
            }
        }

        log(LIFECYCLE_ON_RESTORE_INSTANCE_STATE)
        mLifecycles.add(LIFECYCLE_ON_RESTORE_INSTANCE_STATE)
        mAdapter.notifyDataSetChanged()
    }

    private fun log(lifecycle: String) {
        HpLog.e("当前生命周期为： $lifecycle")
    }

    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = "展示各个生命周期"
}