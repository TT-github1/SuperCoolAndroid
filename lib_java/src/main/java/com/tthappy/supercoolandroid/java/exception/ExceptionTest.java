package com.tthappy.supercoolandroid.java.exception;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/4 12:18
 * Update Date:
 * Modified By:
 * Description:
 */
public class ExceptionTest {

    public static void create() throws Exception {
        System.out.println("hhh");
        throw new Exception("OHHHHH");
    }

    public static void createMath() {
        System.out.println("hhh");
        try {
            int a = 1/0;
        }
        catch (Exception e) { //必须catch，否则还是报错
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.out.println(e.getCause());
        }
        finally {

        }
        System.out.println("xxx");
    }

    public static void main(String[] args) {
        createMath();
    }

}
