package com.tthappy.supercoolandroid.uidemo.ui.okhttp

import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/6 17:42
 * Update Date:
 * Modified By:
 * Description:
 */
//@HpRouterData()
interface TestInterface {
    fun test()
}