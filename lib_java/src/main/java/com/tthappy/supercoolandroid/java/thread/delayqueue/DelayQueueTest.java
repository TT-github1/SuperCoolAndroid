package com.tthappy.supercoolandroid.java.thread.delayqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/16 9:37
 * Update Date:
 * Modified By:
 * Description:
 */
public class DelayQueueTest {
    public static void main(String[] args) {
//        DelayQueue queue = new DelayQueue();

//        LinkedList<String> list = new LinkedList<>();
//
//        list.offer("aaa");
//        list.offer("bbb");
//        list.offer("ccc");
//
//        System.out.println(list.poll());
//        System.out.println(list.poll());
//        0.6个纳秒
//        System.out.println(list.poll());

        int max = Runtime.getRuntime().availableProcessors() + 1;

        ThreadPoolExecutor executor = new ThreadPoolExecutor(3, max, 1000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(10));
        executor.execute(() -> System.out.println("xxx"));

        System.out.println(max);
    }
}
