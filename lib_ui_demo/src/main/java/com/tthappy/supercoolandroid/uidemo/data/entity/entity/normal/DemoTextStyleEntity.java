package com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal;

import androidx.annotation.ColorRes;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/19 15:25
 * Update Date:
 * Modified By:
 * Description:
 */
public class DemoTextStyleEntity {
    private String text;
    private float textSize;
    private @ColorRes int textColor;
    private boolean isBold;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public boolean isBold() {
        return isBold;
    }

    public void setBold(boolean bold) {
        isBold = bold;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    private String styleName;
}
