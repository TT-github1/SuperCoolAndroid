package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/1 16:00
 * Update Date:
 * Modified By:
 * Description: 单纯 KMP
 *
 *              求next数组的过程 和 用next数组来进行字符串匹配的过程 两者具有很多相似的地方
 *
 */
public class JavaTest_24 {
    public static void main(String[] args) {
        String heyStack = "aaaabaaaabaaaaf";
        String needle = "aaaabaaaaf";
        System.out.println(strStr(heyStack, needle));
    }

    private static int[] getNext(String needle) {
        if (needle == null || needle.length() == 0) return new int[0];

        int j = 0;
        int[] next = new int[needle.length()];
        char[] ndl = needle.toCharArray();

        // 不同处1
        for (int i = 1; i < needle.length(); i++) {
            // 不同处2
            while (j > 0 && ndl[i] != ndl[j]) {
                j = next[j - 1]; // 想一想这一步怎么解释
            }
            if (ndl[i] == ndl[j]) j++;
            // 不同处3
            next[i] = j;
        }

        return next;
    }

    private static int strStr(String heyStack, String needle) {
        if (needle.length() == 0) return 0;

        int j = 0;
        int[] next = getNext(needle);

        // 不同处1 这里 i 为 0
        for (int i = 0; i < heyStack.length(); i++) {
            // 不同处1 这里是源字符串取下标和要匹配的字符串取下标进行匹配
            while (j > 0 && heyStack.charAt(i) != needle.charAt(j)) {
                j = next[j - 1];
            }
            if (heyStack.charAt(i) == needle.charAt(j)) j++;
            // 不同处3 求的东西不同，返回当然不同
            if (j == needle.length()) return i - j + 1;
        }

        return -1;
    }
}
