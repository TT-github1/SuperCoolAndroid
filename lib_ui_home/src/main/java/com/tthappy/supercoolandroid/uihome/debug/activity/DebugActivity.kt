package com.tthappy.supercoolandroid.uihome.debug.activity

import androidx.recyclerview.widget.LinearLayoutManager
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.router.BridgeConstants
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.uihome.databinding.ActivityDebugBinding
import com.tthappy.supercoolandroid.uihome.debug.DebugPlugin
import com.tthappy.supercoolandroid.uihome.debug.plugins.*

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/18 9:31
 * Update Date:
 * Modified By:
 * Description: 神秘控制台
 */
@Route(path = BridgeConstants.DEBUG_ACTIVITY)
class DebugActivity: BaseTitleActivity<ActivityDebugBinding, DebugViewModel>() {

    private val mAdapter = DebugAdapter()
    private val plugins = mutableListOf<DebugPlugin>()

    override fun initView() {
        viewBinding.rvContainer.adapter = mAdapter
        viewBinding.rvContainer.layoutManager = LinearLayoutManager(this)

        mAdapter.setOnItemClickListener { _, _, position ->
            plugins[position].doWhat()
        }
    }

    override fun initData() {
        plugins.add(DoSomeThingPlugin(this))
        plugins.add(UITestPlugin(this))
        plugins.add(HotFixPlugin(this))
        plugins.add(WebViewTestPlugin(this))
        plugins.add(DevicePlugin(this))

        mAdapter.setNewInstance(extractItems(plugins))
    }

    private fun extractItems(list: List<DebugPlugin>): MutableList<DebugItem> {
        val items = mutableListOf<DebugItem>()
        list.forEach {
            items.add(DebugItem(it.icon(), it.name(), it.value()))
        }
        return items
    }

    override fun observeData() {}
    override fun providerTitle() = "神秘控制台"
}