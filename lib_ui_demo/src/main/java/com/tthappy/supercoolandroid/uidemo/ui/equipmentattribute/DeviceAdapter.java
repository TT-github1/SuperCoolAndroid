package com.tthappy.supercoolandroid.uidemo.ui.equipmentattribute;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.tthappy.supercoolandroid.uidemo.R;
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal.DeviceEntity;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/24 18:50
 * Update Date:
 * Modified By:
 * Description:
 */
public class DeviceAdapter extends BaseQuickAdapter<DeviceEntity, BaseViewHolder> {
    public DeviceAdapter() {
        super(R.layout.item_device);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder holder, DeviceEntity item) {
        holder.setText(R.id.tv_project, item.getName());
        holder.setText(R.id.tv_text, item.getText());
        holder.setText(R.id.tv_button, item.getButton());
    }
}
