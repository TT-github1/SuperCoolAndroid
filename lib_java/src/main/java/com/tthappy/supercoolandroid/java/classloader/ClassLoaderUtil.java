package com.tthappy.supercoolandroid.java.classloader;

/**
 *  这个类用来测试热加载，请勿修改 ！！！
 */

public class ClassLoaderUtil {

    private ClassLoaderUtil() {}

    private static class Holder {
        private static final ClassLoaderUtil instance = new ClassLoaderUtil();
    }

    public static ClassLoaderUtil getInstance() {
        return Holder.instance;
    }

    public boolean init() {
        throw new IllegalStateException();
//        return true;
    }

}
