package com.tthappy.supercoolandroid.uidemo.ui.question.nestedscrollandfling

import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.annotation.HpRouterData
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityNestedScrollAndFlingBinding
import com.tthappy.supercoolandroid.utils.toast.ToastUtils

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/26 10:55
 * Update Date:
 * Modified By:
 * Description:
 */
@HpRouterData(name = "嵌套滑动加惯性滑动",
    icon = "R.drawable.icon_double_drag_bar_pentagon",
    desc = "嵌套滑动加惯性滑动",
    isNew = false,
    url = BridgeConstansUIDemo.NESTEDSCROLLANDFLING)
@Route(path = BridgeConstansUIDemo.NESTEDSCROLLANDFLING)
class NestedScrollAndFlingActivity: BaseTitleActivity<ActivityNestedScrollAndFlingBinding, BaseViewModel>() {

    override fun initView() {
        viewBinding.viewOne.setOnClickListener {
            ToastUtils.showShort("click one")
        }

        viewBinding.viewTwo.setOnClickListener {
            ToastUtils.showShort("click two")
        }

        viewBinding.viewThree.setOnClickListener {
            ToastUtils.showShort("click three")
        }
    }


    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = "嵌套滑动加惯性滑动"
}