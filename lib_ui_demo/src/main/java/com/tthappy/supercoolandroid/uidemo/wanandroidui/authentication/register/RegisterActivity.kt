package com.tthappy.supercoolandroid.uidemo.wanandroidui.authentication.register

import android.text.TextUtils
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityRegisterBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2022/3/31 11:46
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.WAN_REGISTER)
class RegisterActivity: BaseTitleActivity<ActivityRegisterBinding, RegisterViewModel>() {

    override fun initView() {
        initListener()
    }

    private fun initListener() {
        viewBinding.tvCommit.setOnClickListener {
            val etName = viewBinding.etName.text.toString()
            val etPassword = viewBinding.etPassword.text.toString()
            val etRecheckPassword = viewBinding.etRecheckPassword.text.toString()
            if (TextUtils.isEmpty(etName) || TextUtils.isEmpty(etPassword) ||TextUtils.isEmpty(etRecheckPassword)) return@setOnClickListener

            viewModel.register(etName, etPassword, etRecheckPassword,)
        }
    }

    override fun initData() {

    }

    override fun observeData() {

    }

    override fun providerTitle() = "注册"
}