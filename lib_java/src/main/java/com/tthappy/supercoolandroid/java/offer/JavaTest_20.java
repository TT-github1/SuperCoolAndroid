package com.tthappy.supercoolandroid.java.offer;

import java.util.Stack;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/30 10:14
 * Update Date:
 * Modified By:
 * Description:  括号匹配（符合常规使用习惯）
 */
public class JavaTest_20 {
    public static void main(String[] args) {
        String msg = "([)(])";
        System.out.println(find(msg));
    }

    private static boolean find(String msg) {
        if (msg.length() % 2 == 1) return false;

        Stack<Character> stack = new Stack<>();
        for (char c : msg.toCharArray()) {
            switch (c) {
                case '(' :
                case '[' :
                case '{' :
                    stack.push(c);
                    break;
                case ')' :
                    if (stack.size() == 0 || stack.peek() != '(') return false;
                    else stack.pop();
                    break;
                case ']' :
                    if (stack.size() == 0 || stack.peek() != '[') return false;
                    else stack.pop();
                    break;
                case '}' :
                    if (stack.size() == 0 || stack.peek() != '{') return false;
                    else stack.pop();
                    break;
                default :
                    break;
            }
        }
        return stack.size() == 0;
    }
}
