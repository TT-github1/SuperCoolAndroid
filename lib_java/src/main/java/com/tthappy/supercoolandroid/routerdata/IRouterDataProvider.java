package com.tthappy.supercoolandroid.routerdata;

import java.util.List;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/7 9:12
 * Update Date:
 * Modified By:
 * Description:
 */
public interface IRouterDataProvider {
    void addToRouterData(List<?> list);
}
