package com.tthappy.supercoolandroid.uihome.debug

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import com.tthappy.supercoolandroid.uihome.R
import com.tthappy.supercoolandroid.base.router.BridgeConstants
import com.tthappy.supercoolandroid.baseui.utils.BridgeUtils
import com.tthappy.supercoolandroid.utils.measure.DimenUtils

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/17 19:47
 * Update Date:
 * Modified By:
 * Description:
 */
object DebugActivityLifecycleCallbacks: Application.ActivityLifecycleCallbacks {
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {}
    override fun onActivityStarted(activity: Activity) {}
    override fun onActivityResumed(activity: Activity) {
        val window = activity.window
        var secretBtn: View? = window.findViewById(R.id.super_cool_secret_btn)
        if (null == secretBtn) {
            secretBtn = View(activity)
            secretBtn.id = R.id.super_cool_secret_btn
            secretBtn.isClickable = false
            secretBtn.setBackgroundResource(android.R.color.transparent)
            val lp = FrameLayout.LayoutParams(DimenUtils.dp2px(200f), DimenUtils.dp2px(80f))
            lp.gravity = Gravity.CENTER_HORIZONTAL
            (window.decorView as FrameLayout).addView(secretBtn, lp)
            secretBtn.setOnLongClickListener(View.OnLongClickListener {
                BridgeUtils.bridgeWithUrl(BridgeConstants.DEBUG_ACTIVITY)
                false
            })
        }
    }
    override fun onActivityPaused(activity: Activity) {}
    override fun onActivityStopped(activity: Activity) {}
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}
    override fun onActivityDestroyed(activity: Activity) {}
}