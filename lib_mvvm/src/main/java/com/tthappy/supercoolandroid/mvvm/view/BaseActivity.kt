package com.tthappy.supercoolandroid.mvvm.view

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.alibaba.android.arouter.launcher.ARouter
import com.tthappy.supercoolandroid.mvvm.viewmodel.BaseViewModel
import com.tthappy.supercoolandroid.mvvm.viewmodel.provideViewModelWithGeneric
import com.tthappy.supercoolandroid.utils.equipmemt.ScreenAdaptationUtils
import com.tthappy.supercoolandroid.views.inflateBindingWithGeneric
import java.util.*

/**
 * Author:      tfhe
 * Create Date: Created in 2021/8/10 9:13
 * Update Date:
 * Modified By:
 * Description:
 */
abstract class BaseActivity<VB: ViewBinding, VM: BaseViewModel>: AppCompatActivity() {

    protected var mActivity: Activity? = null
    protected lateinit var viewModel: VM
    lateinit var viewBinding: VB
    private val viewModelList = ArrayList<BaseViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        ARouter.getInstance().inject(this)
        initMainViewModel()
        initExtraViewModels()
        //默认选择简体中文,目前有日本，英语，和中文三种语言可选择（需要在改变屏幕密度前调用）
        changeAppLanguage(Locale.CHINA)

        ScreenAdaptationUtils.resetAppDensity(this, ScreenAdaptationUtils.WIDTH, 375f)
        super.onCreate(savedInstanceState)

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR //全屏显示
        window.statusBarColor = Color.TRANSPARENT

        mActivity = this
        viewBinding = inflateBindingWithGeneric(layoutInflater)
        setContentView(viewBinding.root)
        initView()
        initData()
        observeData()
    }

    private fun changeAppLanguage(locale: Locale?) {
        val metrics = resources.displayMetrics
        val configuration = resources.configuration
        configuration.setLocale(locale)
        resources.updateConfiguration(configuration, metrics)
    }

    private fun initMainViewModel() {
        viewModel = provideViewModel()
    }

    abstract fun initView()
    abstract fun initData()
    abstract fun observeData()
    protected open fun initExtraViewModels() {}
    fun provideViewModel(): VM = provideViewModelWithGeneric(this)
}