package com.tthappy.supercoolandroid.java.designpattern.singleton;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/4 8:49
 * Update Date:
 * Modified By:
 * Description: 懒汉式 - 多线程适用（DCL -- Double Check Lock）
 */
public class ThirdSaveLazySingleton {

    private ThirdSaveLazySingleton() {}

    private static ThirdSaveLazySingleton instance;

    public static ThirdSaveLazySingleton getInstance() {
        if (instance == null) {
            synchronized (ThirdSaveLazySingleton.class) {
                if (instance == null) {
                    instance = new ThirdSaveLazySingleton();
                }
            }
        }
        return instance;
    }

}
