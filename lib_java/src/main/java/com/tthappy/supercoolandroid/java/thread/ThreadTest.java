package com.tthappy.supercoolandroid.java.thread;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/10 11:32
 * Update Date:
 * Modified By:
 * Description:
 */
public class ThreadTest extends AAA {

    static {
        System.out.println("ThreadTest static first");
    }

    {
        System.out.println("ThreadTest normal first");
    }

    public static void main(String[] args) {
        new ThreadTest();
    }

    public ThreadTest() {
        System.out.println("ThreadTest construcor first");
    }

    static {
        System.out.println("ThreadTest static second");
    }

    {
        System.out.println("ThreadTest normal second");
    }
}