package com.tthappy.supercoolandroid.java.reflect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class MyClass {
    private static ServerSocket serverSocket;

    public static void main(String[] args) {

        System.out.println("Start...");
        try
        {
            System.out.println("IP = " + InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            serverSocket = new ServerSocket(8888);

            while (true) {
                Socket socket = serverSocket.accept();

                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String readString = reader.readLine();

                PrintWriter writer = new PrintWriter(socket.getOutputStream());

                if ("yes".equals(readString)) {
                    writer.println("yes");
                    System.out.println("yes");
                } else {
                    writer.println("no");
                    System.out.println("no");
                }
                writer.flush();
                reader.close();
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


