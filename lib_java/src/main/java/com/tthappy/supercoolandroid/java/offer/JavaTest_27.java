package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/7/5 14:01
 * Update Date:
 * Modified By:
 * Description: 找字符串的最后一个单词(字符串肯定有至少一个单词）
 */
public class JavaTest_27 {
    public static void main(String[] args) {
        String msg = "Happy birthday to you   ";
        System.out.println(calculate(msg));
    }

    private static int calculate(String msg) {
        int index = msg.length() - 1;
        while (msg.charAt(index) == ' ') index--;

        int wordLen = 0;
        while (index >= 0 && msg.charAt(index) != ' ') {
            index--;
            wordLen++;
        }
        return wordLen;
    }
}
