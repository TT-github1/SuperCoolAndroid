package com.tthappy.supercoolandroid.java.algorithm.dynamicprogramming;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/24 16:07
 * Update Date:
 * Modified By:
 * Description: 求斐波那契数列第n个数的值
 *
 *              递归是个从顶到底的过程
 *              动态规划是从底到顶的过程
 */
public class Fibonacci {
    public static void main(String[] args) {
        System.out.println(forceRecursion(45));
        System.out.println(memoryRecursion(45, new int[46]));
        System.out.println(dynamicProgrammingByArray(45));
        System.out.println(dynamicProgramming(45));
    }

    // 暴力递归
    private static int forceRecursion(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return forceRecursion(n - 1) + forceRecursion(n - 2);
    }

    // 记忆化递归
    private static int memoryRecursion(int n, int[] arr) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        if (arr[n] != 0) return arr[n];
        arr[n] = memoryRecursion(n - 1, arr) + memoryRecursion(n - 2, arr);
        return arr[n];
    }

    // 动态规划（用数组)（严格来说不是，因为不求最值）
    private static int dynamicProgrammingByArray(int n) {
        int[] arr = new int[n + 1];
        arr[0]  = 0;
        arr[1]  = 1;
        for (int i = 2; i <= n; i++) {
            arr[i] = arr[i - 1] + arr[i - 2];
        }
        return arr[n];
    }

    // 动态规划（严格来说不是，因为不求最值）
    private static int dynamicProgramming(int n) {
        if (n == 0) return 0;
        int a = 0;
        int b = 1;
        for (int i = 2; i <= n; i++) {
            int temp = a + b;
            a = b;
            b = temp;
        }
        return b;
    }
}
