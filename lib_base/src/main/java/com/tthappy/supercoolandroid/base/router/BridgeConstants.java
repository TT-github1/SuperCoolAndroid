package com.tthappy.supercoolandroid.base.router;

/**
 * Author:      tfhe
 * Create Date: Created in 2020/10/13 10:35
 * Update Date:
 * Modified By:
 * Description:
 */
public class BridgeConstants {

    //主页面
    private static final String HOME = "/home/";
    public static final String MAIN = HOME + "main";

    //自定义控件
    private static final String CUSTOM_CONTROL_PREFIX = "/customcontrol/";
    public static final String SHOW_CUSTOM_CONTROL = CUSTOM_CONTROL_PREFIX + "showCustomControl";
    public static final String CC_GUGD = CUSTOM_CONTROL_PREFIX + "goUpGoDown";
    public static final String CC_CHART = CUSTOM_CONTROL_PREFIX + "chart";
    public static final String CC_PANEL = CUSTOM_CONTROL_PREFIX + "panel";
    public static final String FLOWLAYOUT = CUSTOM_CONTROL_PREFIX + "flowLayout";
    public static final String DISPATCH = CUSTOM_CONTROL_PREFIX + "dispatch";

    //经典问题
    private static final String QUESTION_PREFIX = "/question/";
    public static final String SCROLLVIEW_AND_VIEWPAGER = QUESTION_PREFIX + "scrollViewAndViewPager";
    public static final String SCROLLVIEWTEST = QUESTION_PREFIX + "scrollTest";
    public static final String SCROLLVIEWTEST2 = QUESTION_PREFIX + "scrollTest2";
    public static final String EQUIPMENTATTRIBUTE = QUESTION_PREFIX + "equipmentAttribute";
    public static final String MUTILEVELDROPDOWNLIST = QUESTION_PREFIX + "mutiLevelDropDownList";
    public static final String EVENTDISPATCH = QUESTION_PREFIX + "eventDispatch";


    //jetpack
    private static final String JETPACK_PREFIX = "/jetpack/";
    public static final String JETPACK_SCORE = JETPACK_PREFIX + "score";
    public static final String DRAWER_LAYOUT = JETPACK_PREFIX + "drawerLayout";
    public static final String SAMPLE = JETPACK_PREFIX + "sample";
    public static final String MOVIE = JETPACK_PREFIX + "movie";
    public static final String MOVIEPRO = JETPACK_PREFIX + "moviePro";
    public static final String ROOMTEST = JETPACK_PREFIX + "roomTest";
    public static final String LIFECYCLE = JETPACK_PREFIX + "lifecycle";

    //测试
    private static final String TEST = "/test/";
    public static final String UI_TEST = TEST + "ui";
    public static final String MOCK = TEST + "mock";


    //demo
    private static final String DEMO = "/demo/";
    public static final String NORMAL = DEMO + "normal";
    public static final String OLD = DEMO + "old";
    public static final String MVVM = DEMO + "mvvm";
    public static final String RETROFIT = DEMO + "retrofit";

    //系统地学习知识
    private static final String SKILLS = "/skills/";
    public static final String USE_INTENT = SKILLS + "useIntent";  //Intent
    public static final String BROADCAST = SKILLS + "broadcast";  //broadcast
    public static final String INTPUTOUTPUT = SKILLS + "intputOutput";  //文件读写
    public static final String SHAREDPREFERENCES = SKILLS + "sharedPreferences";  //SharedPreferences
    public static final String REQUESTPERMISSION = SKILLS + "requestPermission";  //动态权限
    public static final String NOTIFICATION = SKILLS + "notification";  //手机通知
    public static final String TAKEPHOTO = SKILLS + "takePhoto";  //照相
    public static final String FILEMANAGER = SKILLS + "fileManager";  //文件管理
    public static final String MULTITHREADTEST = SKILLS + "multiThreadTest";  //多线程测试页面
    public static final String WEBVIEWTEST = SKILLS + "webviewTest";  //WebView的测试页面
    public static final String NORMALHTTP = SKILLS + "normalHttp";  //普通网络请求


    //三分组件的使用
    private static final String THIRDPARTY = "/thirdparty/";
    public static final String SMARTREFRESHLAYOUT = THIRDPARTY + "smartRefreshLayout"; // smartRefreshLayout下拉刷新控件


    //登录模块
    private static final String LOGIN = "/login/";
    public static final String BEGIN = LOGIN + "begin";  //初始登录页面




    //debug
    private static final String DEBUG = "/debug/";
    public static final String DEBUG_ACTIVITY = DEBUG + "activity";
    public static final String DEBUG_INIT_SERVICE = DEBUG + "initService";
}
