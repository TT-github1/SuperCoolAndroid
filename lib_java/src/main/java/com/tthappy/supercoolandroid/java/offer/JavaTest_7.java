package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/27 16:29
 * Update Date:
 * Modified By:
 * Description: 统计一个数字在排序数组中出现的次数。例如，输入排序数组{0, 1, 2, 2, 2, 2, 3}和数字3，
 *              由于数字3在该数组中出现了4次，所以函数返回4。
 */
public class JavaTest_7 {
    public static void main(String[] args) {
        int[] arr = {0, 1, 2, 2, 2, 2, 3};
        System.out.println(findLast(arr, 0, arr.length - 1, 2) - findFirst(arr, 0, arr.length - 1, 2) + 1);
    }

    private static int findFirst(int[] arr, int start, int end, int target) {
        int mid = (start + end) / 2;
        if (target == arr[mid]) {
            if (arr[mid - 1] == target) return findFirst(arr, start, mid, target);
            else return mid;
        } else if (target < arr[mid]) {
            return findFirst(arr, start, mid, target);
        } else return findFirst(arr, mid, end, target);
    }

    private static int findLast(int[] arr, int start, int end, int target) {
        int mid = (start + end) / 2;
        if (target == arr[mid]) {
            if (arr[mid + 1] == target) return findLast(arr, mid, end, target);
            else return mid;
        } else if (target < arr[mid]) {
            return findLast(arr, start, mid, target);
        } else return findLast(arr, mid, end, target);
    }
}
