package com.tthappy.supercoolandroid.java.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/14 9:24
 * Update Date:
 * Modified By:
 * Description:
 */
public class Socket {
    public static void main(String[] args) {
        try {
            SocketChannel socket = SocketChannel.open();
            socket.configureBlocking(false);
            Selector selector = Selector.open();
            socket.register(selector, SelectionKey.OP_CONNECT);
            socket.connect(new InetSocketAddress("169.254.17.80", 8888));
            new ChatThread(selector, socket).start();
            Calendar ca = Calendar.getInstance();
            while (true) {
                if (socket.isOpen()) {
                    selector.select();
                    Set<SelectionKey> keys = selector.selectedKeys();
                    Iterator<SelectionKey> iterator = keys.iterator();
                    while (iterator.hasNext()) {
                        SelectionKey key = iterator.next();
                        iterator.remove();
                        if (key.isConnectable()) {
                            while (!socket.finishConnect()) {
                                System.out.println("Connecting...");
                            }
                            socket.register(selector, SelectionKey.OP_READ);
                        }
                        if (key.isWritable()) {
                            socket.write((ByteBuffer) key.attachment());
                            socket.register(selector, SelectionKey.OP_READ);
                            System.out.println("==============" + ca.getTime() + " ==============");
                        }
                        if (key.isReadable()) {
                            ByteBuffer byteBuffer = ByteBuffer.allocate(1024 * 4);
                            int len;
                            try {
                                if ((len = socket.read(byteBuffer)) > 0) {
                                    System.out.println("Recived message from server...");
                                    System.out.println(new String(byteBuffer.array(), 0, len));
                                }
                            } catch (IOException e) {
                                System.out.println("Server error, please call server man,client is clossing...");
                                key.cancel();
                                socket.close();
//                                e.printStackTrace();
                            }
                            System.out.println("==================================");
                        }
                    }
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("Client error, please restart...");
//            e.printStackTrace();
        }
    }
}
