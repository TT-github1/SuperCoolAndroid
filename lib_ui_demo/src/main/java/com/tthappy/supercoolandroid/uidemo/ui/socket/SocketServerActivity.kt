package com.tthappy.supercoolandroid.uidemo.ui.socket

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Message
import com.alibaba.android.arouter.facade.annotation.Route
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.mvvm.view.BaseActivity
import com.tthappy.supercoolandroid.uidemo.databinding.ActivitySocketBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/23 8:45
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.SOCKET_SERVER)
class SocketServerActivity : BaseActivity<ActivitySocketBinding, SocketViewModel>() {

    private val uiHandler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            viewBinding.tvHost.x = msg.arg1.toFloat()
            viewBinding.tvHost.y = msg.arg2.toFloat()
        }
    }

    override fun initView() {
        startServer()

        viewBinding.tvServer.listener = { x, y ->
            val msg = Message.obtain()
            msg.arg1 = x
            msg.arg2 = y
            SocketServerUtils.getHandler().sendMessage(msg)
        }
    }


    private fun startServer() {
        SocketServerUtils.startServer(uiHandler)
    }

    override fun initData() {}
    override fun observeData() {}
}