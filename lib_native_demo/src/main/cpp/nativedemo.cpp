#include <jni.h>
#include <string>
#include <android/log.h>

#define TAG "tthappy"
#define LOG(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)

extern "C" JNIEXPORT jstring JNICALL
Java_com_tthappy_nativedemo_NativeLib_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    LOG("hhddddhh");
    return env->NewStringUTF(hello.c_str());
}