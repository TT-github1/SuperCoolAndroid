package com.tthappy.supercoolandroid.uidemo.ui.recyclerview.recyclerview;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityTryRecyclerViewBinding;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/2/11 11:42
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.RECYCLERVIEW)
public class TryRecyclerViewActivity extends BaseTitleActivity<ActivityTryRecyclerViewBinding, TryRecyclerViewViewModel> {

    TryRecyclerViewAdapter adapter;

    @Nullable
    @Override
    public String providerTitle() {
        return "RecyclerView试炼";
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {
        viewModel.getList();
    }

    @Override
    public void observeData() {
        viewModel.listLiveData.observe(this, strings -> {
            adapter = new TryRecyclerViewAdapter();
            adapter.setData(strings);
            viewBinding.rvContainer.setLayoutManager(new LinearLayoutManager(this));
            viewBinding.rvContainer.setAdapter(adapter);
        });
    }
}
