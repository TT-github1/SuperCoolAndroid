package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/29 15:14
 * Update Date:
 * Modified By:
 * Description: 判断回文数。即正读反读都一样。1、11、121、1221是回文数；12、1212、-11不是回文数
 */
public class JavaTest_18 {
    public static void main(String[] args) {
//        System.out.println(isBackNum(22122));
//        System.out.println(isBackNum(11));
//        System.out.println(isBackNumWithoutString(212));
//        System.out.println(isBackNumWithoutString(1));


        int[] arr = {3, 2, 2, 3};
        System.out.println(removeElement(arr, 3));
    }

    private static boolean isBackNum(int num) {
        if (num < 0) return false;
        char[] nums = String.valueOf(num).toCharArray();
        int start = 0;
        int end = nums.length - 1;
        while (start <= end) {
            if (nums[start] != nums[end]) return false;
            start++;
            end--;
        }
        return true;
    }

    // 不转字符串的方法
    private static boolean isBackNumWithoutString(int num) {
        if (num < 0) return false;
        int temp = num;
        int count = 0;
        while (temp > 0) {
            temp /= 10;
            count++;
        }

        temp = num;
        int[] nums = new int[count];
        for (int i = 0; i < count; i++) {
            nums[i] = temp % 10;
            temp /= 10;
        }

        int start = 0;
        int end = nums.length - 1;
        while (start <= end) {
            if (nums[start] != nums[end]) return false;
            start++;
            end--;
        }
        return true;
    }


    private boolean isBackNumWithoutString2(int num) {
        if (num < 0 || (num % 10 == 0 && num != 0)) return false;

        int revertedNum = 0;

        while (num > revertedNum) {
            revertedNum = revertedNum * 10 + num % 10;
            num /= 10;
        }

        return revertedNum == num || revertedNum / 10 == num;
    }

    public static int removeElement(int[] nums, int val) {
        int start = 0;
        int end = nums.length - 1;
        while(start <= end) {
            if(nums[start] == val) nums[start] = nums[end--];
            start++;
        }


        return end + 1;
    }

    public int hammingWeight(int n) {
        int count = 0;
        int temp = 1;
        for(int i = 0; i < 32; i++) {
            if((n & temp) == 1) {
                count++;
            }
            temp *= 2;
        }

        return count;
    }
}
