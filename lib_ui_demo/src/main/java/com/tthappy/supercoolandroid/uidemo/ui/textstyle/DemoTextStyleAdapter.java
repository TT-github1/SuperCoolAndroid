package com.tthappy.supercoolandroid.uidemo.ui.textstyle;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.tthappy.supercoolandroid.uidemo.R;
import com.tthappy.supercoolandroid.uidemo.data.entity.entity.normal.DemoTextStyleEntity;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/19 15:19
 * Update Date:
 * Modified By:
 * Description:
 */
public class DemoTextStyleAdapter extends BaseQuickAdapter<DemoTextStyleEntity, BaseViewHolder> {
    public DemoTextStyleAdapter() {
        super(R.layout.item_text_style_demo);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder baseViewHolder, DemoTextStyleEntity demoTextStyleEntity) {

    }
}
