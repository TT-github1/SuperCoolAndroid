package com.tthappy.supercoolandroid.uidemo.ui.skills.viewmodel

import androidx.recyclerview.widget.LinearLayoutManager
import com.alibaba.android.arouter.facade.annotation.Route
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity
import com.tthappy.supercoolandroid.mvvm.view.BaseFragment
import com.tthappy.supercoolandroid.uidemo.R
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityVmTestDemoBinding
import com.tthappy.supercoolandroid.uidemo.databinding.FragmentVmTestDemoLeftBinding
import com.tthappy.supercoolandroid.uidemo.databinding.FragmentVmTestDemoRightBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2021/11/29 14:41
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.VIEWMODEL)
class DemoVMTestActivity : BaseTitleActivity<ActivityVmTestDemoBinding, DemoVMTestViewModel>() {
    override fun initView() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fl_left, LeftFragment.getInstance())
            .replace(R.id.fl_right, RightFragment.getInstance())
            .commit()
    }

    class LeftFragment: BaseFragment<FragmentVmTestDemoLeftBinding, DemoVMTestViewModel>() {

        companion object {
            fun getInstance() = LeftFragment()
        }

        private val leftAdapter = LeftAdapter()

        override fun initView() {
            viewBinding?.rvContainer?.adapter = leftAdapter
            viewBinding?.rvContainer?.layoutManager = LinearLayoutManager(context)

            leftAdapter.setOnItemClickListener{ _, _, position ->
                val item = leftAdapter.getItem(position)
                viewModel.notifyRightFragment(item)
            }
        }

        override fun initData() {
            leftAdapter.setNewInstance(viewModel.getDataList())
        }
        override fun observeData() {}

        class LeftAdapter: BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_vm_test_demo_left) {
            override fun convert(holder: BaseViewHolder, item: String) {
                holder.setText(R.id.tv_name, item)
            }
        }
    }

    class RightFragment: BaseFragment<FragmentVmTestDemoRightBinding, DemoVMTestViewModel>() {
        companion object {
            fun getInstance() = RightFragment()
        }

        override fun initView() {
            println()
        }
        override fun initData() {}

        override fun observeData() {
            viewModel.rightFragmentLiveData.observe(this) {
                viewBinding?.tvMsg?.text = it
            }
        }
    }

    override fun initData() {}
    override fun observeData() {}
    override fun providerTitle() = "ViewModel的Fragment试炼"
}