package com.tthappy.supercoolandroid.uidemo.ui.okhttp;

import com.tthappy.supercoolandroid.uidemo.data.entity.entity.base.BaseResult;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Author:      A-mew
 * Create Date: Created in 2020/10/21 17:07
 * Update Date:
 * Modified By:
 * Description:
 */
public class ResultHandler {

    public static <T> Observable<T> handleCommonResult(BaseResult<T> result) {
        if (null == result) return Observable.empty();
        switch (result.getErrorCode()) {
            case 0:
                return Observable.just(result.getData()).observeOn(AndroidSchedulers.mainThread());
            default:
                return Observable.error(new Throwable(result.getErrorMsg()));
        }
    }
}
