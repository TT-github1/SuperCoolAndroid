package com.tthappy.supercoolandroid.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/4/6 20:02
 * Update Date:
 * Modified By:
 * Description:
 */

@Retention(RetentionPolicy.CLASS)  //仅作用于编译期
@Target(ElementType.TYPE)
public @interface HpRouterData {
    String name() default "";
    String icon() default "";
    String desc() default "";
    boolean isNew() default false;
    String url() default "";
}
