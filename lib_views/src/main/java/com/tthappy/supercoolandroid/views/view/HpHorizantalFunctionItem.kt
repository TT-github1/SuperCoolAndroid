package com.tthappy.supercoolandroid.views.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.tthappy.supercoolandroid.views.databinding.LayoutHorizantalFunctionItemBinding

/**
 * Author:      tfhe
 * Create Date: Created in 2022/5/10 17:01
 * Update Date:
 * Modified By:
 * Description:
 */
class HpHorizantalFunctionItem(private val mContext: Context, attrs: AttributeSet): ConstraintLayout(mContext, attrs) {

    init {
        initView()
    }

    private fun initView() {
        val viewBinding = LayoutHorizantalFunctionItemBinding.inflate((mContext as AppCompatActivity).layoutInflater, this)
    }

}