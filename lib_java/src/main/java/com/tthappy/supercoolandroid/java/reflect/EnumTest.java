package com.tthappy.supercoolandroid.java.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/9/30 9:07
 * Update Date:
 * Modified By:
 * Description:
 */
public class EnumTest {

    enum MyEnum {
        TIAN,
        TAN,
        SHUO,
        DI;

        static MyEnum get() {
            return SHUO;
        }

        MyEnum() {

        }
    }

    static class MyClass {
        private MyClass() {

        }
    }


    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        System.out.println(MySingleton.getInstance());
        Class<MySingleton> clazz = MySingleton.class;
        Constructor<MySingleton> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        MySingleton mySingleton = constructor.newInstance();
        System.out.println(mySingleton);
    }
}
