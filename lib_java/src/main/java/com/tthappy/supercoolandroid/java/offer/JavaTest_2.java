package com.tthappy.supercoolandroid.java.offer;

/**
 * Author:      tfhe
 * Create Date: Created in 2022/6/23 14:01
 * Update Date:
 * Modified By:
 * Description: 将一个有序非递减数组旋转，求这个旋转后的数组的最小值
 *              旋转数组指将一个数组前面连续若干个元素移动到数组末尾
 *              例：{1，2，3，4，5} => {3，4，5，1，2}
 *
 *              解：二分查找思想
 *                 搞两个指针，一前一后，每次取中点，
 *                 与前指针对比，前指针小于中点，说明最小值在后半段，令前指针移动到中点
 *                 前指针大于中点，说明最小值在前半段，令后指针移动到中点
 *                 直至前后指针相邻，此时后指针指向最小值
 */
public class JavaTest_2 {
    public static void main(String[] args) {
        int[] arr = {3, 4, 5, 6, 7, 8, 9, 1, 2};
        System.out.println(search(arr));
    }

    private static int search(int[] arr) {
        if (arr == null || arr.length == 0) return 0;
        if (arr.length == 1) return arr[0];
        // 旋转了0个元素
        if (arr[0] < arr[arr.length - 1]) return arr[0];

        int start = 0;
        int end = arr.length - 1;
        while (end - start != 1) {
            int mid = (end + start) / 2;

            // 特殊情况 {1, 0, 1, 1, 1}
            if (arr[start] == arr[mid] && arr[mid] == arr[end]) return order(arr);

            if (arr[mid] >= arr[start]) {
                start = mid;
            } else {
                end = mid;
            }
        }
        return arr[end];
    }

    private static int order(int[] arr) {
        int min = arr[0];
        for (int i : arr) {
            if (i < min) min = i;
        }
        return min;
    }
}
