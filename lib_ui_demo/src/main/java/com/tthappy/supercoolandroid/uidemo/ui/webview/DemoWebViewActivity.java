package com.tthappy.supercoolandroid.uidemo.ui.webview;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.tthappy.supercoolandroid.baseui.title.BaseTitleActivity;
import com.tthappy.supercoolandroid.baseui.title.TitleBarHelper;
import com.tthappy.supercoolandroid.base.router.BridgeConstansUIDemo;
import com.tthappy.supercoolandroid.uidemo.R;
import com.tthappy.supercoolandroid.uidemo.databinding.ActivityWebViewDemoBinding;

/**
 * Author:      tfhe
 * Create Date: Created in 2021/12/8 16:54
 * Update Date:
 * Modified By:
 * Description:
 */
@Route(path = BridgeConstansUIDemo.WEB)
public class DemoWebViewActivity extends BaseTitleActivity<ActivityWebViewDemoBinding, DemoWebViewViewModel> {

    @Autowired
    String url = "";

    TitleBarHelper helper;
    private DemoWebViewFragment fragment;

    @Nullable
    @Override
    public String providerTitle() {
        return "天天开心";
    }

    @Override
    public void initView() {
        fragment = (DemoWebViewFragment) ARouter.getInstance().build(BridgeConstansUIDemo.WEBVIEWFRAGMENT).withString("url", url).navigation();

        helper = getTitleHelper();
        helper.enableRightBtn("刷新", view -> fragment.reload());

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fl_container, fragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (!fragment.canGoBack()) super.onBackPressed();
    }

    @Override
    public void initData() { }
    @Override
    public void observeData() { }
}
